<?php

interface base_client_auth_plugin
{

	public function update_user($user_data);
}

class client_auth
{
	const API_VERSION = "20120223";
	const URL_REGEXP = '/^(https?):\/{2}[a-zA-Z0-9;\/?:@&=+$,#_.!~*\'()%,{}|\^\[\]-]+$/';

	protected $server_endpoint;
	protected $main_page_url;
	protected $return_to;
	protected $plugins = array();

	public function __construct($server_endpoint, $main_page_url)
	{
		$this->server_endpoint = $server_endpoint;
		$this->main_page_url = $main_page_url;
		$this->return_to = isset($_GET["return_to"]) ? $_GET["return_to"] : (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : false);
		if (!$this->return_to or !preg_match(self::URL_REGEXP, $this->return_to))
		{
			// Protect against urls with \r\n
			$this->return_to = $this->main_page_url;
		}
	}

	public function add_plugin(base_client_auth_plugin $client_auth_plugin)
	{
		$this->plugins[] = $client_auth_plugin;
	}

	protected function session_start()
	{
		session_start();
	}

	protected function set_user_id($user_id)
	{
		$_SESSION["user_id"] = $user_id ? $user_id : null;
	}

	protected function log_message($text, $file, $line)
	{
		error_log("{$text} - {$file}:{$line}", 0);
	}

	public function run()
	{
		if (!isset($_GET["method"]))
		{
			header("location: {$this->return_to}");
			return;
		}
		$this->session_start();
		$method = $_GET["method"];
		switch ($method)
		{
			case "ping":
				return $this->do_ping();
			case "login":
				return $this->do_login_or_register();
			case "logout":
				return $this->do_logout();
			case "register":
				return $this->do_login_or_register("register");
			case "login_return":
				return $this->do_login_return();
			case "logout_return":
				return $this->do_logout_return();
			case "update_user":
				return $this->do_update_user();
		}
	}

	private function do_ping()
	{
		if (!$xml = $this->call_server_endpoint())
		{
			$test = "BAD_XML";
		}
		else
		{
			$test = $xml["test"];
		}
		echo "API_VERSION=" . self::API_VERSION . ";TEST={$test}";
	}

	private function do_login_or_register($method = "login")
	{
		$params = array(
			"method" => $method,
			"return_to" => $this->return_to
		);
		$get_str = http_build_query($params, "", "&");
		header("Location: {$this->server_endpoint}?{$get_str}");
	}

	private function do_logout()
	{
		$params = array(
			"method" => "logout",
			"return_to" => $this->return_to
		);
		$get_str = http_build_query($params, "", "&");
		header("Location: {$this->server_endpoint}?{$get_str}");
	}

	private function do_login_return()
	{
		if (!$xml = $this->call_server_endpoint())
		{
			header("Location: {$this->return_to}");
			return;
		}

		if (($type = $xml["type"]) != "login")
		{
			$this->log_message("Bad message type '{$type}'", __FILE__, __LINE__);
			header("Location: {$this->return_to}");
			return;
		}

		if ($user_data = $this->get_user_data_from_xml($xml->user))
		{
			
			foreach ($this->plugins as $plugin)
			{
				$plugin->update_user($user_data);
			}
		}

		$user_id = (int) $xml["user_id"];
		$this->set_user_id($user_id);
		header("Location: {$this->return_to}");
		return;
	}

	private function do_logout_return()
	{
		$this->set_user_id(0);
		header("Location: {$this->return_to}");
	}

	private function do_update_user()
	{
		if (!$xml = $this->call_server_endpoint())
		{
			return;
		}

		if (($type = $xml["type"]) != "update_user")
		{
			$this->log_message("Bad message type '{$type}'", __FILE__, __LINE__);
			return;
		}

		foreach ($xml->user as $user_xml)
		{
			if ($user_data = $this->get_user_data_from_xml($user_xml))
			{
				//$r .= var_export( $user_data, 1);
				foreach ($this->plugins as $plugin)
				{
					$plugin->update_user($user_data);
				}
			}
		}
		echo "OK";
	}

	private function get_user_data_from_xml($xml)
	{
		$required_fields = array(
			"id", "login", "is_member", "is_admin", "last_name", "first_name", "mid_name", "phone", "email", "sex", "game_name",
			"photo_big_url", "photo_big_width", "photo_big_height",
			"photo_mid_url", "photo_mid_width", "photo_mid_height",
			"photo_small_url", "photo_small_width", "photo_small_height",
			"profile_url", "profile_edit_url"
		);
		$user_data = array();
		foreach ($required_fields as $key)
		{
			if (is_null($user_data[$key] = (string) $xml[$key]))
			{
				log_message("'{$key}' key not set in user data", __FILE__, __LINE__);
				return false;
			}
		}
		return $user_data;
	}

	// Вызываем server_endpoint с переданным ключом,
	// возвращаем полученный массив с данными или false.
	private function call_server_endpoint()
	{
		if (!isset($_GET["key"]))
		{
			return false;
		}
		$key = $_GET["key"];
		if ($key == "none")
		{
			return false;
		}
		$url = "{$this->server_endpoint}?key={$key}";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; devyourself.ru/client)");
		$response_xml = curl_exec($ch);
		$c = "UNSET";
		if ((false === $response_xml) or (($c = curl_getinfo($ch, CURLINFO_HTTP_CODE)) != 200))
		{
			// Данных совсем не пришло или может что и есть, но это не 200 ok
			$this->log_message("Empty response from ({$url}) code:{$c}", __FILE__, __LINE__);
			return false;
		}
		libxml_use_internal_errors(true);
		$xml = simplexml_load_string($response_xml);

		if (!$xml)
		{
			$this->log_message("Bad XML from the server endpoint", __FILE__, __LINE__);
			return false;
		}

		return $xml;
	}

}

?>