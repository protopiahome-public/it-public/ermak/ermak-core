<?php

class wp_client_auth_plugin implements base_client_auth_plugin
{

	public function update_user($user_data) // $user_data
	{
		$data = array (
			'user_pass' 		=> md5(md5(rand())),
			'user_login' 		=> $user_data["login"],
			'first_name' 		=> $user_data["first_name"],
			'last_name' 		=> $user_data["last_name"],
			'role' 				=> $user_data["is_admin"] ? 'administrator' :  get_option('default_role'),
			'display_name' 		=> "{$user_data["first_name"]} {$user_data["last_name"]}",
			'user_email'		=> $user_data['email'],
			'avatar'			=> array($user_data['photo_big_url'])
		);

		$users_exists = get_users("meta_key=dys_id&meta_value={$user_data["id"]}");
		if (count($users_exists))
		{
			$user_exists = $users_exists[0];
			$user_id = $user_exists->ID;
			$data["ID"] = $user_id;
			wp_update_user( 
				$data
			);
		}
		else
		{
			$user_id = wp_insert_user( 
				$data
			);
			update_user_meta($user_id, "dys_id", $user_data["id"]);
		}
		
		
		

	}

}

?>