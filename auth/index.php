<?php

/*
  all settings moved to bank config
 */
error_reporting(E_ALL);
ini_set("display_error", 1);
require_once dirname(__FILE__) . "/../../../../wp-load.php";
require_once dirname(__FILE__) . "/client_authlib/client_auth.php";
require_once dirname(__FILE__) . "/client_authlib/wp_client_auth_plugin.php";

function __autoload($class_name)
{
	if (file_exists(dirname(__FILE__) . "/client_authlib/{$class_name}.php"))
	{
		require_once dirname(__FILE__) . "/client_authlib/{$class_name}.php";
	}
}

function log_message($text, $file, $line)
{
	echo "{$text} - {$file}:{$line}\n";
}

class my_client_auth extends client_auth
{

	protected function session_start()
	{
		session_start();
	}

	protected function set_user_id($user_id)
	{
		if (!$user_id)
		{
			wp_logout();
		}
		else
		{
			$users_exists = get_users("meta_key=dys_id&meta_value={$user_id}");
			if (count($users_exists))
			{
				$user_exists = $users_exists[0];
				wp_set_current_user($user_exists->ID, $user_exists->email);
				wp_set_auth_cookie($user_exists->ID);
				do_action('wp_login', $user_exists->ID);
			}
		}
	}

	protected function log_message($text, $file, $line)
	{
		log_message($text, $file, $line);
	}

}

$client_auth = new my_client_auth("http://metaversity.ru/endpoint/" . get_option('dys_project_name') . "/", "http://" . get_option('dys_project_name') . ".metaversity.ru/");
//$client_auth->add_plugin(new tigra_client_auth_plugin($config["db_host"], $config["db_user"], $config["db_pass"], $config["db_name"], $config["cache_path"]));
$client_auth->add_plugin(new wp_client_auth_plugin());
$client_auth->run();
?>