<?php
	
	class Assistants
	{
		protected $options;
		
		function __construct() 
		{
			global $need_login_stroke;
			$this->options				= get_option(SMC_ID);
			if(!isset($this->options['hide_panel']))
			{
				$this->options			= array(
													'hide_panel' 		=> true,
													'admin_optimyze'	=> true,
													'use_exec'			=> true,
													'smc_height'		=> 350,
													'use_panel'			=> true
												);
				update_option(SMC_ID, $this->options);								
			}
			add_action('init', 					array($this, 'enable_errors'));
			// Отключаем админ панель для всех, кроме администраторов.
			if($this->options['hide_panel'])
				add_action("init", 					array($this, 'hide_panel_handler'));
			// Hook onto the action 'admin_menu' for our function to remove menu items
			if($this->options['admin_optimyze'])
			{
				add_action( 'admin_menu', 			array( $this, 'remove_menus' ) );
			}
			//php-code in posts and pages for Admin User
			if($this->options['use_exec'])
				add_action("the_content", 			array($this,'inline_php'), 0);
			
			//	Metagame menu in frontend		
			add_action( 'wp_before_admin_bar_render', array($this,'my_admin_bar_render' ));
			//
			add_action("admin_init", 				array($this,'alien_comment_hide_handler'));
			add_action('admin_menu', 				array( $this, 'init_other_plugins' ) , 11);
			//
			add_action("admin_print_styles", 		array($this,"change_editor_font"));
			
			//
			add_filter('user_contactmethods', 		array($this,'my_user_contactmethods'));
			
			//redirect after logout
			add_filter('login_redirect', 			array($this,'logout_redirect_to_home'));
			
			//id of mediafiles
			add_filter('manage_media_columns', 		array($this,'posts_columns_attachment_id'), 1);
			add_action('manage_media_custom_column', array($this,'posts_custom_columns_attachment_id'), 1, 2);
				
			//slideshow and video courusel
			add_action('admin_init', 				array($this, 'add_slide_show_to_post'), 1);			
			add_action('save_post', 				array($this, 'my_extra_fields_update'), 0);
			
			
			//add_filter( 'the_title',				array($this, 'the_title'),  99, 2 );
			add_filter( 'the_content',				array($this, 'the_content') );
			add_filter('smc_location_archive_title',array($this, 'smc_location_archive_title'),11,  2 );
			
			//add_filter('smc_login_form', 			array($this, 'smc_login_form'),99);
			//add_filter('smc_register_form', 		array($this, 'smc_register_form'),99);
			//add_filter("loginout", 				array($this, 'loginout'));
			
			
			add_action( 'show_user_profile', 		array($this, 'extra_user_profile_fields' ));
			add_action( 'edit_user_profile', 		array($this, 'extra_user_profile_fields' ));
			
			
			add_action( 'after_switch_theme', 		array(__CLASS__,'init_main_mg_menu' ));
			add_filter('smc_lp_main_menu', 			array($this, 'smc_lp_main_menu'));
			add_action( 'after_setup_theme', 		array($this, 'theme_register_nav_menu') );
			add_action( 'wp_login', 				array($this, 'wp_login'), 2 );
			add_filter('login_redirect', 			array($this, 'admin_default_page'));
			
			add_filter("attachment_fields_to_edit", array($this, "my_image_attachment_fields_to_edit"), 99, 2);
			//add_filter("attachment_fields_to_save", "my_image_attachment_fields_to_save", 99, 2);
			add_filter('smc_get_user_avatar', 		array($this, 'smc_get_user_avatar'));
			add_filter("smc_register_action", 		array($this, 'smc_register_action'), 10, 3);
			//add_action('user_register',			array($this, 'after_user_refgister'));
			add_action( 'wp_dashboard_setup',		array($this, 'slt_dashboardWidgets' ), 99);
			add_action( 'admin_init', 				array( $this, 'remove_dashboard_widgets' ) );
			add_action( 'contextual_help', 			array( $this, 'my_admin_help' ), 11, 2);
			add_action( 'widgets_init', 			array($this, 'register_sidebar'));
			add_action( 'footer_widgets',			array($this, 'footer_widgets') , 9);
			add_filter( 'footer_text',				array($this, 'footer_text') , 9);
			
			
			add_filter('template_include', 				array($this, 'my_template'));
			add_filter("ermak_body_before", 			array($this, "ermak_body_before"));// null template content
			
			add_filter( 'parse_query', 				array($this, 'ba_admin_posts_filter') );
			add_action( 'restrict_manage_posts', 	array($this, 'ba_admin_posts_filter_restrict_manage_posts') );
			
			//separators for menus and submenus in admin
			add_filter( 'parent_file',				array($this, 'admin_menu_separator') ); //	https://gist.github.com/franz-josef-kaiser/4693195
			add_filter( 'parent_file', 				array($this, 'admin_submenu_separator' ));
			
			add_filter( "smc_ajax_data", 			array($this, "smc_ajax_data"));
		}
		function smc_ajax_data($data)
		{
			global $SolingMetagameProduction;
			$data[1]['expected_time']	= $SolingMetagameProduction->get_expected_finish_time();
			//if($data[0]!= "reload_log") insertLog("smc_ajax_data", $data);
			return $data;
		}
		
		function ba_admin_posts_filter( $query )
		{
			global $pagenow;
			if ( is_admin() && $pagenow=='edit.php' && isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') 
			{
				$query->query_vars['meta_key'] = $_GET['ADMIN_FILTER_FIELD_NAME'];
				if (isset($_GET['ADMIN_FILTER_FIELD_VALUE']) && $_GET['ADMIN_FILTER_FIELD_VALUE'] != '')
					$query->query_vars['meta_value'] = $_GET['ADMIN_FILTER_FIELD_VALUE'];
			}
		}

		function ba_admin_posts_filter_restrict_manage_posts()
		{
			global $wpdb, $post, $wp_list_table;
			require_once(SMC_REAL_PATH."class/SMC_Object_type.php");
			$SMC_Object_Type	= new SMC_Object_Type();
			$object				= $SMC_Object_Type->object;
			$current 			= isset($_GET['ADMIN_FILTER_FIELD_NAME'])? $_GET['ADMIN_FILTER_FIELD_NAME']:'';
			$current_v 			= isset($_GET['ADMIN_FILTER_FIELD_VALUE'])? $_GET['ADMIN_FILTER_FIELD_VALUE']:'';
			if($post){
			?>
			<select name="ADMIN_FILTER_FIELD_NAME">
			<option value="">--</option>
			<?php
				foreach($object[$post->post_type] as $key=>$val)
				{
					if($key 	== "t") continue;
						$sel	= $key == $current ? ' selected="selected"' : '';
						echo "<option $sel>".$key."</option>";
				}
			?>
			</select> <input type="TEXT" name="ADMIN_FILTER_FIELD_VALUE" value="<?php echo $current_v; ?>" />
			<input name="pagenum" 			type="hidden" value="<?php echo $wp_list_table->get_pagenum();?>" />
			<input name="items_per_page" 	type="hidden" value="<?php echo $wp_list_table->get_items_per_page('per_page');?>" />
			
			<?php
			}
		}
		
		function my_template($template)
		{
			global $post;
			if(
				//is_front_page() || 
				is_archive()
			  ) 
			  null;
			else if(	$post->ID == $this->options['my_log_page_id']
						|| $post->ID == $this->options['location_display'] 
					) 
			{
				return SMC_REAL_PATH."template/empty.php";
			}
			return $template;
		}
		function ermak_body_before($text)
		{
			global $post, $is_mobile;
			switch($post->ID)
			{
				case $this->options['my_log_page_id']:
					return $text . my_log_page();
					break;
				case $this->options['location_display']:
					return $text .  location_display();
					break;
			}
			return $text;
			//if($post->ID != $this->options['my_log_page_id']) return "";		
		}
		function register_sidebar()
		{
			register_sidebar( array(
				'name' => sprintf(__('footer sidebar %d'), 1 ),
				'id' => "smc_footer_sidebar_1",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="footer_widget"><div>',
				'after_widget' => "</div></li>\n",
				'before_title' => '<h2 class="widgettitle">',
				'after_title' => "</h2>\n",
			) );
			register_sidebar( array(
				'name' => sprintf(__('footer sidebar %d'), 2 ),
				'id' => "smc_footer_sidebar_2",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="footer_widget">',
				'after_widget' => "</li>\n",
				'before_title' => '<h2 class="widgettitle">',
				'after_title' => "</h2>\n",
			) );
			register_sidebar( array(
				'name' => sprintf(__('footer sidebar %d'), 3 ),
				'id' => "smc_footer_sidebar_3",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="footer_widget">',
				'after_widget' => "</li>\n",
				'before_title' => '<h2 class="widgettitle">',
				'after_title' => "</h2>\n",
			) );
		}
		function footer_widgets()
		{
			?>
				<center class='mg_footer'>
					<div style='display:inline-block; position:relative;width:100%;'>						
						<ul id="sidebar">
							<?php dynamic_sidebar( 'smc_footer_sidebar_1' ); ?>
							<?php dynamic_sidebar( 'smc_footer_sidebar_2' ); ?>
							<?php dynamic_sidebar( 'smc_footer_sidebar_3' ); ?>
						</ul>
					</div>
				</center>
			<?php
		}
		function footer_text()
		{
			echo __("Metagame", "smc");
		}
		function my_admin_help($text, $screen) 
		{
			// Проверим, только ли для страницы настроек это применимо
			if (strcmp($screen, MY_PAGEHOOK) == 0 ) {
				$text = 'Вот некоторая полезная информация, которая поможет вам разобраться с плагином...';
				return $text;
			}
			// Пусть по умолчанию штуки с помощью будут и на других страницах панели управления
			return $text;
		}
		function init_other_plugins()
		{
			global $add_local_avatars;
			$tips	= get_option( 'taxonomy_image_plugin_settings' );
			if($tips!="")
			{
				if(!$tips["taxonomies"])
					$tips["taxonomies"]= array(SMC_LOCATION_NAME);
				if(!isset($tips["taxonomies"][SMC_LOCATION_NAME]))	$tips["taxonomies"][]= SMC_LOCATION_NAME;	
				if(!isset($tips["taxonomies"]["category"])) 		$tips["taxonomies"][]= "category";			
				update_option('taxonomy_image_plugin_settings', $tips);
			}
			if(is_plugin_active('add-local-avatar/avatars.php') )
			{
				if($plugin_avatars		= get_option("plugin_avatars"))
				{
					$plugin_avatars['upload_dir'] 		= "/avatars";
					$plugin_avatars['legacy'] 			= 'on';
					$plugin_avatars['upload_allowed']	= 'on';
					update_option("plugin_avatars", $plugin_avatars);
					$root = $add_local_avatars->avatar_root();
					if(
						!file_exists( $root . $plugin_avatars['upload_dir'] ) && 
						@!mkdir($root . $plugin_avatars['upload_dir'], 0777)
					  )
						null;
				}
			}
		}
		public static function echo_me($text, $text_area=false)
		{	
			ob_start();
			//var_dump($text);
			print_r($text);
			$var 	= ob_get_contents();
			ob_end_clean();
			if($text_area)
			{
				return "<textarea style='width:100%; height:500px;'>".$var.'</textarea>';
			}
			else
			{
				return $var;
			}
		}
		function my_image_attachment_fields_to_edit($form_fields, $post) 
		{
			if(!isset($_GET['current_url']) || $_GET['current_url']!= home_url().'/wp-admin/admin.php?page=smc_location_menu_sub_menu')	return $form_fields;
			$form_fields = array();
			$form_fields["svg_id"] = array(
				"label" => __("Send texture to map geometry", "smc"),
				"input" => "html", 
				"html" => '<span class="button-primary" style="padding:40px;">'.__("Associate to Map element ", "smc") . $_GET['cur_svg_id'] .'</span>',
			);
			$form_fields["svg_id_hidden"] = array(
				"label" => __("current SVG ID"),
				"input" => "hidden", 
				"value" => $_GET['cur_svg_id'],
			);
			return $form_fields;
		}
		function my_image_attachment_fields_to_save($post, $attachment) 
		{
			if(isset($attachment['svg_id_hidden']))
			{
			
			}
		}
		
		 function extra_user_profile_fields($user)
		{
			?>
				<table class="form-table">
					<tr>
					<th><label for="key"><?php _e("Avatar", "smc"); ?></label></th>
						<td>
							<?php 
							/*
							$plugin_avatars			= get_option("plugin_avatars");
							var_dump($plugin_avatars);
							$avatar	= get_user_meta($user->ID, 'avatar'); 
							var_dump($avatar);
							foreach($avatar as $key=>$val)
							{
								echo "<BR><img src='". $val . "'>"; 
							}
							*/
							echo get_avatar($user->ID, 100);
							?>						
						</td>
					</tr>
				</table>
			<?php
		}
		
		/**
		 * Проверяет роль определенного пользователя. 
		 * Возвращает true при совпадении.
		 *
		 * @param строка $role Название роли.
		 * @param логический $user_id (не обязательный) ID пользователя, роль которого нужно проверить.
		 * @return bool
		 */
		function check_user_role( $role, $user_id = null ) {
		 
			if ( is_numeric( $user_id ) )
			$user = get_userdata( $user_id );
			else
				$user = wp_get_current_user();
		 
			if ( empty( $user ) )
			return false;
		 
			return in_array( $role, (array) $user->roles );
		}
		
		//==========================================================================================
		//
		//	скрыть панель от всех, кроме админов
		//
		//==========================================================================================
		
		public function hide_panel_handler()
		{
			if (!current_user_can('manage_options'))  show_admin_bar(false);
		}
	
		// This function removes each menu item using the Page Hook Suffix ( http://codex.wordpress.org/Administration_Menus#Page_Hook_Suffix )
		public function remove_menus() 
		{
			// Links page
			remove_menu_page( 'link-manager.php' );
			// Tools page
			remove_menu_page( 'tools.php' );
			// Settings page
			remove_menu_page( 'options-general.php' );
		}
		function enable_errors()
		{
			if( $GLOBALS['user_level'] < 5 )
				return;
			error_reporting(E_ALL ^ E_NOTICE);
			ini_set("display_errors", 1);
		}
		
		// This function removes dashboard widgets
		public function remove_dashboard_widgets() 
		{
			// Remove each dashboard widget metabox for Incoming Links, Plugins, the WordPress Blog and Other WordPress News
			remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');
			remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
			remove_meta_box('dashboard_primary', 'dashboard', 'core');
			remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
			remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
			remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
			remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
			remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
			remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
			 
		}
		/* Логи ошибок в виджете админ-панели, в консоли
		--------------------------------------------------------------------- */
		function slt_PHPErrorsWidget() {
			$logfile = $_SERVER['DOCUMENT_ROOT'] . '/../error_log'; // Полный пусть до лог файла
			$displayErrorsLimit = 100; // Максимальное количество ошибок, показываемых в виджете
			$errorLengthLimit = 300; // Максимальное число символов для описания каждой ошибки
			$fileCleared = false;
			$userCanClearLog = current_user_can( 'manage_options' );

			// Очистить файл?
			if( $userCanClearLog && isset( $_GET["slt-php-errors"] ) && $_GET["slt-php-errors"]=="clear" ){
				$handle = fopen( $logfile, "w" );
				fclose( $handle );
				$fileCleared = true;
			}
			// Читаем файл
			if( file_exists( $logfile ) ){
				$errors = file( $logfile );
				$errors = array_reverse( $errors );
				if ( $fileCleared ) echo '<p><em>Файл очищен.</em></p>';
				if ( $errors ) {
					echo '<p> Ошибок: '. count( $errors ) . '.';
					if ( $userCanClearLog ) 
						echo ' [ <b><a href="'. admin_url() .'?slt-php-errors=clear" onclick="return confirm(\'Вы уверенны?\');">Очистить файл логов</a></b> ]';
					echo '</p>';
					echo '<div id="slt-php-errors" style="max-height:500px; overflow:auto; padding:5px; background-color:#FAFAFA;">';
					echo '<ol style="padding:0; margin:0;">';
					$i = 0;
					foreach( $errors as $error ){
						echo '<li style="padding:2px 4px 6px; border-bottom:1px solid #ececec;">';
						$errorOutput = preg_replace( '/\[([^\]]+)\]/', '<b>[$1]</b>', $error, 1 );
						if( strlen( $errorOutput ) > $errorLengthLimit ){
							echo substr( $errorOutput, 0, $errorLengthLimit ).' [...]';
						}
						else
							echo $errorOutput;
						echo '</li>';
						$i++;
						if( $i > $displayErrorsLimit ){
							echo '<p><em>Набралось больше чем '. $displayErrorsLimit .' ошибок в файле...</em></p>';
							break;
						}
					}
					echo '</ol></div>';
				}
				else
					echo '<p>Ошибок нет!</p>';
			}
			else
				echo '<p><em>Произошла ошибка чтения лог файла.</em></p>';
		}
		// Добавляем виджет
		function slt_dashboardWidgets(){
			//if(current_user_can("manage_options"))
				wp_add_dashboard_widget( 'slt-php-errors', 'PHP errors', array($this, 'slt_PHPErrorsWidget') );
		}
		
		/* php в постах или страницах WordPress: [exec]код[/exec]
		----------------------------------------------------------------- */
		function exec_php($matches){
			$my_post = get_post();
			if(!is_super_admin($my_post->post_author)) return;
			eval('ob_start();'.$matches[1].'$inline_execute_output = ob_get_contents();ob_end_clean();');
			return $inline_execute_output;
		}
		
		public function inline_php($content){
			//$content = preg_replace_callback('/\[exec\]((.|\n)*?)\[\/exec\]/', 'exec_php', $content);
			//$content = preg_replace('/\[exec off\]((.|\n)*?)\[\/exec\]/', '$1', $content);
			return $content;
		}
		
		//==================================================
		//
		//	хлебные крошки Локаций
		//
		//==================================================
		
		public function the_breadcrumb($term_id) 
		{
			global $Soling_Metagame_Constructor;
			$loc			= SMC_Location::get_instance($term_id);
			$d				= $Soling_Metagame_Constructor->get_location_type($term_id);
			$text			=  "<a href='' class='breadcrumb'>".$d->picto." ".$loc->name."</a>";
			$i				= 0;
			while( $temp 	= wp_get_term_taxonomy_parent_id( $term_id, SMC_LOCATION_NAME )) 
			{
				if(++$i>2)	
				{
					$text	= " ... <span class='lp-yellow-text'><i class='fa  fa-angle-double-right'></i></span>  ".$text;
					break;
				}
				$term_id 	= $temp;
				$term		= SMC_Location::get_instance($temp);//get_term_by("term_id", $temp, SMC_LOCATION_NAME);
				$d			= $Soling_Metagame_Constructor->get_location_type($temp);
				$text		= '<a href="'.SMC_Location::get_term_link($temp) . '">' . $d->picto . " " . $term->name."</a> <span class='lp-yellow-text'><i class='fa fa-angle-double-right'></i></span> ".$text;
				//$text		= '<a href="javascript:void(0);" onclick="open_metagame_panel('."'locations'".', '.$temp.')">' . $d->picto . " " . $term->name."</a> <span class='lp-yellow-text'><i class='fa fa-angle-double-right'></i></span> ".$text;
				
			}	
			$text			= "<a href='/' style='font-size:1.2em;' class='breadcrumb'><i class='fa fa-home'></i></a> <span class='lp-yellow-text'><i class='fa fa-angle-double-right'></i></span> " ;
			if($term_id == 0)
			{
				$text		.= apply_filters("smc_top_menu_position", "");
			}			
			return $text;
		}
		function my_admin_bar_render() 
		{
			if(!current_user_can('administrator'))	return;
			global $wp_admin_bar;
			$wp_admin_bar->add_menu( array(
				'parent' 	=> false, //'false' для корневого меню
							   //или ID нужного меню
				'id' 		=> 'metagame', // ID ссылки
				'title' 	=> __('Metagame', 'smc'), //заголовок ссылки
				'href' 		=> "/wp-admin/admin.php?page=metagame" //имя файла	
				,'icon' 	=> 'dashicons-welcome-learn-more'
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'metagame', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'metagame_panel', // ID ссылки
				'title' => __('Main Metagame Panel', 'smc'), //заголовок ссылки
				'href' => "/wp-admin/admin.php?page=smc_location_menu_sub_menu" //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'metagame', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'location_panel', // ID ссылки
				'title' => __('Locations', 'smc'), //заголовок ссылки
				'href' => "/wp-admin/edit-tags.php?taxonomy=".SMC_LOCATION_NAME //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'metagame', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'direct_message_panel', // ID ссылки
				'title' => __('Direct Messages', 'smc'), //заголовок ссылки
				'href' => "/wp-admin/edit.php?post_type=direct_message" //имя файла	
			));
		}
 
		// включаем фильтр, если у пользователя нет прав на редактирование чужих постов, то есть он либо автор, либо участник, либо подписчик
		function alien_comment_hide_handler()
		{
			if(!current_user_can('edit_others_posts')) 
			{
				add_filter('comments_clauses', 'true_get_comments_by_user_posts');
			}
		}
		
		function change_editor_font(){
			echo "<style type='text/css'>
			#editorcontainer textarea#content {
				font-family: Verdana, monospace;
				font-size:24px;
				color:#333;
				}
			</style>";
		}
		function my_user_contactmethods($user_contactmethods)
		{
			unset($user_contactmethods['yim']);
			unset($user_contactmethods['aim']);
			unset($user_contactmethods['jabber']);
			
			$user_contactmethods['modilephone'] = __('Mobile Phone Number', "smc");
			$user_contactmethods['vk'] 			= __('VKontakte Username', "smc");
			$user_contactmethods['skype'] 		= __('Skype Username', "smc");
			$user_contactmethods['oovoo'] 		= __('Oovoo Username', "smc");
			return $user_contactmethods;
		}
		
		// redirect to prevous after log out	
		function logout_redirect_to_home() 
		{
			if ($_GET['loggedout'] == 'true') {
				wp_safe_redirect(get_bloginfo('url')); 
				//echo get_bloginfo('url');
				//exit;
			}
		}
		
		//
		// http://wpandyou.ru/tutorials/uroki-wordpress/otobrazhaem-id-vlozheniya-kartinki-v-admin-panele-wordpress-v-razdel-mediafajly/
		
		function posts_columns_attachment_id($defaults){
			$defaults['wps_post_attachments_id'] = __('ID');
			return $defaults;
		} 
		function posts_custom_columns_attachment_id($column_name, $id){
			if($column_name === 'wps_post_attachments_id'){
			echo $id;
			}
		}
		
		
		
	//====================================
	//
	//	Добавление полей в редактор постов
	//
	//====================================

	

	function add_slide_show_to_post() {
		add_meta_box( 'extra_fields', __("Add slideshows or youtube video", "smc"), array($this, 'add_slide_show_to_post_box_func'), 'post', 'normal', 'high'  );
	}

	function add_slide_show_to_post_box_func($post=null)
	{
		$ID		= !$post ? 0 : $post->ID;
		$slides	= get_post_meta($ID, 'slides', 1);
		?>	
		<form method="post" id="form">
			<div style="display:inline-block;width:100%;">
			<div style="width:100%; margin:10px 0 0 0;">
					<label><b>СЛАЙД-ШОУ</b></label>
			</div>
			<div>
				<div style="width:20%; float:left;">
					<label>название:</label>
				</div>
				<div>
					<input type="text" name="extra[slide_title]" value="<?php echo get_post_meta($ID, 'slide_title', 1); ?>" style="width:70%" />					
				</div>
			</div>
			<div>
				<div style="width:20%; float:left;">
					<label>ссылка на 1 кадр:</label>
				</div>
				<div>
					<input type="text" name="extra[slide_0]" value="<?php echo $slides[0]; ?>" style="width:70%" />
				</div>
			</div>
			<div>
				<div style="width:20%; float:left;">
					<label>ссылка на 2 кадр:</label>
				</div>
				<div>
					<input type="text" name="extra[slide_1]" value="<?php echo $slides[1]; ?>" style="width:70%" />				
				</div>
			</div>
			<div>
				<div style="width:20%; float:left;">
					<label>ссылка на 3 кадр:</label>
				</div>
				<div>
					<input type="text" name="extra[slide_2]" value="<?php echo $slides[2]; ?>" style="width:70%" />					
				</div>
			</div>
			<div>
				<div style="width:20%; float:left;">
					<label>ссылка на 4 кадр:</label>
				</div>
				<div>
					<input type="text" name="extra[slide_3]" value="<?php echo $slides[3]; ?>" style="width:70%" />					
				</div>
			</div>
			<div style="width:70%; margin:10px 0 0 0;">
					<label><b>ИЛИ</b></label>
			</div>
			<div>
				<div style="width:70%; float:left;">
					<label>ссылка на ролик youtube.com: </label>
				</div>
				<div>
					<input type="text" name="extra[youtube_id]" value="<?php echo get_post_meta($ID, 'youtube_id', 1); ?>" style="width:70%" />					
				</div>
			</div>
			<div>
				<div style="width:70%; float:left;">
					<input type="checkbox"  class="css-checkbox" id="important_post" name="important_post" <?php checked(1, get_post_meta($ID, 'important_post', 1)); ?>"/>					
					<label for="important_post"  class="css-label"><?php _e("Important Post", "smc");?></label>
				</div>
			</div>
		</div>
		</form>
		<?php
	}
	
	function my_extra_fields_update( $post_id )
	{					
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return $post_id;
		if( !isset($_POST['extra']) ) 						return $post_id; 
		
		$_POST['extra'] = array_map('trim', $_POST['extra']);
		foreach( $_POST['extra'] as $key=>$value )
		{       
			if( empty($value) )
			{
				delete_post_meta($post_id, $key);
				continue ;			
			}	
			switch($key)			
			{
				case "youtube_id":
					update_post_meta($post_id, $key, $this->get_youtube_id_from_url($value));
					break;
				case "important_post":
					update_post_meta($post_id, "important_post", 1);
					break;
				default:
					update_post_meta($post_id, $key, $value);
			}
		}	
		$slides		= array();
		for($i=0; $i<4;$i++)
		{
			$slide	= $_POST['extra']['slide_'.$i];
			if($slide != "")	$slides[] = $slide;
		}
		if(count($slides))		update_post_meta($post_id,'slides', $slides);
		update_post_meta($post_id, "important_post", $_POST['important_post']== "on");
		
		//default trumbail
		global $Soling_Metagame_Constructor;
		$Soling_Metagame_Constructor->default_post_trumbail($post_id);
		return $post_id;
	}	
		
	
	// get youtube id from url
	// http://stackoverflow.com/questions/3392993/php-regex-to-get-youtube-video-id
	
	public static function get_youtube_id_from_url($url){
		if (stristr($url,'youtu.be/'))
		{ 
			preg_match('/(https|http):(\/\/www\.|\/\/)(.*?)\/(.{11})/i', $url, $final_ID); 			
			return $final_ID[4]; 
		}
		else if(stristr($url,'http'))
		{ 
			preg_match('/(https|http):(\/\/www\.|\/\/)(.*?)\/(embed\/|watch\?v=|(.*?)&v=|v\/|e\/|.+\/|watch.*v=|)([a-z_A-Z0-9]{11})/i', $url, $IDD); 
			//insertLog("get_youtube_id_from_url", $IDD);
			return $IDD[6]; 
		}
		else
			return $url;
	}
		
	// return array of all "img src" substrings
	// http://stackoverflow.com/questions/5721025/regular-expression-in-php-to-return-array-with-all-images-from-html-eg-all-src
	
	public static function get_all_image_string($str)
	{
		preg_match_all('/src="([^"]+)"/', $str, $arr, PREG_PATTERN_ORDER);
		$a = array();
		foreach($arr as $ar)
		{
			$a[]	= $ar[0];
		}
		
		return $arr[1];
	}
	
	
	function smc_location_archive_title($title, $location)
	{
		global $UAM;
		if(is_user_logged_in())
		{
			if($UAM->is_current_user_can_write($location->term_id))
			{
				$add_new_post_button	= "
				<div class='botton lp_bg_color_hover  smc-alert' id='add_new_post_button' style='margin:12px 0 1px 0;' target_name='add_new_post_message_win'>".
					__("Add new Post", "smc").
				"</div>
				<div id='add_new_post_message_win' style='max-width:300px; padding:10px; display:none;'>".
					$this->add_new_post_form().
				"</div>";
			}
			else
			{
				$add_new_post_button	= "<div class='smp-comment'>".__("You not member of this. You can read and conmmenting posts but not can create it.","smc")."</div>";
			}
		}
		else
		{
			$add_new_post_button	= "<div class='smp-comment'>".__("Log in please","smc")."</div>";
		}
		
		//if(!$this->options['use_panel']) 
		{
			global $UAM;
			global $Soling_Metagame_Constructor;
			
			$html				= '';
			if(is_category())
			{
				$html			.= $add_new_post_button;
			}
			else if(is_tax(SMC_LOCATION_NAME))
			{
				$loc			= SMC_Location::get_term_meta($location->term_id);
				if(!$UAM->isAccessLocation($location->term_id) )
				{
					$add_new_post_button	= "<div class='smp-comment'>".__("You have not rights to public a post.","smc")."</div>";
				}
				$html			.= '<div class="title_location_hiding_type">'.$Soling_Metagame_Constructor->get_location_hiding_name($location->term_id, null).'</div>';
			}
		}
		$ipost				= $this->get_last_important_post();
		$html				.= $add_new_post_button.'
		<div class="smc_important_post">
			<h3>'.$ipost->post_title.'</h3>'.
			$ipost->post_content.
		'</div>';
		return $html;
	}
	function get_last_important_post()
	{
		//last important post
		$args				= array(
										'numberposts' 	=> 1,
										'post_type'		=> "post", 
										'orderby'		=> "ID",
										'post_status' 	=> 'publish', 
										'order'     	=> 'DESC',
										'meta_query' 	=> array(
																 array(
																			'key'	=> "important_post",
																			'value'	=> 1,
																			'compare'=>"="
																		)
																)
									);
		
		$html				= '';
		if(is_category())
		{
			$args['category__in'] = array(get_category(get_query_var('cat'),false)->term_id);
		}
		else if(is_tax(SMC_LOCATION_NAME))
		{
			$cur_term		= get_term_by('name', single_term_title("", 0), SMC_LOCATION_NAME);
			$args['tax_query'] = array(
																array(
																		'taxonomy' => SMC_LOCATION_NAME,
																		'field' => 'id',
																		'terms' =>  $cur_term->term_id
																	)
															);
		}
		
		$iposts				= get_posts($args);
		$ipost				= $iposts[0];
		return $ipost;
	}
	function the_title($title, $id)
	{
		if($this->options['use_panel'])
			return '';
	}
	
	function the_content($content)
	{
		return $this->pre_content().$content;
	}
	function pre_content()
	{		
		global $post;
		$dop		= " "; 
		
		$youtube_id 		= get_post_meta($post->ID, 'youtube_id', true);		
		$slide_title		= get_post_meta($post->ID, 'slide_title', true);
		$slide_0 			= get_post_meta($post->ID, 'slide_0', true);
		$slide_1 			= get_post_meta($post->ID, 'slide_1', true);
		$slide_2 			= get_post_meta($post->ID, 'slide_2', true);
		$slide_3 			= get_post_meta($post->ID, 'slide_3', true);
		if($youtube_id)
		{
			$dop			.= '
			<div class="archive-post-video lp-left-border-color  smc-submit smc-alert" target_name="wideo_player-'.$post->ID.'" params="width:640px; max-width:640px;"  style="background-image:url(http://img.youtube.com/vi/' . $youtube_id . '/0.jpg); background-size:cover; background-position:center;">
				
			</div>
			<div class="lp-hide" id="wideo_player-'.$post->ID.'">
				<h3>'.$post->post_title.'</h3>
				<iframe width="640" height="360" src="//www.youtube.com/embed/' . $youtube_id . '" frameborder="0" allowfullscreen></iframe>
			</div>';
		}
		else if($slide_0)
		{
			$dop			.= '<div style="display:inline-block;">';		
			if($slide_0)
				$dop		.= '<a href='.$slide_0.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb" style="background-size:cover; background-image:url('.$slide_0.')" title="'.$slide_title.'"></div></a>';	
			if($slide_1)
				$dop		.= '<a href='.$slide_1.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb" style="background-size:cover; background-image:url('.$slide_1.')" title="'.$slide_title.'"></div></a>';		
			if($slide_2)
				$dop		.= '<a href='.$slide_2.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb" style="background-size:cover; background-image:url('.$slide_2.')" title="'.$slide_title.'"></div></a>';		
			if($slide_3)
				$dop		.= '<a href='.$slide_3.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb" style="background-size:cover; background-image:url('.$slide_3.')" title="'.$slide_title.'"></div></a>';
			$dop			.= '</div>';
		}
		return $dop;
	}
	static function get_your_label($bottom=0, $right=0)
	{
		return "<div class='smc-your-label' style='bottom:".$bottom."px; right:".$right."px;'>".__("It's your", "smc")."</div>";
	}
	static function get_not_your_label($bottom=0, $right=0)
	{
		return "<div class='smc-notyour-label' style='bottom:".$bottom."px; right:".$right."px;'>".__("It's not your", "smc")."</div>";
	}
	public static function get_short_your_label($size=12, $shift=0)
	{
		if(!is_array($shift))	$shift	= array($shift, $shift);
		return "
		<div class='hint hint--top' data-hint='".__("It's your", "smc")."' style='position:absolute; display:inline-block; bottom:".$shift[0]."px; right:".$shift[1]."px; width:".$size."px; height:".$size."px;'>
			<svg width=$size height=$size xmlns='http://www.w3.org/2000/svg'>
				<g>
					<path fill='#00FF00' d='M0 $size L$size $size $size 0 0 $size'/>
				</g>
			</svg>
		</div>";
	}
		
		public static function calculate_svg_path($data)
		{
			if(isset($data))
			{
				$src				= explode("," , $data);
				$cnt				= count($src);
				$range_odd 			= array();
				foreach (range(0, $cnt, 2) as $number) {
					$range_odd[]	= $number;
				}
				$range_even			= array();
				foreach (range(1, $cnt, 2) as $number) {
					$range_even[]	= $number;
				}				
				
				function odd($var) {
					return ($var % 2 == 1);
				}

				function even($var) {
					return ($var % 2 == 0);
				}
				
				$dd					= array_flip($src);
				$odd				= array_flip(array_filter($dd, "odd"));
				$even;
				return ;
			}
			else
				return "-- ";
		}
		
		
		
		
		public static function log($text)
		{
			$filename					= SMC_REAL_PATH. "log.log";
			//echo  $filename."<BR>";
			if (is_writable($filename)) 
			{
				if(!$handle				= fopen($filename , "a+"))
				{
					//echo "error open log-file<br>";
				}
				//echo "START WHRITE: <BR>";
				if(fwrite($handle, $text.'<br>') === false)
				{
					//echo "error white text<br>";
				}
				else
				{
					//echo "success whrite\n";
				}
				fclose($handle);
			}
			else
			{
				//echo "Файл $filename недоступен для записи";
			}
		}
		
		
		public function add_new_post_form()
		{
			if(is_category() || is_tax())
			{
				if ( is_user_logged_in() ) 
				{
					
				} 
				else 
				{ 
					//echo "Авторизуйтесь, пожалуйста"; 
					$output .= ob_get_clean();
					$output .= $after_widget."\n";
					echo $output;
					return;
				}
			}
			else
			{
				return "";
			}
			return 
				'<h4>'.__("Add new Post", "smc").'</h4>' .
				$this->add_new_post_form_content();
		}
		public function add_new_post_form_content()
		{
			
			$html	= '
			<div rid="new_post_modal">
					<form name="new_post1" method="post"  enctype="multipart/form-data" id="new_post_form_modal">
						<div style="margin:0; display:inline-block; height:100%; width:100%;">											
							<div class="" style="">
								<div>
									<input type="text" name="new_post_title_2" id="new_post_title_2" placeholder="'.__("Title").'" class="lp_input" />
								</div>
								<div>
									<textarea rows="4" id="new_post_text_2" name="new_post_text_2" maxlengt="140"  placeholder="'.__("Text").'" class="lp_input" style="height:105px;"></textarea>
								</div>
							</div>
							<div class="" style="">							
								<div>
									<div class="ermak_login_picto">';
			if($this->options['public_content']['image'])
			{
				$html	.= 			'	<div class="lp-button-sector hint hint--top" data-hint="'.__("slideshow", "smc").'" iid="1">
											 <img src="'.SMC_URLPATH.'img/photo_ico.png">
										</div>';	
			}
			if($this->options['public_content']['youtube'])
			{
				$html	.= 				'<div class="lp-button-sector  hint hint--top" data-hint="'.__("video", "smc").'" iid="2">
											 <img src="'.SMC_URLPATH.'img/youtube_icon.png">
										</div>';
			}
			if($this->options['public_content']['audio'])
			{
				$html	.= 				'<div class="lp-button-sector hint hint--top" data-hint="'.__("audio", "smc").'"  iid="3">
											 <img src="'.SMC_URLPATH.'img/audio_ico.png">
										</div>';
			}
			if($this->options['public_content']['local_video'])
			{
				$html	.= 				'<div class="lp-button-sector hint hint--top" data-hint="'.__("local video", "smc").'"  iid="3">
											 <img src="'.SMC_URLPATH.'img/video_ico.png">
										</div>';
			}
			$switch		= 	self::get_switcher(
									array(
										array(
												'title'	=> __("from internet", "smc"),
												'slide'	=> '<div>
																<input type="text" id="slide_nn1" class="new_dm_slide" slide_id="0" placeholder="'. sprintf(__('put url for %s slide', 'smc'), 1) .'"/>
																<p></p>
																<input type="text" id="slide_nn2"  class="new_dm_slide" slide_id="1" placeholder="'.sprintf(__('put url for %s slide', 'smc'), 2).'"/>
																<p></p>
																<input type="text" id="slide_nn3"  class="new_dm_slide" slide_id="2" placeholder="'.sprintf(__('put url for %s slide', 'smc'), 3).'"/>
																<p></p>
																<input type="text" id="slide_nn4"  class="new_dm_slide" slide_id="3" placeholder="'.sprintf(__('put url for %s slide', 'smc'), 4).'"/>
															</div>'
											 ),
										array(
												'title'	=> __("from local computer", "smc"),
												'slide'	=> '<div>
																<input type="file" multiple name="upload_files[]" id="upload_files_nn"/>
															</div>'
											 )
										 ),
									"pics"
								);
			$html	.= 		'</div>															
							<div class="lp_new_post_meta0" id="nn_n1">
								<div>'.
								__("add urls of pictures", "smc").
								'</div>'.
								'	
								<div>
									<input type="file" multiple name="upload_files[]" id="upload_files_nn"/>
								</div>
							</div>
							
							<div class="lp_new_post_meta0" id="nn_n2">'.
								__('put youtube url', 'smc').
								'<p></p>
								<input type="text" id="youtube_nn" name="youtube_nn" placeholder="'.__('put youtube url', 'smc').'"/>
								<p></p>
							</div>															
							<div class="lp_new_post_meta0" id="nn_n3">'.
								__('add audio', 'smc').
								'<input type="text" id="audio_file_nn" placeholder="'.__("put mp3 url", "smc").'"/>
								<p></p>
								<!--input type="file" id="audio_file_nn" name="audio_file"/-->
								<p></p>
							</div>
						</div>
								
								<center style="padding-top:20px;">								
										<span class="botton" id="add_new_post_button_2" style="border:none; padding:15px;">'.
											__("Create Post", "smc").
										'</span>
								</center>
							</div>											
						</div>
					</form> 
				</div>';
			return $html;
			
		}
		public static function get_call_to_owner_hinter($owner_id, $message)
		{
			return " 
			<div class='smc_call_to_owner hint hint--left' owner_id='$owner_id' message='$message' data-hint='".__("Press to call owners","smc")."'>
				<i class='fa fa-envelope'></i>
			</div>";
		}
		
		
		public function get_div_hint_helper($div, $id="", $color=-1, $style="")
		{
			if($color != -1)
			{
				$colorr = "style='color:".$color."'";
			}
			return "<span class='smc-question div_hinter' style='$style' data-hint='".$id."'><i class='fa fa-question-circle' $colorr></i></span>".
			"<div class=lp-hide id='".$id."'>".$div."</div>";
		}
		public function get_hint_helper($text, $height=100, $color=-1, $style="")
		{
			if($color != -1)
			{
				$colorr = "style='color:".$color."'";
			}
			return "<span class='smc-question hinter' style='$style' data-hint='".$text."' height=".$height."><i class='fa fa-question-circle' $colorr></i></span>";
		}
		static function get_instruction_sign($div, $id="instr", $color=-1, $added_class="")
		{
			if($color == -1) $color = "#666";
			$targ		= "targ_".$id;
			return "
			<div id='$id' class='instruction_sign $added_class smc-alert'  target_name='$targ'>
				<i class='fa fa-question'></i>
			</div>
			<div id='$targ' class='lp-hide'>
				$div
			</div>
			";
		}
		
		public function get_users_drop_list($params="")
		{
			if($params == "")
				$params 	= array('class'=>'', 'name'=>'users');
			$html			= "<select ";
			if($params['name'])
				$html		.= "name='".$params['name']."' ";
			if($params['class'])
				$html		.= "class='".$params['class']."' ";
			if($params['multiple'])
			{
				$html		.= "multiple='multiple'";				
			}
			$selecteds		= $params['selected'] ? $params['selected'] : array();
			
				
			$html			.= " ><option value='-1'>---</option>";
			$all_users		= get_users(array("fields"=>"all", 'orderby' => "display_name", 'order' => "ASC"));
			foreach($all_users as $user)
			{
				$selected = "";
				for($i=0; $i<count($selecteds); $i++)
				{
					if($selecteds[$i]==$user->ID)
					{
						$selected	=  "selected";
						break;
					}
				}
				$html		.= "<option value='" .$user->ID . "' ".selected($user->ID, $params['selected']) ." ". $selected ."> " . $user->display_name . "</option>";
			}
			$html			.= "</select>";
			return $html;
		}
		
		
		// get current URL
		static function get_current_URL() 
		{
			$current_url  = 'http';
			$server_https = $_SERVER["HTTPS"];
			$server_name  = $_SERVER["SERVER_NAME"];
			$server_port  = $_SERVER["SERVER_PORT"];
			$request_uri  = $_SERVER["REQUEST_URI"];
			if ($server_https == "on") $current_url .= "s";
			$current_url .= "://";
			if ($server_port != "80") $current_url .= $server_name . ":" . $server_port . $request_uri;
			else $current_url .= $server_name . $request_uri;
			return $current_url;
		}
		
		//
		static function get_wait_form( $height="100px" )
		{
			return '
			<div class="lp_wait_form" style="height:'.$height.';">
				<div class="lp_wait_spin">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
			</div>';
		}
		//
		static function get_tabs($tabs, $id_prefix="")
		{
			$html			= '
			<div class="tab_container" id="cont_'.$id_prefix.'">
				<div class="tab_header">
					<div class="tab_bckgrnd">					
					</div>
					<div style="display:inline-block; position:absolute; bottom:0; left:0;">';
			$i=0;
			foreach($tabs as $tab)
			{
				$name		= isset($tab['name'])	? ' name="'.$tab['name'].'" ' : "";
				if(isset($tab['hint']))
					$hint_class		= 'hint hint--top';
				if(isset($tab['exec']))
					$tab_ex		= ' exec="'.$tab['exec'] . '" args="'.$tab['args']. '" ';
				if(isset($tab['title']))
					$html	.= '<div '.$name.' is_load="" id="btn_'.$id_prefix.$i.'" ' . $tab_ex . ' id_prefix="'.$id_prefix.'" class="tab_button '.$hint_class.'" tab_button_id="'.$i++.'" data-hint="'.$tab['hint'].'">'.$tab['title'].'</div>';
			}
			$html			.= '
					</div>
				</div>
				<div class="tab_body">';
			$i=0;
			foreach($tabs as $tab)
			{
				if(isset($tab['slide']))
					$html	.= '<div id="tab_'.$id_prefix.$i.'" class="tab_slide" n="'.$i++.'">'.$tab['slide'].'</div>';
			}
			$html			.= '
				</div>
			</div>';
			return $html;
		}
		
		static function get_switcher($tabs, $name="sw", $params = "")
		{	
			if($name=="")	$name = "sw";
			if(!is_array($params))	$params	= array();
			$inset_id		= (int)$params['slide'];
			$html			= '
			<div class="smc_switcher_container" id="' . $name . '"' . $params['name'] .' inset_id="'.$inset_id.'">
				<div class="smc_switcher_head" >';
			$t 				= array();
			if(!count($tabs))	return '';
			foreach($tabs as $tab)
			{
				if(!is_array($tab))	continue;
				if($tab['title'] != '')
					$t[]	= $tab;
			}
			for($i=0; $i<count($t); $i++)
			{
				if($t[$i]['title'] != '')
				{
					$edge		= '';
					if($i == $inset_id)	
					{
						$edge	= 'smc_sw_right smc_switch_button_release';
					}
					else if($i==count($t)-1)	
						$edge	= 'smc_sw_left';
					if(isset($t[$i]['hint']))
						$hint_class		= ' hint="'.$tabs[$i]['hint'].'" ';
					$label		= $t[$i]['label']	? " lablel='" . $t[$i]['label']  . "' " : "";
					$act		= $t[$i]['exec']	? " exec='" . $t[$i]['exec'] . "' args='" . $t[$i]['args'] ."' " : "";
					$nm			= $t[$i]['name']	? " name='"   . $t[$i]['name']. "' " : "";
					$html		.= ' 
					<div class="smc_switch_button ' . $params['class']. " ". $edge . '"  id="btn_' . $name.$i . '" sw_name="' . $name . '" sw_i="'.$i.'" '.$hint_class. $act . $label . $nm . '>'.
						$t[$i]['title'].
					'</div>';
				}
			}
			$html			.= '
				</div>
				<div class="smc_switcher_body">';
			for($i=0; $i<count($t); $i++)
			{
				if(!isset($t[$i]['slide']))	continue;
				$vis		= '';
				if($i == $inset_id)		$vis = ' style="display:block;"';
				
				$nm			= $t[$i]['name']	? " name='slide_"   . $t[$i]['name']. "' " : "";
				$html		.= '
				<div class="smc_switch_slide" id="slide_'.$name.$i.'" sw_name="'.$name.'" sw_i="'.$i.'" '.$vis.'" '. $nm	.' style="width:100%;">'.
					$t[$i]['slide'].
				'</div>';
			}
			$html			.='
				</div>
			</div>
			';
			return $html;
		}	

		static function get_lists( $tabs, $name="sl", $params = "", $current_tab=0 )
		{
			if($name == "")		$name = "sl";
			$t 					= array();
			if(!count($tabs))	return ' -- ';
			foreach($tabs as $tab)
			{
				if(!is_array($tab))	continue;
				if($tab['title'] != '')
					$t[]	= $tab;
			}
			$col0			= "<select>";
			for($i=0; $i<count($t); $i++)
			{
				if($t[$i]['action'])
				{
					$action	= " action='".$t[$i]['action']."' args='".$t[$i]['args']."' ";					
				}
				$col0		.= "<option value=$i " . selected($i, $current_tab, 0) . " >" . $t[$i]['title'] . "</option>";
				$col1		.=  "<div $action choosen class='smp-pr-title lp-personal-message-title smc-myloc-btn' style='text-align:right;font-weight:normal; display:inline-block; position:relative; ' id='production-btn-$name' button_id=$i><span style='opacity:1;'>" . $t[$i]['title'] . "</div>";
				
			}	
			$col0				.= "</select>";			
			$html				.= "<div class='smp-production-container' id='$name' $params>
				<div id='col0' class='smp-col0'>".
					$col0 .
				"</div>
				<div id='col1' class='smp-cl1'>" . 
					$col1.
				"</div>
				<div id='col2' class='smp-cl2'  style=''>
					<form method='post'  enctype='multipart/form-data'  id='form'>"; 			
			for($i=0; $i<count($t); $i++)
			{
				$style			= $i == $current_tab ?  " style='display:block;'" : " style='display:none;'";
				$html			.= "<div class='smp-pr-main' id='production-$id' button_id='".$i."' factory_id='$id' $style>";				
				$html			.= $t[$i]['slide'];
				$html			.= '</div>';
			}
			$html				.= '
							</form>						
						</div>	
					</div>';
			return $html;
		}
		
		//
		function smc_login_form()
		{
			$is_logged_in = intval(is_user_logged_in());
			$auth_url = SMC_URLPATH."auth/index.php";

			return '
			<div  id="login_form" style="display:none">
				<h3>'.__("Metaversitet log in", "smc").'</h3>'.
			'	<iframe src="http://metaversity.ru/iframe/userbar/wordpress/?auth_url='.$auth_url.'&is_logged_in='.$is_logged_in.'" width="320" height="200" frameborder="0"></iframe>
			</div>';
			
		}
		function smc_register_form()
		{
			return '
			<div  id="register-form" style="display:none; margin-bottom:20px;">
				<h3>'.__("Metaversitet register", "smc").'</h3>
				<div style="display:inline-block;>"'.
				'	<div>'.__("Our Project taked refuge of Metaversity project. Only Metaversitet's members followed we. You must register in Metaversity and return here automatically.<BR>If you already have the Metaversity account, close this window and press ENTER button.", "smp").'</div>
					<a href="'.SMC_URLPATH.'auth/index.php?method=register&return_to=">
						<div class="black_button_2" style="margin:20px;">
						'.
							__("Register").
						'</div>
					</a>
				</div>
			</div>
			';
		}
		function loginout()
		{
			echo '
			<a href="'.SMC_URLPATH.'auth/index.php?method=logout&return_to='.site_url().'/?login=failed">'.__('Log out').'</a>';
		}
		
		function wp_login($user_login)
		{
			$user	= get_user_by("login", $user_login);
			if(!$iface_fill = get_user_meta($user->ID, 'iface_fill'))
			{
				update_user_meta($user->ID, 'iface_fill', "#009900");
			}
		}
		function admin_default_page()
		{
			 return '/';
		}
		
		//main metagame panel menu
		static function init_main_mg_menu()
		{       
			global $Soling_Metagame_Constructor;
			// return;
			if(!has_nav_menu("Main_metagame"))
			{
				$menu_id2 = wp_create_nav_menu( 'Main_metagame' );
				$menu2 = array( 
								'menu-item-type' 	=> 'custom', 
								'menu-item-url' 	=> '/',
								'menu-item-title' 	=> "<i class='fa fa-home'></i>", 
								'menu-item-status' 	=> 'publish' 
							);
				wp_update_nav_menu_item( $menu_id2, 0, $menu2 );
			}
			if (!has_nav_menu('main_metagame_menu'))
			{
				$menu_id = wp_create_nav_menu( 'main_metagame_menu' );
				error_log('-1 ' . var_export($menu_id, 1));
				$menu = array( 
								'menu-item-type' 	=> 'custom', 
								'menu-item-url' 	=> '/?page_id='. $Soling_Metagame_Constructor->options['about_the_project_page_id'],
								'menu-item-title' 	=> __('About the project', 'smc'), 
								'menu-item-status' 	=> 'publish' 
							);
				wp_update_nav_menu_item( $menu_id, 0, $menu );
				$menu1 = array( 
								'menu-item-type' 	=> 'custom', 
								'menu-item-url' 	=> '/?page_id='.$Soling_Metagame_Constructor->options['rules_page_id'],
								'menu-item-title' 	=> __('Rules and rights', 'smc'), 
								'menu-item-status' 	=> 'publish' 
							 );
				wp_update_nav_menu_item( $menu_id, 0, $menu1 );
				
				
			}
		}
		function theme_register_nav_menu()
		{
			register_nav_menu( 'main_metagame_menu', __("Main Metagame Menu", "smc") );
		}
		function smc_lp_main_menu()
		{
			$args = array(
						  'theme_location'  => 'main_metagame_menu',
						  'menu'            => 'main_metagame_menu', 
						  'echo'			=> false,
						  'menu_class'		=> "lp-location-exec-button lp_transparent",
						  'container'       => 'span',
						  );
		
			return wp_nav_menu($args);
		}
		static function get_widget_location_form($location_id)
		{
			$form			= "
			<div>
				
			</div>
			";
			return apply_filters("smc_widget_location_form", $form, $location_id);
		}
		function smc_get_user_avatar()
		{
			$is_avatar	= is_plugin_active('user-access-manager/user-access-manager.php');
			$avatar		= get_avatar(get_current_user_id(), 100);
			$html		.= "<div id=avatar_reg><div>".$avatar."</div>";
			$html		.= $is_avatar ? "<input type='file' name='avatar_pic' ><div class='avatar_help'>".__("Choose avatar image. Max size: 100x100 pixels.", "smc")."</div>" : "";
			$html		.= "</div>";
			return $html;
		}
		function smc_register_action($mess, $new_user_id, $datas)
		{
			return $mess;
			$is_avatar	= is_plugin_active('user-access-manager/user-access-manager.php');
			if($is_avatar)
			{
				$plugin_avatars		= get_option("plugin_avatars");
				$uploadfile			= $plugin_avatars['upload_dir'].'/'. basename($_FILES['avatar_pic']['name']);
				if ( copy($_FILES['avatar_pic']['tmp_name'],  ABSPATH . $uploadfile) ) 
				{
				}
				update_user_meta($new_user_id, 'avatar', $uploadfile );
			}
			return $mess;
		}
		function after_user_refgister($user_id)
		{
			$user					= get_user_by('id', $user_id);
			
			$creds = array();
			$creds['user_login'] 	= $user['user_login'];
			$creds['user_password'] = $user['user_pass'];
			$creds['remember'] 		= false;
			$usera					= wp_signon( $creds, false );
			if ( is_wp_error($usera) )
				echo $usera->get_error_message();
		}
		
		static function add_new_location_widget($location_id , $hash=1)
		{
			global $Soling_Metagame_Constructor;
			return '
			<div>
				<div style="position:relative; display:inline-block; width:100%;">
					<div>
						<board_title>'. __("Set a type of Location", "smc").'</board_title>'.
						$Soling_Metagame_Constructor->wp_dropdown_location_type(array("name"=>"nl_lt", "class"=>"lp_nl_lt")).
					'</div>
					<div>
						<div style=" margin:0 0 25px 0;">
							<board_title>'. __("Set a Title", "smc").'</board_title>
							<input type="text" placeholder:"'.__("Title").'" name="new_location_title" style="-webkit-border-radius: 30px; border-radius: 30px; height:30px; padding: 20px!important; font-size:20px!important;"/>
						</div>
					</div>
					<div>
						<p>
							<input type="radio" name="nl_ht'.$hash.'" id="is_locked1'.$hash.'" value="0" class="radio" checked="checked" />
							<label for="is_locked1'.$hash.'" >'.__('Public Location', 'smc').'</label>
						<br>
							<input type="radio" name="nl_ht'.$hash.'" id="is_locked2'.$hash.'" value="1" class="radio" />
							<label for="is_locked2'.$hash.'">'.__('Lock this location', 'smc').'</label>
						<br>														
							<input type="radio" name="nl_ht'.$hash.'" id="is_locked3'.$hash.'" value="2" class="radio" />
							<label for="is_locked3'.$hash.'">'.__('Hide this location','smc').'</label>
						</p>
					</div>
					<div>
						<div id="add_new_loc" loc_id="'.$location_id.'"  elt="' . __("Choose location type", "smc") . '"  enm="' . __("Choose title of new Location", "smc") . '" class="button smc_padding">'.
							__("Add New Location","smc").
						'</div>
					</div>
				</div>				
			</div>';
		}
		static function get_log($start_time = -1, $finish_time="'NOW'", $limit=1000)
		{
			global $table_prefix, $wpdb;
			$res	= $wpdb->get_results( "SELECT * FROM  `${table_prefix}ermak_log` WHERE ts <  $finish_time AND ts > $start_time LIMIT 0 , $limit", OBJECT );
			return $res;
		}
		static function clear_log()
		{
			global $table_prefix, $wpdb;
			$wpdb->query("Truncate table ". $table_prefix . "ermak_log");
		}
		
		static function object_property_args($class_name, $method, $args)
		{
			return call_user_func(array($class_name, $method), $args);
		}
		static function get_pagi_form($count, $offset, $max, $cont_name = "", $fun="")
		{
			if($max > $count)
			{
				$maximum = (int) ($max / $count);
				$html	.= "<div class='ermak_pagi_cont' fun='$fun' cont_name='$cont_name'>";
				$html	.= $offset > 0 ? "<span class='ermak_pagi_button' page_id='" . ($offset-1) ."'><i class='fa fa-chevron-left'></i></span>" : "";
				for($i = 0; $i < $maximum; $i++)
				{
					if( ( $i < $offset - 3 || $i > $offset + 3 ) && $i < $maximum - 1 && $i > 0 )
					{
						if(!isset($enbl))
						{
							$html .= "<span class='ermak_pagi_button' >..</span>";
							$enbl = true;
						}
						continue;
					}
					if($i == $offset)
					{
						$html	.= "<span class='ermak_pagi_cur'>". ($i+1)  . "</span>";
					}
					else
					{
						$html	.= "<span class='ermak_pagi_button' page_id='$i'>". ($i+1)."</span>";
					}
					unset($enbl);
				}
				$html	.=  $offset < $maximum - 1 ? "<span class='ermak_pagi_button' page_id='" . ($offset+1) ."'><i class='fa fa-chevron-right'></i></span>" : "";
				$html	.= '</div>';
			}
			return $html;
		}
		static function get_numeric($value, $param=-1, $max="", $min=0)
		{
			$value			= (int)$value;
			if(!is_array($params))	$params	= array();
			if($max == "") $max = $value;
			$html			= "
			<div class='smcnumeric " . $param['class'] . "' style='" . $param['style'] . "' id='" .$param['id']. "' min='$min' max='$max' arg1='".$param['arg1']."' arg2='".$param['arg2']."' name='".$param['name']."'>
				<div class='smcnumdn' data='$min'></div>
				<div class='smctxt' style=''>$value</div>				
				<div class='smcnumup' data='$max'></div>	
				<input type='hidden' value='$value' name='".$param['name']."'/>
			</div>	
			";
			return $html;
		}
		static function add_default_menu()
		{
			//wp_get_nav_menus(); - зарегистрированные менюшки 
			$menus = wp_get_nav_menus();
			return Assistants::echo_me( $menus );
		}
		
		static function HSVtoRGB(array $hsv, $return_str=true) 
		{
			list($H,$S,$V) = $hsv;
			//1
			$H *= 6;
			//2
			$I = floor($H);
			$F = $H - $I;
			//3
			$M = $V * (1 - $S);
			$N = $V * (1 - $S * $F);
			$K = $V * (1 - $S * (1 - $F));
			//4
			switch ($I) {
				case 0:
					list($R,$G,$B) = array($V,$K,$M);
					break;
				case 1:
					list($R,$G,$B) = array($N,$V,$M);
					break;
				case 2:
					list($R,$G,$B) = array($M,$V,$K);
					break;
				case 3:
					list($R,$G,$B) = array($M,$N,$V);
					break;
				case 4:
					list($R,$G,$B) = array($K,$M,$V);
					break;
				case 5:
				case 6: //for when $H=1 is given
					list($R,$G,$B) = array($V,$M,$N);
					break;
			}
			$R		= dechex (255 * $R);
			if(strlen($R)==1)	$R = "0".$R;
			$G		= dechex (255 * $G);
			if(strlen($G)==1)	$G = "0".$G;
			$B		= dechex (255 * $B);
			if(strlen($B)==1)	$B = "0".$B;
			
			return $return_str ? $R.$G.$B: array($R, $G, $B);
		}
		static function get_register_form()
		{
			return '
			<div  id="register-form" style="display:none"> <!-- Registration -->
				<div>
				<form method=post enctype=multipart/form-data>
					
							<!--div class="title">
							<span>'. 
								//get_bloginfo("name").
							'</span>
							</div-->
						<div style="width:120px; display:inline-block; position:relative; margin:10px;">'.
							apply_filters("smc_get_user_avatar", "").
						'</div>
						<div style="display:inline-block; position:relative; width:260px; margin:10px;">
							<input type="text" name="smp-username-reg" 	placeholder="'.__("Username").'" id="rusername" 	class="input_field"/>
							<input type="password" name="smp-password-reg" placeholder="'.__("Password").'"	id="rpassword" class="input_field" />
							<input type="text" name="smp-email-reg" placeholder="'.__('e-mail','smc').'"id="remail" class="input_field" />
							<div id="smp_register"  class="button smc_padding">'.__("Register", "smc").'</div>
						</div>
				</form>
				</div>
			<hr />							
			</div><!-- /Registration -->';
		}
		
		static function get_postId_by_slug($slug, $post_type)
		{
			global $wpdb;
			$query	= "SELECT ID AS id FROM ".$wpdb->prefix."posts WHERE post_type='$post_type' AND post_name='$slug' LIMIT 1";
			return $wpdb->get_var($query);
		}
		static function get_termId_by_slug($slug, $tax)
		{
			$term	= get_term_by("slug", $slug, $tax);
			return $term->term_id;
		}
		
		function admin_menu_separator( $parent_file )
		{
			// Handle main menu separators
			$menu = &$GLOBALS['menu'];
			foreach( $menu as $key => $item )
			{
				if (
					in_array( 'wp-menu-separator', $item )
					AND 5 < count( $item )
					)
				{
					$menu[ $key ][2] = 'separator';
					$menu[ $key ][4] = 'wp-menu-separator';
					unset(
						 $menu[ $key ][5]
						,$menu[ $key ][6]
					);
				}
			}
			return $parent_file;
		}
		function admin_submenu_separator( $parent_file )
		{
			// Handle submenu separators
			$submenu = &$GLOBALS['submenu'];
			foreach( $submenu as $key => $item )
			{
				foreach ( $item as $index => $data )
				{
					// Check if we got the identifier
					if ( in_array( 'wp-menu-separator', $data, true ) )
					{
						// Set the MarkUp, so it gets used instead of the menu title
						$data[0] = '<div class="separator1"></div>';
						// Grab our index and temporarily save it, so we can safely override it
						$new_index = $data[2];
						// Set the parent file as new index, so core attaches the "current" class
						$parent_file === $key AND $data[2] = $GLOBALS['parent_file'];
						// Reattach to the global with the new index
						$submenu[ $key ][ $new_index ] = $data;
						// Prevent duplicate
						unset( $submenu[ $key ][ $index ] );
						// Get back into the right order
						ksort( $submenu[ $key ] );
					}
				}
			}
			return $parent_file;
		}
		
		static function doubled_post($post_id)
		{
			$post		= get_post($post_id);
			if(!isset($post))
				return new WP_Error( __("It is no post object.", "smc") );
			$metas		= array('post_title'=>$post->post_title, 'post_content'  => $post->post_content);
			require_once(SMC_REAL_PATH."class/SMC_Object_type.php");
			$SMC_Object_Type	= new SMC_Object_Type();
			$object				= $SMC_Object_Type->object;
			foreach($object[$post->post_type] as $key=>$val)
			{
				if($key 		== "t") continue;
				$metas[$key]	= get_post_meta($post_id, $key, true);
			}
			$id		= wp_insert_post(
				array(
					"post_type"		=> $post->post_type,
					'post_title'    => $metas['post_title'],
					'post_content'  => $metas['post_content'],
					'post_status'   => 'publish',
				)
			);			
			foreach($metas as $meta=>$val)
			{
				//insertLog("SMC_Post.insert", array($meta, $val));
				if(	$meta	== 'title' || $meta	== 'post_title' || $meta	== 'post_content' || $meta	== 'name' || $meta == 'obj_type'  )
				{
					continue;
				}
				update_post_meta( $id, $meta, $val );
			}	
			return $post;
		}
	}

function insertLog($who, $data)
{
	global $table_prefix, $wpdb;
	$data	=  Assistants::echo_me($data);
	return $wpdb->insert(
							$table_prefix . "ermak_log",
							array("who"=>$who, "data"=>$data),
							array('%s', '%s')
						 );
}
