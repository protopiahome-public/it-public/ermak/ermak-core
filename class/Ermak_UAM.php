<?php

	class Ermak_UAM extends UserAccessManager
	{
		function __construct()
		{
			// parent::__construct();
		}
		
		/**
		 * Returns the access handler object.
		 * 
		 * @return Ermak_UAM_Handler
		 */
		public function &getAccessHandler()
		{
			if ($this->_oAccessHandler == null) 
			{
				$this->_oAccessHandler = new Ermak_UAM_Handler($this);
			}			
			return $this->_oAccessHandler;
		}
		
		
    /**
     * The function for the wp_get_nav_menu_items filter.
     * 
     * @param array $aItems The menu item.
     * 
     * @return array
     */
    public function showCustomMenu($aItems)
    {
        $aShowItems = array();
        
        foreach ($aItems as $oItem) {
            if ($oItem->object == 'post' || $oItem->object == 'page') 
			{
                $oObject = $this->getPost($oItem->object_id);
              
                if ($oObject !== null) {
                    $oPost = $this->_getPost($oObject);

                    if ($oPost !== null) {
                        if (isset($oPost->isLocked)) {
                            $oItem->title = $oPost->post_title;
                        }

                        $oItem->title .= $this->adminOutput($oItem->object, $oItem->object_id);
                        $aShowItems[] = $oItem;
                    }
                }
            } elseif ($oItem->object == 'category') {
                $oObject = $this->getCategory($oItem->object_id);
                $oCategory = $this->_getTerm('category', $oObject);

                if ($oCategory !== null && !$oCategory->isEmpty) {
                    $oItem->title .= $this->adminOutput($oItem->object, $oItem->object_id);
                    $aShowItems[] = $oItem;
                }
            }
			elseif ($oItem->object == 'location') 
			{
                $oObject = $this->getCategory($oItem->object_id);
                $oCategory = $this->_getTerm('location', $oObject);
                if ($oCategory !== null && !$oCategory->isEmpty) {
                    $oItem->title .= $this->adminOutput($oItem->object, $oItem->object_id);
                    $aShowItems[] = $oItem;
                }
            } 
			else 
			{
                $aShowItems[] = $oItem;
            }
			
        }
        
        return $aShowItems;
    }
	}
?>