<?php
	class Ermak_UAM_Handler extends UamAccessHandler
	{
		public function __construct(UserAccessManager &$Ermak_UAM)
		{
			
			parent::__construct($Ermak_UAM);
			$this->_aObjectTypes[]			= "location";
		}
		public function get_ObjectTypes()
		{
			return $this->_aObjectTypes;
		}
		
     /**
     * Returns the excluded posts.
     * 
     * @return array
     */
    public function getExcludedPosts()
    {
        /**
         * @var wpdb $wpdb
         */
        global $wpdb;
        
        if ($this->checkUserAccess('manage_user_groups')) {
            $this->_aSqlResults['excludedPosts'] = array();
        }
        
        if (isset($this->_aSqlResults['excludedPosts'])) {
            return $this->_aSqlResults['excludedPosts'];
        }
        
        if ($this->getUserAccessManager()->atAdminPanel()) {
            $sAccessType = "write";
        } else {
            $sAccessType = "read";
        }

        $aCategoriesAssignedToUser = $this->getCategoriesForUser();
            
        if ($aCategoriesAssignedToUser !== array()) {
            $sCategoriesAssignedToUser = implode(', ', $aCategoriesAssignedToUser);
        } else {
            $sCategoriesAssignedToUser = "''";
        }
        
        $aPostAssignedToUser = $this->getPostsForUser();
        
        if ($aPostAssignedToUser !== array()) {
            $sPostAssignedToUser = implode(', ', $aPostAssignedToUser);
        } else {
            $sPostAssignedToUser = "''";
        }
        
        $sPostSql = "SELECT DISTINCT p.ID
            FROM $wpdb->posts AS p
            INNER JOIN $wpdb->term_relationships AS tr
                ON p.ID = tr.object_id
            INNER JOIN $wpdb->term_taxonomy tt
                ON tr.term_taxonomy_id = tt.term_taxonomy_id
			WHERE (tt.taxonomy = 'category' OR tt.taxonomy = 'location')
            AND tt.term_id IN (
                SELECT gc.object_id
                FROM ".DB_ACCESSGROUP." iag
                INNER JOIN ".DB_ACCESSGROUP_TO_OBJECT." AS gc
                    ON iag.id = gc.group_id
                WHERE (gc.object_type = 'category' OR gc.object_type = 'location')
                AND iag.".$sAccessType."_access != 'all'
                AND gc.object_id  NOT IN (".$sCategoriesAssignedToUser.")
            ) AND p.ID NOT IN (".$sPostAssignedToUser.")
            UNION
            SELECT DISTINCT gp.object_id
            FROM ".DB_ACCESSGROUP." AS ag
            INNER JOIN ".DB_ACCESSGROUP_TO_OBJECT." AS gp
                ON ag.id = gp.group_id
            INNER JOIN $wpdb->term_relationships AS tr
                ON gp.object_id  = tr.object_id
            INNER JOIN $wpdb->term_taxonomy tt
                ON tr.term_taxonomy_id = tt.term_taxonomy_id
            WHERE gp.object_type = 'post'
            AND ag.".$sAccessType."_access != 'all'
            AND gp.object_id  NOT IN (".$sPostAssignedToUser.")
            AND tt.term_id NOT IN (".$sCategoriesAssignedToUser.")";
        
        $this->_aSqlResults['excludedPosts'] = $wpdb->get_col($sPostSql);
        return $this->_aSqlResults['excludedPosts'];
    }
    
    
	}
?>