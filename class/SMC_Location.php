<?php
class SMC_Location
{	
	public $options;
	static $location_meta;
	public $location_type;
	static $instanses;
	static function init()
	{
		//$this->options										= get_option(SMC_ID);
		add_action('init', 										array( __CLASS__, 'add_location_handler' ) , 10);		
		add_action("delete_".SMC_LOCATION_NAME,					array( __CLASS__, 'delete_location_handler'));
		add_action( 'category_add_form_fields', 				array( __CLASS__, 'pippin_taxonomy_add_new_meta_field'), 10, 2 );
		add_action( 'category_edit_form_fields', 				array( __CLASS__, 'pippin_taxonomy_edit_meta_field'), 10, 2 );
		//
		add_action( SMC_LOCATION_NAME.'_add_form_fields', 		array( __CLASS__, 'add_location_type_to_location'), 10, 2 );
		add_action( SMC_LOCATION_NAME.'_edit_form_fields', 		array( __CLASS__, 'edit_location_type_to_location'), 2, 2 );
		add_action( 'edit_'.SMC_LOCATION_NAME, 					array( __CLASS__, 'save_taxonomy_custom_meta'), 10);  
		add_action( 'create_'.SMC_LOCATION_NAME, 				array( __CLASS__, 'save_taxonomy_custom_meta'), 10);
		add_action( SMC_LOCATION_NAME.'_edit_form',  			array( __CLASS__, 'location_edit_form_fun'));
		
		//manage column for Location screen
		add_filter("manage_edit-".SMC_LOCATION_NAME."_columns", array( __CLASS__,'location_columns')); 
		add_filter("manage_".SMC_LOCATION_NAME."_custom_column", array( __CLASS__,'manage_location_columns'), 11.234, 3);
		add_filter( 'manage_edit-'.SMC_LOCATION_NAME.'_sortable_columns', array( __CLASS__,'sort_locations'), 20 );
		add_filter( 'request', 									array( __CLASS__,'column_orderby') );
		add_filter("after-".SMC_LOCATION_NAME."-table", 		array( __CLASS__,'after_location_table'), 10, 3);
		add_action( 'parent_file', 								array( __CLASS__, 'recipe_tax_menu_correction'),1); //http://wordpress.org/support/topic/moving-taxonomy-ui-to-another-main-menu
		add_filter("location_display_tbl0", 					array( __CLASS__, "location_display_tbl0"), 11, 4);
				
	}
	static function after_location_table($taxonomy_name)
	{
		echo "
		<div  style='display:none;' id='drop_users'>".
			wp_dropdown_users(
								array(
										'show_option_none'  => "---",
										'echo'				=> false,
										'show'				=> 'display_name',
										'name'				=> 'term_meta[creator]',
										'class'				=> 'chosen-select',
										'multi'             => true,
										'selected'			=> $term_meta ['creator']
									  )
							  ).
		"</div>
		";
		
		return;
		/*
		ob_start();
		var_dump(get_option( 'taxonomy_image_plugin_settings' ));
		$var	= ob_get_contents();
		ob_end_clean();
		echo "<div>". $var ."</div>";
		*/
	}
	static function install()
	{
		global $wpdb;
		$wpdb->query(
				"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "location_meta` (
				  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				  `location_type` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `hiding_type` int(8) unsigned NOT NULL DEFAULT '0',
				  `map_type` int(8) unsigned NOT NULL DEFAULT '0',
				  `owner1` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `owner2` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `owner3` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `creator` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `members` varchar(125),
				  `x_pos` INT(10) unsigned NOT NULL DEFAULT '0',
				  `y_pos` INT(10) unsigned NOT NULL DEFAULT '0',
				  PRIMARY KEY (`ID`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
			);
		$wpdb->query(
				"CREATE TABLE IF NOT EXISTS  `" . $wpdb->prefix . "location_object_relationships` (
				 `ID` BIGINT( 20 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
				 `location_id` BIGINT( 20 ) UNSIGNED NOT NULL DEFAULT  '0',
				 `object_id` BIGINT( 20 ) UNSIGNED NOT NULL DEFAULT  '0',
				 `object_type` VARCHAR( 20 ) NOT NULL DEFAULT  '',
				 `relationships_type` INT( 10 ) UNSIGNED NOT NULL DEFAULT  '0',
				PRIMARY KEY (  `ID` ) ,
				CONSTRAINT location_industry_circle UNIQUE (
					`location_id` ,
					`object_id` ,
					`object_type`
					)
				) ENGINE = MYISAM DEFAULT CHARSET = utf8 AUTO_INCREMENT =1"
			);
		$wpdb->query(
				"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "location_member` (
				  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				  `location_id` int(10) unsigned NOT NULL,
				  `user_id` int(10) unsigned NOT NULL,
				  PRIMARY KEY (`ID`),
				  UNIQUE KEY `location_user` (`location_id`,`user_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;"
			);
		$wpdb->query(
				"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "location_log` (
				  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				  `location_id` int(10) unsigned NOT NULL,
				  `type` int(10) unsigned NOT NULL,
				  `content` longtext,
				  `time` int(10) unsigned NOT NULL,
				  `user_id` int(10) unsigned NOT NULL,
				  PRIMARY KEY (`ID`)
				) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;"
			);
		
	}	
	
	/*
		types:
		0	- CHANGE_LOCATION_MEMBERS 
		1	- CHANGE_BATCH_DISTINATION
		2	- FINISH_PRODUCTION_CYRCLE
		3	- FINISH_CONSUME_CYRCLE
		4	- CREATE_LOCATION
	*/
	
	static function add_log($id, $content, $type, $user_id)
	{
		global $wpdb;
		$add_query						= "REPLACE INTO " . $wpdb->prefix . "location_log (location_id,content,type,time, user_id) VALUES ($id, '$content', $type, ". time() .", $user_id)";
		$wpdb->query( $add_query );
	}
	static function read_log($ids, $count='10', $start='0')
	{
		global $wpdb;
		$ides		= is_array($ids) ? implode(",", $ids) : $ids;
		$query		= "SELECT * FROM ".$wpdb->prefix . "location_log WHERE location_id IN ($ides) ORDER BY ID DESC LIMIT ".(string)$start." ,$count";
		//insertLog("read_log", $query);
		return $wpdb->get_results($query, OBJECT );
	}
	
	static function add_properties($names, $types, $defaults)
	{	
		global $wpdb;
		if(!is_array($names))			$names 		= array($names);
		if(!is_array($types))			$types 		= array($types);			
		if(!is_array($defaults))		$defaults 	= array($defaults);			
		$ct								= count($types);
		$cn								= count($names);
		$cd								= count($defaults);
		if($cn > $ct) 					$types		= array_merge( $types,		array_fill(0, $cn-$ct,  $types[0]));
		if($cn > $cd) 					$defaults	= array_merge( $defaults,	array_fill(0, $cn-$cd,  $defaults[0]));
		$query							= "ALTER IGNORE TABLE `" . $wpdb->prefix . "location_meta` ADD COLUMN (";
		$values							= array();
		$i=0;
		foreach($names as $nm)
		{
			$values[]					= $nm . " " . $types[$i] . " NOT NULL DEFAULT " . $defaults[$i];
			$i++;
		}
		$query							.= implode(", ", $values). ");";
		//insertLog( "SMC_Location::add_properties", $query );
		return $wpdb->query($query);
	}
	
	static function get_instance($id)
	{	
		global	$instances;
		if(!isset($id) || !$id || !is_numeric($id))			return;
		if(!$instances)					$instances = array();
		if(!$instances[$id])
			$instances[$id]				= get_term_by("id", $id, SMC_LOCATION_NAME);
		return $instances[$id];
	}
	static function get_term_meta($id)
	{
		global	$location_meta;
		if(!$location_meta)			$location_meta = array();
		//if(!$location_meta[$id])
			$location_meta[$id] 	= self::get_taxonomy_custom_meta($id);//get_option("taxonomy_$id");
		return $location_meta[$id];
	}
	static function update_term_meta($id, $obj, $is_insert, $is_log)
	{
		//update_option("taxonomy_$id", $obj);
		insertLog("update_term_meta", 'SMC_Location::update_taxonomy_custom_meta');
		static::update_taxonomy_custom_meta($id, $obj, $is_insert, $is_log);
		global	$location_meta;
		if(!$location_meta)			$location_meta = array();
		$location_meta[$id] 		= self::get_taxonomy_custom_meta($id);
	}
	
	/*
		$metafields - named array as \ array("key"=>"val", "key2"=>"val2") \ 
	*/
	static function get_filtered($metafields, $sign="=")
	{
		global $wpdb;
		$where			= array();
		foreach($metafields as $mf=>$val)
		{
			$where[]	= $mf . $sign . $val;
		}
		$w				= implode(" AND ", $where);
		$sel			= "SELECT * FROM " . $wpdb->prefix . "location_meta WHERE ". $w;
		return $wpdb->get_results( $sel );
		
	}
	static function get_term_link($id)
	{
		//return is_numeric((int)$id);
		//return get_term_link((int)$id, 'location');
		global $location_links;
		if(!$location_links)		$location_links = array();
		if(!$location_links[$id])
			$location_links[$id]	= get_term_link( (int)$id, SMC_LOCATION_NAME );
		return $location_links[$id];
	}
	static function add_new_location(
										$parent_location_id,
										$name,
										$slug,
										$location_type,
										$hiding_type = 0,
										$params = -1
									)
	{
		global $UAM;
		if($UAM->is_current_user_can_add_location($parent_location_id))
		{
			$args = array(
							'parent'=> $parent_location_id,
							'slug'	=> $slug
						  );
			$new_location_data	= wp_insert_term( $name, SMC_LOCATION_NAME, $args );
			if(is_wp_error($new_location_data))
				return false;
			$nl_id			 	= $new_location_data['term_id'];
			$term_meta			= array(
											"location_type"	=> $location_type, 
											"owner1"		=> get_current_user_id(), 
											"creator"		=> get_current_user_id(),
											"hiding_type"	=> $hiding_type
										);
			if($params != -1)
			{
				foreach($params as $param => $val)
					$term_meta[$param]	= $val;
			}
			self::update_taxonomy_custom_meta( $nl_id, $term_meta, true );
			//update_option( "taxonomy_$nl_id", $term_meta );
			$UAM->update_uam_group($nl_id);			
			
			$content		=  sprintf(
				__("Create new Location %s.", "smc"), 
				"<b>".$new_location_data->name."</b> "
			);
			static::add_log((int)$nl_id, $content, CREATE_LOCATION, get_current_user_id());
		
			return $nl_id;
		}
		return false;
	}	
	// register new taxonomy which applies to attachments
	static function add_location_handler() 
	{
		$labels = array(
			'name'              => __('Location', "smc"),
			'singular_name'     => __('Location', "smc"),
			'search_items'      => __('Search Locations', "smc"),
			'all_items'         => __('All Locations', "smc"),
			'parent_item'       => __('Parent Location', "smc"),
			'parent_item_colon' => __('Parent Location:', "smc"),
			'edit_item'         => __('Edit Location', "smc"),
			'update_item'       => __('Update Location', "smc"),
			'add_new_item'      => __('Add New Location', "smc"),
			'new_item_name'     => __('New Location Name', "smc"),
			'menu_name'         => __('Locations', "smc"),
		);

		$args = array(
			'labels' => $labels,
			'show_ui '=>false,
			'hierarchical' => true,
			'query_var' => 'true',
			'rewrite' => 'true',
			'show_admin_column' => true,
		);
		register_taxonomy( SMC_LOCATION_NAME, array('post'), $args );	
		
		
	}
	
	static function delete_location_handler($iLocationId)
	{
		delete_option("taxonomy_$iLocationId");
		delete_option("taxonomy_map_$iLocationId");
	} 
	
	
	//=============================================
	//
	// Save extra taxonomy fields callback function.
	//
	//=============================================
	
	static function save_taxonomy_custom_meta( $term_id ) 
	{
		if ( isset( $_POST['term_meta'] ) ) 
		{	
			self::create_taxonomy_custom_meta( $term_id, $_POST['term_meta'] );
			$loc			= static::get_instance($term_id);
			$content		= sprintf(
				__("Create new Location %s.", "smc"), 
				"<b>".$loc->name."</b> "
			);
			static::add_log((int)$term_id, $content, CREATE_LOCATION, get_current_user_id());
		}
	}
	static function create_taxonomy_custom_meta($term_id, $meta_data)
	{
		if(!is_array($meta_data))		return;
		global $wpdb;
		global $Soling_Metagame_Constructor;
		$old_meta						= self::get_term_meta($term_id);
		
		//update users
		$old_users						= array($old_meta['owner1'],$old_meta['owner2'],$old_meta['owner3'],$old_meta['creator']);
		$new_users						= array($meta_data['owner1'],$meta_data['owner2'],$meta_data['owner3'],$meta_data['creator']);
		$add_users						= array_unique( array_diff( $new_users, $old_users, array( "", null, 0 ) ) );
		$rem_users						= array_unique( array_diff( $old_users, $new_users ) );
			
		//remove
		$rem_query						= "DELETE FROM " . $wpdb->prefix . "location_object_relationships WHERE (";
		$rems							= array();
		foreach($rem_users as $user)
		{
			$rems[]						= "object_id=%s";
		}
		$rem_query						.= implode($rems, " OR ") . ') AND location_id='.$term_id.' AND object_type="user"';
		$wpdb->query( $wpdb->prepare( $rem_query, $rem_users ) );
		/**/
		//add
		$add_query						= "REPLACE INTO " . $wpdb->prefix . "location_object_relationships (location_id,object_id,object_type,relationships_type) VALUES ";
		$adds							= array();
		foreach($add_users as $user)
		{
			$adds[]						= '('.$term_id.','.$user.',"user",1)';
		}
		$add_query						.= implode($adds, ",");
		$wpdb->query( $wpdb->prepare( $add_query, $add_users ) );
		
		//
		$query							= "REPLACE INTO `" . $wpdb->prefix . "location_meta` SET ID=$term_id";
		
		$vals						= array();
		$values						= array();
		$nex_meta					= array_diff( array_keys($meta_data), $Soling_Metagame_Constructor->options['location_meta'] );
		if(count($nex_meta)>0)
		{
			$nqs					= array();
			foreach($nex_meta as $meta)
			{
				$nqs[]				= " ADD COLUMN `" . $meta . "` varchar(20) NOT NULL default '' ";
				$Soling_Metagame_Constructor->options['location_meta'][] = $meta;
			}
			update_option( SMC_ID, 	$Soling_Metagame_Constructor->options );
			$nq						= "ALTER IGNORE TABLE `" . $wpdb->prefix . "location_meta` " . implode($nqs, ",");
			$wpdb->query($nq);
		}			
		foreach($meta_data as $meta	=> $val)
		{
			$values[]				=  $val;
			$vals[]					= $meta . "=%s";
		}
		$query						.= "," . implode($vals, ",");	
		$wpdb->query( $wpdb->prepare( $query, $values ) );		
		return $query;
	}
	static function get_taxonomy_custom_meta($id)
	{
		global $wpdb;
		if(!isset($id) || $id == "")	return array();
		$query			= "SELECT * FROM ".$wpdb->prefix . "location_meta WHERE ID = $id";
		return $wpdb->get_row($query, ARRAY_A );
	}
	static function update_taxonomy_custom_meta($id, $update, $is_insert=false,  $is_log=false)
	{
		global $wpdb, $location_meta;
		if( $is_log)
			insertLog("SMC_Location::update_taxonomy_custom_meta", $update);
		if( $is_insert)
		{
			$update['ID']		= $id;
			return $wpdb->insert	(
										$wpdb->prefix . "location_meta",
										$update,
										array( '%s')
									);
		}
		else
		{
			return $wpdb->update( 	
									$wpdb->prefix . "location_meta",
									$update, //$data,
									array( 'ID' => $id ),
									array( '%s', '%d' ),
									array( '%d' )
								);
			foreach($update as $key => $val)
			{
				InsertLog("SMC_Location::update_taxonomy_custom_meta", 4);
				$location_meta[$id][$key]	= $val;
			}
		}
	}
	//==============================================================================================
	//
	//	Дополнительные поля в Location taxonomy
	//
	//===============================================================================================
		
		
	static function pippin_taxonomy_add_new_meta_field($tax_name, $name_field = array('military', "ID союза"))
	{
		// this will add the custom meta field to the add new term page
		?>
		<div class="form-field">
			<label for="term_meta[extrude_courusel]"><?php _e("exclude from carousel", "smc"); ?></label>
			<select  name="term_meta[extrude_courusel]" id="term_meta[extrude_courusel]" class='chosen-select'>
				<option> <?php _e("include", "smc") ?></option>
				<option selected><?php _e("exclude", "smc") ?></option>
			</select>			
			<p class="description"><?php _e("","smc"); ?></p>
		</div>
		<script>
			set_chosen(".chosen-select", {max_selected_options: 5});
		</script>
	<?php
	
	}
	
	
	// Edit term field

	static function pippin_taxonomy_edit_meta_field($term, $tax_name, $name_field=array('military', "ID союза")) 
	{
	 
		// put the term ID into a variable
		$t_id = $term->term_id;
	 
		// retrieve the existing value(s) for this meta field. This returns an array
		$term_meta = SMC_Location::get_term_meta($t_id); //get_option( "taxonomy_$t_id" ); 
		//print_r ($term_meta);
		?>
		<tr class="form-field">
		<th scope="row" valign="top"><label for="term_meta[extrude_courusel]"><?php _e("exclude from carousel", "smc"); ?></label></th>
			<td>
				<select  name="term_meta[extrude_courusel]" id="term_meta[extrude_courusel]" class='chosen-select'>
				<option <?php selected($term_meta ['extrude_courusel'],0); ?> value='0'><?php _e("include", "smc") ?></option>
				<option <?php selected($term_meta ['extrude_courusel'],1); ?> value='1'><?php _e("exclude", "smc") ?></option>
			</select>	
			<p class="description"><?php _e("","smc"); ?></p>
			</td>
		</tr>
		<script>
			set_chosen(".chosen-select", {max_selected_options: 5});
		</script>
	<?php
	}
	
	
	static function add_location_type_to_location($tax_name)
	{
		require_once(SMC_REAL_PATH.'tpl/new_location_form.php');
	}
	static function edit_location_type_to_location($term, $tax_name)
	{
		require_once(SMC_REAL_PATH.'tpl/edit_locatin_form.php');
	}

	static function location_edit_form_fun($term)
	{
		/*
		global $Ermak_UAM;
		//echo Assistants::echo_me($Ermak_UAM->getAccessHandler()->get_ObjectTypes(), true);
		$sls			= $Ermak_UAM->getAccessHandler()->getUserGroups();
		$sls1			= array();
		foreach($sls as $sl)
			$sls1[]		= array(
									'name'				=> $sl->getGroupName(), 
									'id'				=> $sl->getId(), 
									//'ObjectTypes'		=> $sl->getAllObjectTypes(),
									'ReadAccess'		=> $sl->getReadAccess(),
									'WriteAccess'		=> $sl->getWriteAccess(),
									'objectIsMember'	=> $sl->objectIsMember('category', $term->term_id),
									'term_id'			=> $term->term_id,
								);		
		echo Assistants::echo_me($sls1, true);
		echo Assistants::echo_me($Ermak_UAM->getAccessHandler()->getUserGroupsForObject('category', $term->term_id, false), true);
		*/
		?>
			<div id="modal-fon" style="position:absolute; top:0; left:-80px; width:100%; height:100%; background:rgba(0,0,0,0.75); display:none;">
			</div>
			<div id="modal-window" style="position:absolute; 
						top:40px; 
						left:170px; 
						width:1100px; 
						height:600px; 
						background:#EEE; 
						border:1px solid #000;
						-webkit-box-shadow: 8px 8px 4px 4px rgba(0,0,0,0.5);
						box-shadow: 8px 8px 4px 4px rgba(0,0,0,0.5);
						display:none;">
				<div style="padding:10px;">
					<?php include SMC_REAL_PATH.'tpl/map-edit.php' ; ?>
				</div>
			</div>
			<div id="waiting-fon" style="position:absolute;top:0; left:0; background:rgba(0,0,0,0.25); width:200%; height:100%;display:none;">
				<div style="position:absolute; top:45%; left:45%; text-align:center; ">
					<i class="fa fa-cog fa-spin fa-5x"></i><br>
					<?php _e("Please wait"); ?>
				</div>
			</div>
			<?php
				$dd				= '<?xml version="1.0" encoding="UTF-8"?><svg width="1000" height="1000" xmlns="http://www.w3.org/2000/svg"><!-- Created with SVG-edit - http://svg-edit.googlecode.com/ --><g><title>Layer 1</title></g></svg>';
				$args				= array(
									'number' 		=> 120
									,'offset' 		=> 0
									,'orderby' 		=> 'id'
									,'order' 		=> 'ASC'
									,'hide_empty' 	=> false
									,'fields' 		=> 'all'
									,'slug' 		=> ''
									,'hierarchical' => true
									,'name__like' 	=> ''
									,'pad_counts' 	=> false
									,'get' 			=> ''
									,'child_of' 	=> 0
									,'parent' 		=> $term->term_id
								);
								
				$childLocs			= get_terms(SMC_LOCATION_NAME, $args);
				$chldren_id			= array();
				$chldren_name		= array();
				foreach($childLocs	as $ch)
				{
					$chldren_id[]	= "L".$ch->term_id;			
					$chldren_name[]	= $ch->name;			
				}
				$ids				= json_encode($chldren_id);	
				$names				= json_encode($chldren_name);	
				//Assistants::log($dd);
			?>
			<script>
				
				function StringtoXML(text){
					if (window.ActiveXObject){
					  var doc=new ActiveXObject('Microsoft.XMLDOM');
					  doc.async			= 'false';
					  doc.loadXML(text);
					} else {
					  var parser		= new DOMParser();
					  var doc			= parser.parseFromString(text,'text/xml');
					}
					return doc;
				}
			
				window.saveSvg  = function (datas)
				{
					//alert("saveSvg.datas=" + datas);
					send(["save_map_data", datas, <?php print($term->term_id);?>]);
				}
				
				jQuery(function($)
				{
					
					window.map_data 		= "map_data";
					var xml 				= "<svg>" + $("#edit_map svg").html() + "</svg>";//'<?php echo $dd ?>';
					//var $map_data = StringtoXML(xml);
					//alert("xml = " + xml);
					window.map_data 		= xml;
					window.list_ids			= JSON.parse('<?php echo $ids?>');
					window.list_names		= JSON.parse('<?php echo $names?>');
				
					$("#modal-fon").appendTo(document.body);
					
					//$("modal-fon").changeZIndex('auto');
					$("#modal-window").appendTo(document.body);
					//$("modal-window").changeZIndex('auto');
					$("#waiting-fon").appendTo(document.body);
					//$("waiting-fon").css('z-index', 'auto');
				});
			</script>
		<?php
	}
	
	static function location_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 			=> ' ',
			'id' 			=> 'id',
			'name' 			=> __('Name'),
			//'map_type' 	=> __('map type', 'smc'),
			//'hiding_type'	=>__("Hiding Type", 'smc'),
			//'creator'		=>__("Location creator", 'smc'),
			'owner'			=>__("Location owners", 'smc'),
			//'description' => __('Description'),
			'slug' 			=> __('Slug'),
			//'posts' 		=> __('Posts'),
			'events'		=> __("Over params", "smc")
		);
		return $new_columns;
	}
	static function sort_locations($columns)
	{
		$columns['id']					= 'id';
		//$columns['map_type']			= 'map_type';
		//$columns['hiding_type']			= 'hiding_type';
		$columns['events']				= 'events';
		return $columns;
	}
	static function column_orderby($vars)
	{
		//if ( !is_admin() )				 return $vars;
		if ( isset( $vars['orderby'] ) && 'y_pos' == $vars['orderby'] ) {
			$vars = array_merge( $vars, array( 'meta_key' => 'y_pos', 'orderby' => 'meta_value_num' ) );
		}
		if ( isset( $vars['orderby'] ) && 'map_type' == $vars['orderby'] ) {
			$vars = array_merge( $vars, array( 'meta_key' => 'map_type', 'orderby' => 'meta_value_num' ) );
		}
		elseif ( isset( $vars['orderby'] ) && 'hiding_type' == $vars['orderby'] ) {
			$vars = array_merge( $vars, array( 'meta_key' => 'hiding_type', 'orderby' => 'meta_value' ) );
		}
		return $vars;
	}
	static function manage_location_columns($out, $column_name, $theme_id) 
	{
		global $location_hide_types;
		global $location_map_types;
		global $Soling_Metagame_Constructor;
		$theme 	= self::get_instance($theme_id);//get_term($theme_id, 'location');
		$tax	= self::get_term_meta($theme_id);//get_option("taxonomy_$theme_id");
		switch ($column_name) {
			case 'id':
				$out 		.= $theme_id;
				break;
			case 'map_type': 
				$mt			= (int)$tax["map_type"];
				$lt			= $tax['location_type'];
				$out 		.= 
								(get_post_meta($lt, "map_behavior", true) == DOT_MAP_BEHAVIOR) ?  
									"<i>" . __($location_map_types[$mt], "smc") . "</i><BR>positions:<BR>" . $tax['x_pos'] . "<BR>" . $tax['y_pos'] :  
									"<i>" . __($location_map_types[$mt], "smc"). "</i>"; 
				break;	 
			case 'hiding_type': 
				$out 		.= __($location_hide_types[$tax["hiding_type"]], "smc"); 
				break;	  
			case 'creator': 
				$out 		.= get_userdata($tax["creator"])->display_name; 
				break;  
			case 'owner': 
				$out 		.= "<div class='abzaz'>"; 
				$out 		.= get_userdata($tax["owner1"])->display_name."<BR>"; 
				$out 		.= get_userdata($tax["owner2"])->display_name."<BR>"; 
				$out 		.= get_userdata($tax["owner3"])->display_name; 
				$out		.= "
				</div>
				<div class=smc_switcher_container>
					<div class='button change_owner' loc_id='$theme_id' owner1='".$tax["owner1"]."' owner2='".$tax["owner2"]."' owner3='".$tax["owner3"]."'>".
						__("Change", "smc").
					"</div>
				</div>
				<a href='" .get_post_permalink( $Soling_Metagame_Constructor->options['location_display'] ) ."&lid=$theme_id'>".__("go to Location display", "smc")."</a>";
				break;	 
			case 'events': 
				$lt			= $tax['location_type'];
				$mt			= (int)$tax["map_type"];
				$lt			= $tax['location_type'];
				$loc_type	.= (get_post_meta($lt, "map_behavior", true) == DOT_MAP_BEHAVIOR) ?  
									"<i>" . __($location_map_types[$mt], "smc") . "</i><BR>positions: x=" . (int)$tax['x_pos'] . ", y=" . (int)$tax['y_pos'] :  
									"<i>" . __($location_map_types[$mt], "smc") . "</i>"; 
				$out 		.= apply_filters(	
												"smc_location_tab_over_params",
												__("Type") .  ": <B>" . get_post_meta($lt, "slug", true)."</B><BR>" . $loc_type . "<BR><b>" .__($location_hide_types[$tax["hiding_type"]], "smc")."</b><BR>" , 
												$theme_id, 
												$tax
											) ;
				break;	
			default:
				break;
		}
		return $out;    
	}
	
	// highlight the proper top level menu
	static function recipe_tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ($taxonomy == SMC_LOCATION_NAME)
			$parent_file = 'metagame';
		return $parent_file;
	}
	
	static function get_all_locations()
	{
		$locs		= get_terms(SMC_LOCATION_NAME, array("number"=>120, 'orderby'=>'name', "hide_empty"=>false, "fields" => "ids"));
		return $locs;
	}
	static function get_all_location_ids()
	{	$args			= array(
									 'number' 		=> 100
									,'offset' 		=> 0
									,'orderby' 		=> 'name'
									, 'fields'     	=> 'ids'
									,"hide_empty"	=> false,
								);
		$locations		= get_terms(SMC_LOCATION_NAME, $args);
		return $locations;
	}
	/* TODO : argument $user_id is not exists */
	static function get_all_user_visible_location_ids($user_id=-1)
	{
		global $UAM;
		$locs			= self::get_all_location_ids();
		if(!$UAM->is_install() )
			return $locs;
		if($user_id == -1)
			return $locs;
		$ids			= array();
		foreach($locs as $location)
		{
			if($UAM->isAccessLocation($location))
				$ids[] = $location;
		}
		
		return $ids;
	}
	/* TODO : argument $user_id is not exists */
	static function get_all_user_visible_locations($args, $user_id = -1)
	{
		global $UAM;
		$all_locations		= get_terms(SMC_LOCATION_NAME, $args);
		if(!$UAM->is_install() )
			return $all_locations;
		if($user_id == -1)
			return $all_locations;
		$locations			= array();
		foreach($all_locations as $location)
		{
			if($UAM->isAccessLocation($location->term_id))
				$locations[] = $location;
		}
		return $locations;
	}
	static function get_all_usage_location_ids()
	{
		global $Soling_Metagame_Constructor;
		$args			= array(
									 'number' 		=> 100
									,'offset' 		=> 0
									,'orderby' 		=> 'name'
									, 'fields'     	=> 'ids'
									,"hide_empty"	=> false,
								);
		$locations		= get_terms(SMC_LOCATION_NAME, $args);
		$locs			= array();
		foreach($locations as $loc)
		{
			if($loc_us	= $Soling_Metagame_Constructor->location_has_owners($loc->term_id))
			{				
				$locs[]	= $loc;
			}
			else
			{
				continue;
			}				
		}
		return $locs;
	}
	static function get_by_types($array_types, $only_usage_locations = false)
	{
		global $Soling_Metagame_Constructor;
		$args			= array(
									 'number' 		=> 100
									,'offset' 		=> 0
									,'orderby' 		=> 'name'
									,"hide_empty"	=> false,
								);
		$locations		= get_terms(SMC_LOCATION_NAME, $args);
		$locs			= array();
		foreach($locations as $loc)
		{
			$option		= SMC_Location::get_term_meta( $loc->term_id ); //get_option( "taxonomy_" . $loc->term_id );
			//$usage		= $only_usage_locations ?	$Soling_Metagame_Constructor->location_has_owners( $loc->term_id ) :	true;
			if( array_intersect( array( $option['location_type'] ), $array_types ) )
			{				
				$locs[]	= $loc;
			}
			else
			{
				continue;
			}				
		}
		return $locs;
	}
	static function get_child_location_ids($parent_id)
	{
		global $UAM;
		global $Soling_Metagame_Constructor;
		$locations		= $parent_id==0 ?  $Soling_Metagame_Constructor->get_locations_id() : get_term_children($parent_id, SMC_LOCATION_NAME);
		$locations[]	= strval($parent_id);
		$locs			= array();
		foreach($locations as $loc)
		{
			if($UAM->isAccessLocation($loc))
				$locs[] =  strval($loc);
		}
		//insertLog("SMC_L::get_child_location_ids", $locs);
		return $locs;
	}
	static function get_infrastructure_widget($location_id)
	{
		global $Soling_Metagame_Constructor;
		$html11	.= "
			<div class='klapan3_subtitle lp_clapan_raze'>".
				//__("all Infrastructures", "smc").
			'<div class=lp_clapan_raze1></div>
		</div>';
		$html	.= apply_filters("smc_widget_infrastructure", $html11, $location_id);
		return $html;
	}
	static function get_main_widget($location_id)
	{
		global $Soling_Metagame_Constructor;
		$html	.= "		
			<div class='klapan3_subtitle lp_clapan_raze'>".
				apply_filters("smc_before_main_location_widget", "", $location_id).
				$Soling_Metagame_Constructor->get_location_hiding_name($location_id, '', false).
			'	<div class=lp_clapan_raze1></div>'.
				'<div class="smp_bevel_form simply_text fac_block">'.		
					$Soling_Metagame_Constructor->get_location_hiding_discr($location_id).
				'</div>'.
				apply_filters("smc_main_location_widget", "", $location_id).			'	
			</div>';
		return $html;
	}
	static function get_location_widget($location_id)
	{
				
		$html			= array(
									array("title" => "<img src=".SMC_URLPATH."img/goods.png>", 			"slide" => self::get_main_widget($location_id), 			'hint' => __("Location", "smc")),														
									array("title" => "<img src=".SMC_URLPATH."img/factory.png>", 		"slide" => self::get_infrastructure_widget($location_id), 	'hint' => __("Location", "smc")),														
									array("title" => "<img src=".SMC_URLPATH."img/user.png>", 			"slide" => self::owners_form($location_id), 				'hint' => __("Owners", "smc"))														
								);
		return $html;
	}
	static function owners_form($location_id, $is_owner="-1", $location_name=false)
	{
		global $Soling_Metagame_Constructor;
		$lt 				= $Soling_Metagame_Constructor->get_location_type($location_id);
		$html				.= "
			
				<div class='klapan3_subtitle lp_clapan_raze'>".
					__("Owners", "smc").
				"	<div class=lp_clapan_raze1></div>	
				</div>";
		if($location_name)
		{
			$html .=
				'
			<div class="smp_bevel_form">
				<div class="lp-location-owners">'.	
						$location_name .
				'</div>
			</div>';
			//$html 				.= "<div class='smp_bevel_form'>";
		}
		if($is_owner == "-1")
		{
			$is_owner			= $Soling_Metagame_Constructor->cur_user_is_owner($location_id);
		}
		if($is_owner)
		{
			$html .=
				'<div class="lp-location-owners">'.	
						__("You are owner of this Location.", "smc").
				'</div>';
		}
		//else
		{
			$html 			.= "<div class='smp_bevel_form'>";
			$html			.= static::my_owner_form($location_id);
		}
		$html				.= ($lt->may_have_children && is_user_logged_in()) || current_user_can("manage_options") ? "
		<div style='margin:10px;'>
			<div class='smc-alert button' target_name='new_location_win' title='".__("Add New Location","smc")."' style='padding:10px;'>". __("Create location", "smc") ."</div>
			<div id='new_location_win' class='lp-hide'>
				<div style='position:relative; display:inline-block; padding:20px;'>". 
					'
					<div>
						<div style="position:relative; display:inline-block; width:100%;">
							<div>
								<board_title>'. __("Set a type of Location", "smc").'</board_title>
								<label class="smc_input">'.
									$Soling_Metagame_Constructor->wp_dropdown_location_type(array("name"=>"nl_lt", "style"=>"width:350px!important;")).
							'	</label>
							</div>
							<div>
								<div style=" margin:0 0 25px 0;">
									<board_title>'. __("Set a Title", "smc").'</board_title>
									<input type="text" placeholder:"'.__("Title").'" name="new_location_title" class="smc_decor" style="width:350px!important;"/>
								</div>
							</div>
							<div>
								<p>
									<input type="radio" name="nl_ht" id="islocked1" value="0" class=""/>
									<label for="islocked1" >'.__('Public Location', 'smc').'</label>
								<br>
									<input type="radio" name="nl_ht" id="is_locked2" value="1" class="" />
									<label for="islocked2">'.__('Lock this location', 'smc').'</label>
								<br>														
									<input type="radio" name="nl_ht" id="islocked3" value="2" class="" />
									<label for="islocked3">'.__('Hide this location','smc').'</label>
								</p>
							</div>
							<div>
								<div id="add_new_loc" loc_id="'.$location_id.'"  elt="' . __("Choose location type", "smc") . '"  enm="' . __("Choose title of new Location", "smc") . '" class="button smc_padding">'.
									__("Add New Location","smc").
								'</div>
							</div>
						</div>				
					</div>'.
			"	</div>
			</div>
		</div>" :
		"<div class='smp-comment'>".__("You can't create children in this Location", "smc")."</div>";
		$html 				.= '</div>';
		return $html;
	}
	static function my_owner_form($location_id)
	{		
		global $Soling_Metagame_Constructor;
		if($owners			= $Soling_Metagame_Constructor->location_has_owners($location_id))
		{
			$html			.='				<div class="lp_owner_container lp-location-owners">';
			foreach($owners as $owner)
			{
				if( $owner > 0 )
				{
					$user1		= get_userdata($owner);
					$html		.= '<div>'.
										get_avatar($owner, 25).
										' 	<span class="lp-owner-loc-cont smc-alert hint hint--left" data-hint="'. __("call to owner", "scm"). '" target_name="owner'.$owner.'">' .
												$user1->display_name.
										'	</span>
									</div>
									<div class="lp-hide" id="owner'.$owner.'">'.
										Direct_Message_Menager::add_new_dm_form(array('adresse_id'=>$owner)).
									'</div>';
				}
			}
			
			$html			.='				</div>';
		}
		if($location_id != 0)
		{
			if(!$owners)
			{
				$html		.= '<div class="lp-location-owners"><p>'.__("No owners presents.", "smc").'</p></div>';
			}
		}
		else
		{
			$html			.= "";
		}
		return $html;
	}
	
	/*
		update Location's parameter by its owner ONLY!!
		@id				- term_id of SMC_Location object
		$update_data	- array:
			'hiding_type'
			'owner1'
			'owner2'
			'owner3'
			'members'
			
	*/
	static function update_my_location($id, $update_data)
	{
		global $UAM, $Soling_Metagame_Constructor;
		if(!$Soling_Metagame_Constructor->cur_user_is_owner($id) )
			return __("You not owner of this Location. And you not may do this manipulations.", "smc") . $id;
		if(!is_array($update_data))	return __("Error data type", "smc");
		$location					= self::get_instance($id);
		$loc_option					= array();
		if(isset($update_data->hiding_type))
		{
			$loc_option['hiding_type']	= (int)$update_data->hiding_type;
			$UAM->update_uam_group($id);
		}
		$loc_option['owner1']		= (int)$update_data['owner_1'];
		$loc_option['owner2']		= (int)$update_data['owner_2'];
		$loc_option['owner3']		= (int)$update_data['owner_3'];
		$loc_option['members']		= '';
		$users						= array();
		if($update_data['members'])
		{
			foreach (@$update_data['members'] as $tt)
			{
				array_push($users, $tt);
			}
			$loc_option['members']	= implode(",", $users);
		}
		$owners						= array(
												$loc_option['owner1'],
												$loc_option['owner2'],
												$loc_option['owner3']
											);
		$users						= array_unique(array_merge($owners, $users));
		$UAM->update_uam_users($id, $users);
		$loc_options				= apply_filters("smc_update_my_location", $loc_options, $id, $update_data);
		$serv						.= __("Location changed: ", "smc"). "<b>". $location->name."</b><BR> ";
		self::update_taxonomy_custom_meta(
											(int)$id,
											$loc_option
										 );
		return $serv;
	}
	static function location_display_tbl0($text="", $location=0, $term_meta=-1, $location_type=-1)
	{
		$location	= is_numeric($location) ? $location : $location->term_id;
		$locs		= static::get_child_location_ids($location);
		$res		= self::read_log($locs, 7);
		$html		= "<h3>".__("Last actions", "smp")."</h3>";
		foreach($res as $r)
		{
			$html	.= "<div class='replic'><span class='replic_time'>". date_i18n('M j, Y  G:i', $r->time)."</span>".$r->content."</div>";
		}
		//insertLog("location_display_tbl0", $html);
		return $html;
	}
}

?>