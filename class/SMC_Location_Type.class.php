<?php
	define('UNVISIBLE_MAP_BEHAVIOR',		0);
	define('HAVENT_POSITION_MAP_BEHAVIOR',	1);
	define('DOT_MAP_BEHAVIOR',				2);
	define('GEOMETRY_MAP_BEHAVIOR',			3);
	add_action('plugins_loaded', function()
	{
			global $map_behoviors;
			$map_behoviors		= array(
				UNVISIBLE_MAP_BEHAVIOR				=> __("Unvisibled", "smc"),
				HAVENT_POSITION_MAP_BEHAVIOR		=> __("Haven't position", "smc"),
				DOT_MAP_BEHAVIOR					=> __("Is Dot", "smc"),
				GEOMETRY_MAP_BEHAVIOR				=> __("Is Geometry", "smc")
			);		
	});
	
	class SMC_Location_Type extends SMC_Post
	{
		static function init()
		{	
			// мета-поля в редактор типа локаций
			add_action('admin_menu',									array(__CLASS__, 'my_extra_fields_location_type'));
			//add_action('save_post_location_type',						array(__CLASS__, 'true_save_box_data'), 10);		
			add_action( 'init', 										array(__CLASS__, 'add_location_type_menu'), 11 );
			add_filter( 'post_updated_messages', 						array(__CLASS__, 'location_type_messages') );		
			//add_filter('manage_edit-location_type_columns', 			array(__CLASS__, 'add_views_column'), 4);
			//add_filter('manage_edit-location_type_sortable_columns', 	array(__CLASS__, 'add_views_sortable_column'));
			//add_filter('manage_location_type_posts_custom_column', 		array(__CLASS__, 'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			//add_filter('pre_get_posts',									array(__CLASS__, 'add_column_views_request'));
			parent::init();
		}
			
			
		//========================================================================
		//
		//	НАСТРОЙКА ТИПОВ ЛОКАЦИЙ В АДМИКЕ
		//
		//========================================================================
		
		static function add_location_type_menu()
		{
			global $smc_main_tor_buttons;
			array_unshift($smc_main_tor_buttons, array('ico'=>"", 'targ'=>__('Map', "smc"), 'comm'=>'get_current_location_map'));
			$labels = array(
				'name' => __('Location type', "smc"),
				'singular_name' => __("Location type", "smc"), // админ панель Добавить->Функцию
				'add_new' => __("add Location type", "smc"),
				'add_new_item' => __("add new Location type", "smc"), // заголовок тега <title>
				'edit_item' => __("edit Location type", "smc"),
				'new_item' => __("new Location type", "smc"),
				'all_items' => __("all Location types", "smc"),
				'view_item' => __("view Location type", "smc"),
				'search_items' => __("search Location type", "smc"),
				'not_found' =>  __("Location type not found", "smc"),
				'not_found_in_trash' => __("no found Location type in trash", "smc"),
				'menu_name' => __("Location type", "smc") // ссылка в меню в админке
			);
			$args = array(
				'labels' => $labels,
				'public' => true,
				'show_in_menu' => "metagame",
				'show_ui' => true, // показывать интерфейс в админ-панели
				'has_archive' => true, 
				'exclude_from_search' => true,
				'menu_icon' =>'dashicons-admin-site', //SMC_URLPATH .'/img/pin.png', // иконка в меню
				'menu_position' => 20, // порядок в меню
				'supports' => array( 'title', 'thumbnail')
				,'capability_type' => 'page'
			);
			register_post_type('location_type', $args);
		}
		
		// see http://truemisha.ru/blog/wordpress/post-types.html
		static function location_type_messages( $messages ) {
			global $post, $post_ID;
		 
			$messages['location_type'] = array( // location_type - название созданного нами типа записей
				0 => '', // Данный индекс не используется.
				1 => sprintf( __('Location type is updated'). '<a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				2 => 'Параметр обновлён.',
				3 => 'Параметр удалён.',
				4 => 'Location type обновлена',
				5 => isset($_GET['revision']) ? sprintf( 'Location type восстановлена из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6 => sprintf( 'Location type опубликована на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				7 => 'Функция сохранена.',
				8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
				9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
				10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
			);
		 
			return $messages;
		}
		
		// мета-поля в редактор типа локаций
		
		static function my_extra_fields_location_type() 
		{
			parent::my_extra_fields();
			//add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func'), 'location_type', 'normal', 'high'  );
			add_meta_box( 'extra_fields2', __('Map Behavior Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func_map'), 'location_type', 'normal', 'low'  );
		}
		static function view_admin_edit($lt)
		{
			$radius				= $lt->get_meta( 'radius');
			$radius				= $radius == "" ? 10 : $radius;
			$may_have_children	= $lt->get_meta( 'may_have_children' );
			$html				= '
			<div class="h_cont">
				<div class="h0" style="width:300px;"> 
					<div class=h7>';
			$html 	.= '<div>
							<label for="slug">'.__('slug', 'smc').' </label>
							<p>
							<input type="text" name="slug" value="' . $lt->get_meta( 'slug') . '" style="font-size:25px;width:100%;"/>
						</div> ';
			
			$html	.= "<div style='margin-bottom:20px;' class='brr'>
							<label for='picto'>".__('picto', 'smc')."</label>
							<input type='text' size=40 id='picto' name='picto' value='" . $lt->get_meta( 'picto') . "'  style='font-size:12px;display:none;'/>
							<p></p>
							<div id='picto1' style='width:60px; height:60px;font-size:55px; margin:-5px 0 0 0; cursor:pointer;'>". $lt->get_meta( 'picto' ) ."</div>
						</div>";
			
			$html 	.= '<div class="brr">
							<p></p>	
							<input type="checkbox" class="css-checkbox" name="use_by_player" id="use_by_player"'.($lt->get_meta( 'use_by_player') == '1' ? ' checked="checked"' : '').' />						
							<label class="css-label" for="use_by_player">'. __('Players can create', 'smc'). '</label>
						</div>
						<div>	
							<input type="checkbox" class="css-checkbox" name="may_have_children" id="may_have_children"'. checked($may_have_children, 1, 0).' />						
							<label class="css-label" for="may_have_children">'. __('Players can create children', 'smc'). '</label>
						</div>
						<div class="brr">
							<p></p>							
							<label for="radius">'. __('Radius', 'smc'). '</label><BR>
							<input type="number" name="radius" id="radius" value="'.$radius.'" />
						</div>
						<div class="brr">
							<label>'.
								__('Color', "smc").
							'</label><p></p>	
							<input class="color" name="lp-color" id="lp-color" value="' . $lt->get_meta( 'color' ) . '">
						</div>';
			global $Ermak_UAM;
			echo $html . "</div></div><div class='h0'> ".apply_filters("smc_location_type_meta", "", $post)."</div></div>"; 
			include SMC_REAL_PATH.'tpl/choose_picto.php' ;
		}
		static function extra_fields_box_func_map( $post)
		{
			global $map_behoviors;			
			$lt					= static::get_instance( $post );
			$hint_behavior		= (int) $lt->get_meta( 'hint_behavior');
			$show_content_type	= (int) $lt->get_meta( 'show_content_type');
			$radius				= (int) $lt->get_meta( 'radius'); 
			$map_behavior		= (int) $lt->get_meta( 'map_behavior');
			$is_pic				= (int) $lt->get_meta( 'is_picto');
			$png1				= $lt->get_meta( 'png_url');
			$svg1				= $lt->get_meta( 'svg_url');
			$png				= $png1 != "" ? $png1 : $svg;
			$png_x				= $lt->get_meta( 'png_x');
			$png_y				= $lt->get_meta( 'png_y');
			$png_w				= $lt->get_meta( 'png_w');
			$png_h				= $lt->get_meta( 'png_h');
			$dot_center			= $png_x == '' ? "" : "<div id='png_center' class='png_center' top='". $png_y ."' left='" . $png_x . "' style=''></div>";
			
			if(!isset($map_behavior))	$map_behavior = HAVENT_POSITION_MAP_BEHAVIOR;		
			$html	= '
			<script>
				jQuery(document).ready(function( $ )
				{	
					jQuery( "#my_image_upload" ).click(function() 
					{
						on_insert_media = function(json)
						{
							//alert(json.url);
							$( "#picto_media_id" ).val(json.url);
							
							
							
								var downloadingImage = new Image();
								downloadingImage.onload = function()
								{								
									$("#png_example").empty().append("<img src=\'"+this.src+"\'>");
									$( "#my_image_upload" ).height( $("#png_example").height() + 0);
									//alert($("#png_example").height());
								};
								downloadingImage.src = json.url;
							
							
							
							//
						}
						open_media_uploader_image();						
					});
					$( "#my_image_upload" ).height( $("#png_example").height() + 0);
				});
			</script>
			<div class="h_cont">
				<div class="h0" style="width:300px;"> 
					<div class=h4>
						<h3>'.
							__("Hint behavior", "smc").
						'</h3>
						<table style="margin-top:20px; min-width:250px;">
							<tr>
								<td>
									<input type="radio" id="hint_behavior0" name="hint_behavior" class="css-checkbox"  value=0 '.checked ($hint_behavior, 0, false).'/>
									<label  class="css-label" for="hint_behavior0">'.__("Pop up", "smc").'</label>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" id="hint_behavior1" name="hint_behavior" class="css-checkbox"  value=1 '.checked ($hint_behavior, 1, false).'/>
									<label  class="css-label" for="hint_behavior1">'.__("Visibled", "smc").'</label>
								</td>
							</tr>
						</table>
					</div>
					<div class=h4>
						<h3>'.
							__("Show Content Type", "smc").
						'</h3>
						<table style="margin-top:20px; min-width:250px;">
							<tr>
								<td>
									<input type="radio" id="show_content_type0" name="show_content_type" class="css-checkbox"  value=0 '.checked ($show_content_type, 0, false).'/>
									<label  class="css-label" for="show_content_type0">'.__("Hide Content", "smc").'</label>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" id="show_content_type1" name="show_content_type" class="css-checkbox"  value=1 '.checked ($show_content_type, 1, false).'/>
									<label  class="css-label" for="show_content_type1">'.__("Show Content in stroke", "smc").'</label>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="h0" style="width:320px;"> 
					<div  class="h7" behavior="'.$map_behavior.'">
					<h3>'.
						__("Map behavior", "smc").
						'</h3>
						<table style="margin-top:20px; width:500px;">
							<tr>
								<td>
									<input type="radio" name="map_behavior" id="radio0" class="css-checkbox"  value=0 '.checked($map_behavior, UNVISIBLE_MAP_BEHAVIOR, false).'/> 
									<label  class="css-label" for="radio0">'. $map_behoviors[UNVISIBLE_MAP_BEHAVIOR] .'</label>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="map_behavior" id="radio1" class="css-checkbox"  value=1 '.checked($map_behavior, HAVENT_POSITION_MAP_BEHAVIOR, false).'/> 
									<label  class="css-label" for="radio1">'.$map_behoviors[HAVENT_POSITION_MAP_BEHAVIOR].'</label>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="map_behavior" id="radio3" class="css-checkbox" value=3 '.checked($map_behavior, GEOMETRY_MAP_BEHAVIOR, false).'/> 
									<label  class="css-label" for="radio3">'.$map_behoviors[GEOMETRY_MAP_BEHAVIOR].'</label>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="map_behavior" id="radio2" class="css-checkbox" value=2 '.checked($map_behavior, DOT_MAP_BEHAVIOR, false).'/> 
									<label class="css-label" for="radio2">'.$map_behoviors[DOT_MAP_BEHAVIOR].'</label>	
									<div id="is_picto1" style="margin:4px 35px;" is_pic="'.$is_pic.'">
										<input class="css-checkbox" type="radio"  id="is_picto_0" name="is_picto" value="0" ' . checked($is_pic, 0, false) . '/>
										<label class="css-label" for="is_picto_0">'.__("Show in map as dot", "smc").'</label><br>
										<input class="css-checkbox" type="radio"  id="is_picto_1" name="is_picto" value="1" ' . checked($is_pic, 1, false) . '/>
										<label class="css-label" for="is_picto_1">'.__("Show in map as picto", "smc").'</label><br>
										<input class="css-checkbox" type="radio"  id="is_picto_2" name="is_picto" value="2" ' . checked($is_pic, 2, false) . '/>
										<label class="css-label" for="is_picto_2">'.__("Show in map as PNG-image or GIF-image", "smc").'</label>
										<br>
										<input class="css-checkbox" type="radio"  id="is_picto_3" name="is_picto" value="3" ' . checked($is_pic, 3, false) . '/>
										<label class="css-label" for="is_picto_3">'.__("Show in map as SVG-image", "smc").'</label>
										
										
									</div>
								</td>
							</tr>
						</table>
						
					</div>
					<div class=h7 id="map_img_src">
						<h3>'.
							__("Image parameters", "smc").
						'</h3>					
						<div id="my_image_upload" class="button" style="padding:10px; float:left; margin-right:3px;">
							<div id="png_example" style="position:relative; display:inline-block; overflow:hidden;">
								<img src="'.$png.'"/>'.$dot_center.
							'</div>
						</div>
						<input name="picto_media_id" id="picto_media_id" type="hidden" value="' . $png . '"/>
						
						
						<div style="display:inline-block; margin-bottom:2px;">
							
							<input id="png_w" name="png_w" type="number" class="css_number" step="1" min="0" value="'.$png_w.'"/>
							<label for="png_w">width</label>						
							<br>
							<input id="png_h" name="png_h" type="number" class="css_number" step="1" min="0" value="'.$png_h.'"/>
							<label for="png_h">height</label>						
							<br>
							<input id="png_x" name="png_x" type="number" class="css_number" step="1" min="0" value="'.$png_x.'"/>
							<label for="png_x">center x</label>
							<input id="mouse_x" type="number" class="css_number" step="1" min="0" value=""/>
							<br>
							<input id="png_y" name="png_y" type="number" class="css_number" step="1" min="0" value="'.$png_y.'"/>					
							<label for="png_y">center y</label>
							<input id="mouse_y" type="number" class="css_number" step="1" min="0" value=""/>	
							
						</div>
					</div>
					<div class=h7 id="map_svg_src">
						<h3>'.
							__("SVG parameters", "smc").
						'</h3>
						<input type="file" accept="svg" name="svg_file">
					</div>
				</div>
				
			</div>
			';
			
			echo $html;
		}
		
		static function save_admin_edit ( $lt ) 
		{
			$arr			= array(
				'slug'					=> esc_attr($_POST['slug']),
				'picto'					=> $_POST['picto'],
				'may_have_children'		=> $_POST['may_have_children']=="on" ? 1 : 0,
				'hint_behavior'			=> $_POST['hint_behavior'],
				'radius'				=> $_POST['radius'],
				'show_content_type'		=> $_POST['show_content_type'],
				'map_behavior'			=> $_POST['map_behavior'],
				'use_by_player'			=> $_POST['use_by_player'] == "on" ? 1 : 0,
				'color'					=> '#'.$_POST['lp-color'],
				'is_picto'				=> 0
			);	
			if($_POST['map_behavior'] == DOT_MAP_BEHAVIOR)
			{
				$arr['is_picto']		= $_POST['is_picto'];
				$png					= $_POST['png_file'];
				switch($_POST['is_picto'])
				{
					case 0: //dot
						break;
					case 1: // picto
						
						break;
					case 2: // png
						$arr['png_url']	= $_POST['picto_media_id'];
						$arr['png_name']= $_POST['png_name'];
						$arr['png_x']	= $_POST['png_x'];
						$arr['png_y']	= $_POST['png_y'];
						$arr['png_w']	= $_POST['png_w'];
						$arr['png_h']	= $_POST['png_h'];						
						break;
					case 3: // svg
						
						break;
				}
			}
			return $arr;
		}
		
		//======================================
		//
		// традиционная вкладка Location Type
		// http://wp-kama.ru/id_995/dopolnitelnyie-sortiruemyie-kolonki-u-postov-v-adminke.html
		//
		//======================================
		
		// редактируем колонки вкладки "Типы Локаций" 	
		static function add_views_column( $columns )
		{
			//$columns;
			$posts_columns = array(
				  "cb" 					=> " ",
				  "IDS" 				=> __("IDs", "smc"),
				  'use_by_player' 		=> __("Use by player", "smc"),
				  'may_have_children' 	=> __("Can create children", "smc"),
				  'picto' 				=> __("picto", "smc"),
				  'image' 				=> __("Image"),
				  "title" 				=> __("Title"),
				  'slug' 				=> __("slug", "smc"),
				  'map_behavior' 		=> __("Map behavior", "smc"),
				  'hint_behavior' 		=> __("Hint behavior", "smc"),
			   );
			return $posts_columns;		
		}
		
		// заполняем колонку данными	
		static function fill_views_column($column_name, $post_id) 
		{		
			switch( $column_name) 
			{		
				case "IDS":
					$color				= get_post_meta($post_id, "color", true);
					if($color == "#FFFFFF")	
						$font			= "color:#111;";
					echo "<br><div class='ids' style='background:".$color.";$font'><span>ID</span>".$post_id."</div>
					<p>
					<div class='button' add_bid='$post_id' post_type='SMC_Location_Type'>" . __("Double", "smc") . "</div>";
					break;	
				case 'use_by_player':
					echo get_post_meta($post_id, 'use_by_player', 1) ? '<img src="'.SMC_URLPATH.'img/check_checked.png">' : '<img src="'.SMC_URLPATH.'img/check_unchecked.png">';
					break;		
				case 'may_have_children':
					echo get_post_meta($post_id, 'may_have_children', 1) ? '<img src="'.SMC_URLPATH.'img/check_checked.png">' : '<img src="'.SMC_URLPATH.'img/check_unchecked.png">';
					break;		
				case 'image':
					echo get_the_post_thumbnail( $post_id, array(56, 56));
					break;
				case "slug":
					echo get_post_meta($post_id, 'slug', 1);
					break;
				case "picto":
					echo "<span style='font-size:33px!important; line-height:33px;'>".get_post_meta($post_id, 'picto', 1)."</span>";
					break;
				case "map_behavior":
					$map_behavior 	= get_post_meta($post_id, 'map_behavior', true) ;
					$is_pic			= (int) get_post_meta($post_id, 'is_picto', true);
					switch($map_behavior)
					{
						case 0:
							_e("Unvisibled", "smc");
							break;
						case 1:
							_e("Haven't position", "smc");
							break;
						case 2:
							switch($is_pic)
							{
								case 0:
									_e("Is Dot", "smc");
									break;
								case 1:
									echo "<span style='font-size:22px!important; line-height:33px;'>".get_post_meta($post_id, 'picto', 1)."</span>";
									break;
								case 2:
									echo "<img src='". get_post_meta($post_id, 'png_url', true) . "'>";
									break;
								case 3:
									_e("Show in map as SVG-image", "smc");
									break;
							}						
							break;
						case 3:
							_e("Is Geometry", "smc");
							break;
					}
					break;
				case "hint_behavior":
					echo get_post_meta($post_id, 'hint_behavior', 1) ? __("Pop up", "smc") : __("Visibled", "smc");
					break;
			}		
		}
		
		// добавляем возможность сортировать колонку
		static function add_views_sortable_column($sortable_columns){
			$sortable_columns['use_by_player'] 	= 'use_by_player';
			$sortable_columns['picto'] 			= 'picto';
			$sortable_columns['slug'] 			= 'slug';
			$sortable_columns['map_behavior'] 	= 'map_behavior';
			$sortable_columns['hint_behavior'] 	= 'hint_behavior';
			return $sortable_columns;
		}
		
		// изменяем запрос при сортировке колонки	
		static function add_column_views_request( $object ){
			switch( $object->get('orderby'))
			{
				case 'use_by_player':
					$object->set('meta_key', 'use_by_player');
					$object->set('orderby', 'meta_value_num');
					break;
				case "picto":
					$object->set('meta_key', 'picto');
					$object->set('orderby', 'meta_value_num');
					break;
				case "slug":
					$object->set('meta_key', 'slug');
					$object->set('orderby', 'meta_value_num');
					break;
				case "hint_behavior":
					$object->set('meta_key', 'hint_behavior');
					$object->set('orderby', 'meta_value_num');
					break;
				case "map_behavior":
					$map_behavior = get_post_meta($post_id, 'map_behavior', 1) ;
					switch($map_behavior)
					{
						case 0:
							_e("No present", "smc");
							break;
						case 1:
							_e("Haven't position", "smc");
							break;
						case 2:
							_e("Is Dot", "smc");
							break;
						case 3:
							_e("Is Geometry", "smc");
							break;
					}
					break;
			}
		}
		static function get_type()
		{
			return "location_type";
		}	
	}
?>