<?php
	
	/*
	/	I changed User-Access-Manager plugin in:
	/	1. UamAccessHandler_class.php:
	/		1.1. p 615:	WHERE (tt.taxonomy = 'category' OR tt.taxonomy = 'location')
	/		1.2. p 621: WHERE (gc.object_type = 'category' OR gc.object_type = 'location')
	/		1.3. p 41-42:  
		'role',
		'location'
		
	/	2. UserAccessManager_class.php:
	/		2.1. p 1637: 			
			elseif ($oItem->object == 'location') 
			{
                $oObject = $this->getCategory($oItem->object_id);
                $oCategory = $this->_getTerm('location', $oObject);
                if ($oCategory !== null && !$oCategory->isEmpty) {
                    $oItem->title .= $this->adminOutput($oItem->object, $oItem->object_id);
                    $aShowItems[] = $oItem;
                }
            } 
	*/
	
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );	
	class UAM
	{
		protected $options;
		
		function __construct() 
		{
			$this->options				= get_option(SMC_ID);			
				
			if(is_plugin_active('user-access-manager/user-access-manager.php'))
			{
				add_action('init', 						array($this, 'add_location_to_uam'),0 );
				add_action('location_edit_form',		array($this, 'showLocationEditForm'));
				//add_action('edit_location',			array($this, 'saveLocationData'), 0);
				add_action('edit_location',				array($this, 'update_uam_group'), 99);
				//add_action('create_location',			array($this, 'saveLocationData'), 0);
				add_action('create_location', 			array($this, 'create_uam_group'), 99);
				add_action("delete_location",			array($this, 'delete_uam_group'), 0);
				add_action("init",						array($this, 'plugins_loaded'));
				
				//add_filter('the_posts', 				array($this, 'showPost'));
			}
		}
		function plugins_loaded()
		{
			if (is_plugin_active('user-access-manager/user-access-manager.php'))
			{
				require_once(SMC_REAL_PATH.'class/Ermak_UAM.php');
				require_once(SMC_REAL_PATH.'class/Ermak_UAM_Handler.php');					
				global $Ermak_UAM;
				$Ermak_UAM = new Ermak_UAM; 
			}		
		}
		function is_install()
		{
			return class_exists("Ermak_UAM");//is_plugin_active('user-access-manager/user-access-manager.php');
			//return is_plugin_active('user-access-manager/user-access-manager.php');
		}
		
		//access for current user
		public function isAccessLocation($location_id)
		{				
			if(!$this->is_install()) 	return true;
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();			
			return $oUamAccessHandler->checkObjectAccess('category', $location_id);
		}
		public function isVisibleLocation($location_id)
		{	
			$loc_option					= SMC_Location::get_term_meta($location_id);
			$visible					= $loc_option['hiding_type'];
			//echo $visible."<BR>";
			if($visible<2)
			{
				return true;
			}
			else
			{
				return is_user_logged_in() ? $this->isAccessLocation($location_id): false;
			}
			
		}
		public function is_current_user_member($location_id)
		{
			if(!$this->is_install()) 	return true;
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler, get_option( "uam_group_$location_id"));
			return $oUamUserGroup->objectIsMember("user", get_current_user_id());
		}
		public function is_current_user_can_write($location_id)
		{
			if(!$this->is_install()) 	return true;
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler, get_option( "uam_group_$location_id"));
			if($oUamUserGroup->getWriteAccess())	return true;
			return $oUamUserGroup->objectIsMember("user", get_current_user_id());	
		}
		
		public function is_current_user_can_add_location($location_id)
		{
			return true;//!(!current_user_can("administartor") && $location_id==0);			
		}	
			
		function showPost($aPosts = array())
		{
			//var_dump($aPosts);
			$aShowPosts = array();
			foreach ($aPosts as $iPostId)
			{
				if ($iPostId !== null) 
				{
					$locs				= wp_get_post_terms($iPostId->ID, 'location', array("fields" => "ids"));
					//var_dump($locs);
					$yes				= false;
					foreach($locs as $loc)
					{
						if($this->isVisibleLocation($loc))
						{
							$yes		= true;
							break;
						}
					}
					if($yes)			$aShowPosts[] = $iPostId;
                }
            }
			return  $aShowPosts;
		}
		
		public function showPostSql($sSql)
		{   
			if(!$this->is_install()) return;
			global $wpdb;
			$aExcludedPosts = $oUamAccessHandler->getExcludedPosts();
			
			if (count($aExcludedPosts) > 0) {
				$sExcludedPostsStr = implode(",", $aExcludedPosts);
				//AND igc.object_type = 'location'
				$sSql .= " AND $wpdb->posts.ID NOT IN($sExcludedPostsStr) ";
			}		
			
			return $sSql;
		}
		//=====================================
		//
		// update UAM plugin
		//
		//=====================================
		
		function add_location_to_uam()
		{		
			// UAM plugin registating
			if(!$this->is_install()) return;
			global $Ermak_UAM;
			$oUamAccessHandler 	= $Ermak_UAM->getAccessHandler();
			$ot					= $oUamAccessHandler->getObjectTypes();
			if(!$oUamAccessHandler->isValidObjectType('location'))
			{
				array_push($ot, 'location');
				$oUamAccessHandler->registerPlObject(
														array(
																'name'				=> 'location',
																'reference'			=> '',
																'getFull'			=> '',
																'getFullObjects'	=> ''
															 )
													);
			}
			//var_dump ($ot);
		}
		
		function saveLocationData($iLocationId)
		{
			if(!$this->is_install()) return;
			global $Ermak_UAM;
			$Ermak_UAM->savePlObjectData('location', $iLocationId);
			
			
		}
		public function showLocationEditForm($location)
		{
			if(!$this->is_install()) return;		
			include SMC_REAL_PATH.'tpl/locationEditForm.php';		
			
		}
		
		public function is_location_added()
		{
			if(!$this->is_install()) return;
			global $Ermak_UAM;
			$oUamAccessHandler 	= $Ermak_UAM->getAccessHandler();	
			$ot					= $oUamAccessHandler->getObjectTypes();
			foreach($ot as $type)
				echo $type.", ";
		}
		
		//after create Location add them UAM group
		function create_uam_group($term_id)
		{
			if(!$this->is_install()) 	return true;
			$location					= SMC_Location::get_instance($term_id);
			$meta						= SMC_Location::get_term_meta($term_id);
			/**/
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler);
			$oUamUserGroup->setGroupName($location->name);
			$oUamUserGroup->setGroupDesc("group for ". $this->options['main_map_name']);
			$read 						= $meta['hiding_type'] >= 2 ? "group" : "all";
			$write						= $meta['hiding_type'] >  0 ? "group" : "all";	
			$oUamUserGroup->setReadAccess($read); // all, group
			$oUamUserGroup->setWriteAccess($write); // all, group
			$oUamAccessHandler->addUserGroup($oUamUserGroup);
			$oUamUserGroup->addObject("category", $term_id);
			$oUamUserGroup->addObject("user", get_current_user_id());		
			$oUamUserGroup->save();
			
			//$meta['uam_group']			= '0';//$oUamUserGroup->getID();
			add_option( "uam_group_$term_id", $oUamUserGroup->getID());
			
		}
		
		function update_uam_group($term_id)
		{
			//return;
			if(!$this->is_install()) 	return true;
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();
			$location					= SMC_Location::get_instance($term_id);//get_term($term_id, "location");	
			$meta						= SMC_Location::get_term_meta($term_id); //get_option( "taxonomy_$term_id" );
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler, get_option( "uam_group_$term_id"));
			$oUamUserGroup->setGroupName($location->name);
			$read 						= $meta['hiding_type'] >= 2 ? "group" : "all";
			$write						= $meta['hiding_type'] >  0 ? "group" : "all";
			$oUamUserGroup->setReadAccess($read); // all, group
			$oUamUserGroup->setWriteAccess($write); // all, group
			$oUamUserGroup->save();
			//SMC_Location::update_term_meta($term_id, $meta);
			//update_option( "taxonomy_$term_id", $meta );
		}
		
		function delete_uam_group($term_id)
		{
			if(!$this->is_install()) 	return true;
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();	
			$meta						= SMC_Location::get_term_meta($term_id);//get_option( "taxonomy_$term_id" );	
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler, get_option( "uam_group_$term_id"));	
			$oUamUserGroup->delete();		
		}
		
		function get_uam_users($uamGroupId)
		{
			if(!$this->is_install()) 	return true;
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();	
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler, get_option( "uam_group_$uamGroupId"));	
			return $oUamUserGroup->getObjectsFromType("user");		
		}
		function get_uam_user_locations($user_id=-1)
		{
			$locations					= array();
			if(!$this->is_install()) 	return $locations;
			global $Ermak_UAM;
			if($user_id == -1)
				$user_id				= get_current_user_id();
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();			
			$aUamUserGroups 			= $oUamAccessHandler->getUsergroupsForObject("user", $user_id);
			$aUserGroupsForObject = $aUamUserGroups;
			if ($aUamUserGroups != Array()) 
			{
				foreach ($aUamUserGroups as $oUamUserGroup) 
				{
					//echo $oUamUserGroup->getReadAccess()."<br>";
					if($oUamUserGroup->getReadAccess() != "all")
						$locations		= array_merge($locations, $oUamUserGroup->getObjectsFromType("category"));
				}
				return $locations;
			}
			else
				return $locations;		
		}
		function update_uam_users($uamGroupId, $array_users_id)
		{
			if(!$this->is_install()) 	return true;
			global $Ermak_UAM;
			$oUamAccessHandler 			= $Ermak_UAM->getAccessHandler();	
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler, get_option( "uam_group_$uamGroupId"));	
			$users						= $oUamUserGroup->getObjectsFromType("user");
			foreach($users as $user)
			{
				$oUamUserGroup->removeObject('user', (int)$user->id);
			}
			foreach($array_users_id as $u)
			{
				$oUamUserGroup->addObject('user', $u);
			}			
			$oUamUserGroup->save();			
			$oUamUserGroup				= new UamUserGroup($oUamAccessHandler, $uamGroupId);	
			$users1						= $oUamUserGroup->getFullUsers();
			//var_dump($users1);
			return $users;
		}
	}
	$UAM		= new UAM;
?>