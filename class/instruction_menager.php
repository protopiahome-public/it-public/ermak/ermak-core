<?PHP

	//========================================
	//
	//	INIT INSTRUCTION TYPE
	//
	//========================================
	
	class Instruction_Menager
	{
		function __construct() 
		{
			//register direct message type
			add_action( 'init', 					array($this, 'add_Instruction_type'), 14 );
			add_filter( 'post_updated_messages', 	array($this, 'Instruction_messages') );
		}
		function add_Instruction_type()
		{
			$labels = array(
				'name' => __('Instructions', "smc"),
				'singular_name' => __("Instruction", "smc"), // админ панель Добавить->Функцию
				'add_new' => __("add Instruction", "smc"),
				'add_new_item' => __("add new Instruction", "smc"), // заголовок тега <title>
				'edit_item' => __("edit Instruction", "smc"),
				'new_item' => __("new Instruction", "smc"),
				'all_items' => __("all Instruction", "smc"),
				'view_item' => __("view Instruction", "smc"),
				'search_items' => __("search Instruction", "smc"),
				'not_found' =>  __("Instruction not found", "smc"),
				'not_found_in_trash' => __("no found Instruction in trash", "smc"),
				'menu_name' => __("Instructions", "smc") // ссылка в меню в админке
			);
			$args = array(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true, // показывать интерфейс в админке
				'has_archive' => true, 
				'exclude_from_search' => true,
				'menu_icon' =>'dashicons-admin-site', //SMC_URLPATH .'/img/pin.png', // иконка в меню
				'menu_position' => 20, // порядок в меню
				'supports' => array( 'title', 'editor', 'thumbnail')
				,'show_in_nav_menus' => true
				,'show_in_menu' => "metagame"
				,'capability_type' => 'page'
			);
			register_post_type('Instruction', $args);		
		}
		
		//
		//
		//
		//
		//
		
		function add_instruction_menu_items()
		{
			global $Soling_Metagame_Constructor;
			if($Soling_Metagame_Constructor->options['instruction_menu_enabled'])	
			{
				//return;
			}
			
			//$locations = get_nav_menu_locations();
			$locations = wp_get_nav_menus();
			echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>';
			if(count($locations)==0)
			{
				echo __('No search one location  menus','smc').'</b></p></div>';
				return;
			};
			foreach($locations as $item)
			{
				//echo "<br>term_id = ";
				//var_dump($item->term_id);
				//return;
				$add = wp_update_nav_menu_item((int)$item->term_id, 0,
																		array(
																				'menu-item-type' 		=> 'custom',
																				'menu-item-url'    		=> '/?instruction='.__("Instructions","smc"),
																				'menu-item-title'  		=> __("Instructions","smc"),
																				'menu-item-attr-title'  => __("Instructions","smc"),
																				"menu-item-status" 		=> "publish",
																			)
																		);
				//echo "<br>";
				//var_dump($add);
				$instrs			= get_posts(array("post_type"=>"Instruction"));				
				foreach($instrs as $instr)
				{
					if($instr->post_name  == __("Instructions","smc"))	return;
					wp_update_nav_menu_item((int)$item->term_id, 0,
																		array(
																				'menu-item-type' 		=> 'custom',
																				'menu-item-parent-id' 	=> $add,
																				'menu-item-url'    		=> '/?instruction='.$instr->post_name ,
																				'menu-item-title'  		=> $instr->post_title,
																				'menu-item-attr-title'  => __("Instructions","smc"),
																				"menu-item-status" 		=> "publish",
																			)
																		);
					//return;
				
				}
				break;
			}
			//return;
			
			$items 				= wp_get_nav_menu_items($locations[0]);
			$Soling_Metagame_Constructor->options['instruction_menu_enabled']	= 1;
			update_option(SMC_ID, $Soling_Metagame_Constructor->options);
			//echo __('Instructions Menu created successfull.','smc');
			echo '</b></p>'.$items.'</div>';			
			return $items;
		}
		
		function remove_instruction_menu_iems()
		{
			global $Soling_Metagame_Constructor;
			$item_id			= $Soling_Metagame_Constructor->options['instruction_menu_enabled'];
			
			$instrs				= get_posts(array("post_type"=>"Instruction"));
			foreach($instrs as $instr)
			{
				unregister_nav_menu($instr->post_title);
			}
			unregister_nav_menu(__("Instructions","smc"));
			 $Soling_Metagame_Constructor->options['instruction_menu_enabled'] = null;
			 update_option(SMC_ID, $Soling_Metagame_Constructor->options);	
		}
		
		// see http://truemisha.ru/blog/wordpress/post-types.html
		function Instruction_messages( $messages ) {
			global $post, $post_ID;
		 
			$messages['Instruction'] = array( // Instruction - название созданного нами типа записей
				0 => '', // Данный индекс не используется.
				1 => sprintf( __('Instruction is updated', 'smc'). '<a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				2 => 'Параметр обновлён.',
				3 => 'Параметр удалён.',
				4 => 'Instruction обновлена',
				5 => isset($_GET['revision']) ? sprintf( 'Instruction восстановлена из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6 => sprintf( 'Instruction опубликована на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				7 => 'Функция сохранена.',
				8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
				9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
				10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
			);
		 
			return $messages;
		}
		//============================
		// 
		//	INSTRUCTION TAXONOMY
		//
		//============================
		function register_instructions_taxonomy() 
		{
			$labels = array(
				'name' => __('Instructions', "smc")
				,'singular_name' => __('Instructions', "smc")
				,'search_items' => __('Search Instructions', "smc")
				,'all_items' => __('All Instructions', "smc")
				,'parent_item' => __('Parent Instructions', "smc")
				,'parent_item_colon' => __('Parent Instructions:', "smc")
				,'edit_item' => __('Edit Instructions', "smc")
				,'update_item' => __('Update Instructions', "smc")
				,'add_new_item' => __('Add New Instructions', "smc")
				,'new_item_name' => __('New Instructions Name', "smc")
				,'menu_name' => __('Instructions', "smc")
			); 

			$args = array(
				'label' => 'Instructions' // определяется параметром $labels->name
				,'labels' => $labels
				,'public' => false
				,'show_in_nav_menus' => false // равен аргументу public
				,'show_ui' => false 	// равен аргументу public
				,'hierarchical' => false
				,'update_count_callback' => ''
				,'rewrite' => array( 'slug' => 'Instructions' )
				,'query_var' => 'Instructions'
				,'capabilities' => array( 'manage_terms')
				,'_builtin' => false
			);
			register_taxonomy('Instructions', 'page', $args);
		}
		//
		//========================================================
		//
		//	Создаем Инструкции
		//
		//======================================================
		static function init_Instructions()
		{
			init_textdomain();
			$get_p				= get_posts(array("post_type"=>"Instruction"));
			if(count($get_p)!=0)return;
			
			$types				= array(
											array(
													'post_name'=> "Instructions"
													,"thumbnail"=>"lp_planet_system.jpg"
													,'content'=>"Instruction0.txt"
													),
											array(
													'post_name'=> "Categories and Locations"
													,"thumbnail"=>"lp_planet_system.jpg"
													,'content'=>"Instruction1.txt"
													),
											array(
													'post_name'=> "How pablish Post or Commentary "
													,"thumbnail"=>"lp_planet_system.jpg"
													,'content'=>"Instruction2.txt"
													),
											array(
													'post_name'=> "Photos publicate"
													,"thumbnail"=>"lp_planet_system.jpg"
													,'content'=>"Instruction3.txt"
													),
											array(
													'post_name'=> "Video publicate"
													,"thumbnail"=>"lp_planet_system.jpg"
													,'content'=>"Instruction4.txt"
													)
										);
				foreach($types as $type)
				{
					continue;
					$url							= SMC_URLPATH."class/".$type['content'];
					$contentss						= $url."<BR>";
					$res 							= fopen($url, 'rb');
					if (false !== $res) 
					{
						$contentss .= fread($res, filesize($url));						
					} else 
					{
						$contentss .= $type['content'];
					}
					fclose($res);
					
					$new_loctype_ID 				= wp_insert_post(array(
																			  "comment_status"	=> 'closed'
																			, 'post_name'		=> $type['post_name']
																			, 'post_status' 	=> 'publish'
																			, "post_title"		=> __($type['post_name'], "smc")
																			, "post_type"		=> "Instruction"
																			, "post_content"	=> $contentss
																			)
																	);
					
				}
				
		}
		
	}
?>