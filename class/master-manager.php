<?php
	class Master_Menager{
		
		public $master_pade_id;
		
		function __construct()
		{	
			//register Master type
			add_action( 'init', 					array($this, 'add_master_type'), 10 );
			add_filter( 'post_updated_messages', 	array($this, 'master_messages') );
			
			// мета-поля в редактор
			add_action('admin_menu',				array($this, 'my_extra_fields_master'));
			add_action('save_post_master',			array($this, 'true_save_box_data'));
			add_filter('manage_edit-master_columns',		 		array($this, 	'add_views_column'), 4);
			add_filter('manage_edit-master_sortable_columns', 		array($this,	'add_views_sortable_column'));
			add_filter('manage_master_posts_custom_column', 		array($this,	'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			
			
		}
		public static function install()
		{
			
			// init location page
			$my_post = array(
								  'post_title'   		=> __("Masters", "smc"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smc_masters_page]",
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);
			return wp_insert_post( $my_post );
		}
		public static function deinstall()
		{
			$options									= get_option(SMC_ID);
			wp_delete_post( $options["masters_page_id"], true);
		}
		
		function add_master_type()
		{
			$labels = array(
				'name' => __('Masters', "smc"),
				'singular_name' => __("Master", "smc"), // админ панель Добавить->Функцию
				'add_new' => __("add Master", "smc"),
				'add_new_item' => __("add new Master", "smc"), // заголовок тега <title>
				'edit_item' => __("edit Master", "smc"),
				'new_item' => __("new Master", "smc"),
				'all_items' => __("all Masters", "smc"),
				'view_item' => __("view Master", "smc"),
				'search_items' => __("search Master", "smc"),
				'not_found' =>  __("Master not found", "smc"),
				'not_found_in_trash' => __("no found Master in trash", "smc"),
				'menu_name' => __("Masters", "smc") // ссылка в меню в админке
			);
			$args = array(
				'labels' => $labels,
				'public' => false,
				'show_ui' => true, // показывать интерфейс в админке
				'has_archive' => true, 
				'exclude_from_search' => true,
				//'menu_icon' =>'dashicons-admin-site', //SMC_URLPATH .'/img/pin.png', // иконка в меню
				'menu_position' => 20, // порядок в меню
				'show_in_menu' => "metagame",
				'supports' => array( '' )
				,'capability_type' => 'page'
			);
			register_post_type('master', $args);
		}
		
		// see http://truemisha.ru/blog/wordpress/post-types.html
		function master_messages( $messages ) {
			global $post, $post_ID;
		 
			$messages['master'] = array( // master - название созданного нами типа записей
				0 => '', // Данный индекс не используется.
				1 => sprintf( __('Master is updated'). '<a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				2 => 'Параметр обновлён.',
				3 => 'Параметр удалён.',
				4 => 'Location type обновлена',
				5 => isset($_GET['revision']) ? sprintf( 'Location type восстановлена из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6 => sprintf( 'Master опубликована на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				7 => 'Функция сохранена.',
				8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
				9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
				10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
			);
		 
			return $messages;
		}
		
		// мета-поля в редактор
		
		function my_extra_fields_master() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(&$this, 'extra_fields_box_func'), 'master', 'normal', 'high'  );
		}
		function extra_fields_box_func( $post )
		{	
							
			$html 	.= '<div><p>
							<label for="user_id">'.__('User', 'smc').' </label>
							<p>'.
							wp_dropdown_users(
												array(
														'name' 				=> 'author', 
														'echo'				=> false, 
														'selected'			=> get_post_meta($post->ID, 'user',true),
														'show'				=> 'display_name',
														'name'				=> 'user_id',
														'id'				=> 'user_id',
														'multi'             => true,
													  )
											  ).
						'</div>
						<div><p>
							<label for="descr">'.__('Description').' </label>
							<p>
								<input type="text" name="descr" value="' . get_post_meta($post->ID, 'descr',true) . '" style="font-size:25px;width:100%;"/>'.
							
						'</div>';
			
		 
			echo $html;
			wp_nonce_field( basename( __FILE__ ), 'master_metabox_nonce' );
		}
		
		function true_save_box_data ( $post_id ) 
		{
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['master_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['master_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;
			// теперь также проверим тип записи	
			global $table_prefix;
			global $wpdb;
			update_post_meta($post_id, 'user', 	esc_attr($_POST['user_id']));
			update_post_meta($post_id, 'descr', esc_attr($_POST['descr']));
			$user					= get_userdata($_POST['user_id'])->display_name;
			$wpdb->update(
							$table_prefix."posts",
							array("post_title"	=> $user),
							array("ID" 			=> $post_id)
						);
			
			return $post_id;
		}
		
		public static function get_master_page($contents=null)
		{
			$mp							= is_master_page();
			if($mp)						return $mp;
			if(!isset($contents))		$contents		= "Content of master_page";
			$args			= array(
										'post_title'	=> __("Masters", "smc"),
										'post_content'  => $contents,
										'post_status'   => 'publish',
										'post_type' 	=> 'page'
									);
			$master_post	= wp_insert_post($args);
			add_post_meta($master_post, "Master_page", true);
		}
		function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "title" 			=> __("Name"),
				  "description" 	=> __("Description"),
			   );
			return $posts_columns;			
		}
		// добавляем возможность сортировать колонку
		function add_views_sortable_column($sortable_columns){
			$sortable_columns['description'] 	= 'description';
			return $sortable_columns;
		}	
		// заполняем колонку данными	
		function fill_views_column($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{			
				case 'description':
					echo get_post_meta($post_id, 'owner_id', 1);
					break;
			}
		}
		
		
		public static function is_master_page()
		{
			$arg			= array(
										'meta_key'=>"Master_page"
										,'meta_value' => true
										,'post_type' => 'page'
									);
			$pages			= get_pages($arg);
			wp_reset_postdata();
			if($pages)		return $pages[0];
			return false;
		}
		
		public function draw_master($master_postdata, $i=0, $is_echo=true)
		{
			global $smc_height;	
			$master_userdata		= get_userdata(get_post_meta($master_postdata->ID, "user", true));
			$d= get_option(SMC_ID);
			$txt ='<div class="master-cont" id="master-cont-'.$i.'" style="height:'.($smc_height).'px; background:'.$d["fills"][$i%9][0].'; ">
					<div style="float:left;width:100%; height:100%;">
						<div name="personal"  num="'.$i.'"  user="'.$master_userdata->ID.'" class="lp-master-personal" style="background:'.$d["fills"][$i%9][0].'; ">
							<div style="height:35px;">
							</div>
							<div class=" hint hint--top smc-alert" data-hint="'.__("Direct Message","smc").'"  target_name="master_dm_win'.$master_userdata->ID.'"  >
								<div class="master-avatar">'.
									get_avatar($master_userdata->ID, 90).
								'	<span><i class="fa fa-comment"></i></span>
								</div>
							</div>
							<div class="master-name">'.
								$master_userdata->display_name.
							'</div>
							<div class="lp-hide" id="master_dm_win'.$master_userdata->ID.'">'.
								Direct_Message_Menager::add_new_dm_form(array("adresse_id"=>$master_userdata->ID, "post_title_prefix"=>__("to Master: ", "smc"))).
							'</div>
						</div>
						<div name="descr" class="master-descr" style="background:'.$d["fills"][$i%9][1].'; width:150px; ">'.
							get_post_meta($master_postdata->ID, "descr", true).
						'</div>
						<div name="contacts" class="master-contacts" style="padding: 0 5px 0 10px ; background:'.$d["fills"][$i%9][1].'; height:145px; width:145px; ">
							<span class=" hint  hint--top" data-hint="'.__("Phone").'" id="phone" user="'.$master_userdata->ID.'"><i class="fa fa-phone-square"></i></span>
							<span class=" hint  hint--top" data-hint="'.__("Skype").'" id="skype" user="'.$master_userdata->ID.'"><i class="fa fa-skype"></i></span>
							<span class=" hint  hint--top" data-hint="'.__("e-mail").'" id="mail"  user="'.$master_userdata->ID.'"><i class="fa fa-envelope"></i></span>
							<span class=" hint  hint--top" data-hint="'.__("VK").'" id="vk"    user="'.$master_userdata->ID.'"><i class="fa fa-vk"></i></span> 
						</div>
						<!--div name="direct-message" class="master-direct-message" style="top:235px;">
							<a href="#" class="master-dm-button" style=""><i class="fa fa-edit"></i>'.__("Direct Message", "smc").'</a>
						</div-->
					</div>
					<div class="master-message" style="width:400px; height:100%; display:inline-block; ">
						<div style="margin:5px; border:#000 1px dotted;wisth:100%; height:100%; background:red;">
						</div>
					</div>
					<div id="master-dm_'.$master_userdata->ID.'" class="lp-hide">
						<div class="box-modal-dialog">
							<h3>'.__("Direct Message","smc")." " . $master_userdata->display_name .'</h3>
							<p>'.
							__("Title").
						'	</p>
							<input id="new_post_title" type="text" style="width:400px;"/>
							<p>'.
							__("Text").
						'	</p>
							<textarea id="new_post_text" style="width:400px; height:120px;"> </textarea>
							<p style="margin-top:20px;">
								<span id="send_master_dm_nuttom" user="'.$master_userdata->ID.'" class="lp-location-exec-button">' . __('Submit') . '</span>
							</p>
						</div>
					</div>
				</div>';
			if($is_echo)
				echo $txt;
			return $txt;
		}
		function insert_master_page()
		{
			$master_pade_id;
		}
	}
?>