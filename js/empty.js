var jssor_slider3, podval_slider;
function jssor_slidera()
{
	if(jQuery("#mfcc").size()==0) return;
	jQuery("[u=navigator]").width(width);
	var options = {
		$DragOrientation: 1,
		
		$NavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
			$Class: $JssorNavigator$,                       //[Required] Class to create navigator instance
			$Rows:1,
			$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
			$AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
			$Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
			$Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
			$SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
			$SpacingY: 10,                                   //[Optional] Vertical space between each item in pixel, default value is 0
			$Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
		},
		/**/
	};
	function OnSlidePark(slideIndex, fromIndex) 
	{
		if(fromIndex ==  slideIndex )	return;
		var slide_id					= jQuery(".eb_slider_container:eq("+ slideIndex + ")").attr("slide_id");
		send(['eb_refresh_mobile', slide_id], false);
		jQuery("[slide_id=" + slide_id + "] .eb_refresh").find(".fa-refresh").addClass("fa-spin");
		//jQuery("[slide_id=" + fromIndex + "]").empty().append("<div class='eb_refresh'><i class='fa fa-refresh'></i></div>");
    }
	
	jssor_slider3 = new $JssorSlider$("mfcc", options);	
    jssor_slider3.$On($JssorSlider$.$EVT_PARK, OnSlidePark);	

	//resize
	jQuery(window).resize(function(evt)
	{
		var parentHeight = jQuery("body").height();//jssor_slider3.$Elmt.parentNode.clientHeight;
		var sliderOriginalWidth = jssor_slider3.$GetOriginalWidth();
		var sliderOriginalHeight = jssor_slider3.$GetOriginalHeight();
		
		//var newWidthToFitParentContainer = parentHeight / sliderOriginalHeight * sliderOriginalWidth;
		//jssor_slider3.$SetScaleWidth(newWidthToFitParentContainer);
		//var parentWidth = jssor_slider3.$Elmt.parentNode.clientWidth;
		//$JssorUtils$.$SetStyleLeft(jssor_slider3.$Elmt, (parenWidth - newWidthToFitParentContainer) / 2);
		jQuery("[u=slides]").css({"height":parentHeight+"px"});;
		jQuery(".container").css({"height":parentHeight+"px"});;
		jQuery(".eb_slider_container").css({"height":parentHeight+"px"});;
	});
}
function goods_batch_podval_slider()
{
	jQuery(function($)
	{
		if($("#ldgbspodval").size() == 0) return;
		var ww		= ($(window).width() - 282);
		var col		= Math.floor(ww/103);
		var www		= col * 103 + 3;
		$("#ldgbspodval").css({"left":"60px", position:"absolute",  width:www + "px", height:'152px'});
		$("#ldgbspodval").append("<div u='slides' id='gb_slider1'  style='cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width:" + www + "px; height: 150px;'></div>");
		$("#ldgbspodval").append('<!-- Arrow Left --><span u="arrowleft" id="gbarleft" class="jssor_1_left"  style="top:0px; left:-59px;"></span>');
		$("#ldgbspodval").append('<!-- Arrow Right --><span u="arrowright" id="gbarright" class="jssor_1_right" style="top:53px; left:-59px;"></span>');
		$("#gbnavi").css({width:ww + "px"});
	
		var i  = 0,
			ii = 0;		
		$("#ldgbspodval").children(".gbpiccont").each(function(num, elem)
		{	
			//console.log(i%col, i, col);
			if(ii%col == 0)
			{
				$("#gbnavi").append("<span class='navig2_buttons' num='" + i + "'></span>");
				i++;
				$( "#gb_slider1" ).append( "<div id='gb_slide_" + i + "' style='position: absolute; overflow: hidden; left: 0px; top: 0px; width:" + www + "px; height: 150px;'></div>" );				
				$(elem).appendTo("#gb_slide_" + i);
			}
			else
			{
				$(elem).appendTo("#gb_slide_" + i);
			}
			ii++;
		});
		var options = { 
			$DragOrientation: 1,		
			$NavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
				$Class: $JssorNavigator$, 
			} 
		}
		function OnSlidePark(slideIndex, fromIndex) 
		{
			if(fromIndex ==  slideIndex )	return;
			$(".navig2_buttons").removeClass("naviactive");
			$("#gbnavi [num=" + slideIndex + "]").addClass("naviactive");
		}
		$("#gbarleft").click(function(evt)
		{
			podval_slider.$Prev();
		})
		$("#gbarright").click(function(evt)
		{
			podval_slider.$Next();
		})
		$(".navig2_buttons").click(function(evt)
		{
			var num		= parseInt($(this).attr('num'));
			//a_alert(num);
			podval_slider.$PlayTo(num);
		});
		podval_slider = new $JssorSlider$("ldgbspodval", options);	
		podval_slider.$On($JssorSlider$.$EVT_PARK, OnSlidePark);
		$(".navig2_buttons:first").addClass("naviactive");
	})
};
 
jQuery( function($)
{
	$("#home").click(function(evt)
	{
		window.location.replace('<?php echo home_url();?>');
	});
	
	resized			= function()
	{
		var podval	= 200;
		$(window).scrollTop(0);
		var width	= $(window).width();
		var height	= $(window).height();
		$('.eb_screen_cont, #mfcc, #mfcc>[u]').width(width);
		$('.eb_screen_cont, #mfcc, #mfcc>[u]').height(height + 25);
		$('#tbl, .tbl').width(width);
		$('#tbl, .tbl').height(height + 25);
		if($("#tbl0").size())
		{
			$("#tbl_table").width(width);
			$("#tbl_table").height(height);
			$("#tbl0").height(height - podval);
			$("#tbl0").width(width - 430);
			$("#tbl0 .cell_0").height(height - podval);
			$("#tbl0 .cell_0").width(width - 430);
			$("#tbl20").height(podval);
			$("#tbl1").height((height - podval) / 2 + 70);
			$("#tbl1 .cell_0").height((height - podval) / 2 + 70);
		}
		//$("#tbl11").height((height - podval)/2);
		//$("#tbl11 .cell_0").height((height - podval)/2);
		if($("#eb_login_cont").size())
			$("#eb_login_cont").offset({left: (width - $("#eb_login_cont").width())/2, top: (height - $("#eb_login_cont").height())/2});
		$(".bb_container").each(function(num, elem)
		{
			//a_alert( height + " - " +(height - $(elem).position().top - 40) );
			$(elem).height( height - $(elem).position().top - 220 );
		});
	}
});
jQuery( document ).ready(function( $ ) 
{
	resized();
	$(window).resize(function() 
	{
		resized();
	});
	$("[conv_id]").live({click:function(e)
	{
		if(confirm(__("Are you shure?")))
		{
			//a_alert($(this).attr("batch_picto_id"));
			send(["convert_goods_batch", $(this).attr("batch_picto_id")]);
		}
	}});
	jssor_slidera();   
	goods_batch_podval_slider();
	
	
});
