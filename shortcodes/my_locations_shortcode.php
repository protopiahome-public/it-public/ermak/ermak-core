<?php
	function locations_page()
	{ 
		if(!is_user_logged_in())
		{
			return "<div class='smp-comment'>".__("You must logged in!", 'smp')."</div>
			<div><a href='".wp_login_url( home_url())."' title='Login'>".__('Login', 'smc')."</a></div>";
		}
		echo "<div id=smc_content>" . get_location_page() . "</div>";
	}
	function get_location_page()
	{
		global $UAM;	
		global $user_iface_color;			
		global $Soling_Metagame_Constructor;
		
		$serv				= "";
		$comm_visible		= "none";
		
		$all_locations1		= get_terms(SMC_LOCATION_NAME, array("number"=>0, 'orderby'=>'name', "hide_empty"=>false));
		$all_locations		= array();
		$all_loc_types		= array();
		foreach($all_locations1 as $location)
		{
			if( $Soling_Metagame_Constructor->cur_user_is_owner($location->term_id))
			{
				$all_locations[]	= $location;
				$all_loc_types[]	= $Soling_Metagame_Constructor->get_location_type($location->term_id);
			}
		}
		
		//обрабатываем форму
		if ( wp_verify_nonce( $_POST['my_location_sh'], basename( __FILE__ ) ) )
		{		
			//echo 'Do something:<BR>'.count($all_locations);;
			foreach($all_locations as $location)
			{
				$id								= $location->term_id;
				if($_POST["update_location_".$id])
				{	
					
				}
			}
		}
		if($serv !="")	
		{
			$comm_visible	= "display";
		}
		
		//отрисовываем форму
		$html				.= "<div class='lp-comment_server' style='display:".$comm_visible.";'>".$serv."
								</div>";
		if(count($all_locations)==0)
		{
			$html			.= "<div class=smp-comment>".__("You haven't Locations now", "smc")."</div>";
		}
		else
		{
			$i=0;
			foreach($all_locations as $location)
			{
				$id			= $location->term_id;
				$loc_type	= $all_loc_types[$i];
				$title		= "<span style='opacity:0.5;'>" . $loc_type->picto. "</span> " .$location->name;
				$loc_type	= $all_loc_types[$ii];
				$loc_option	= SMC_Location::get_term_meta( $location->term_id );
				$slide		= '<h3><span style=\'font-weight:700; color:'.$user_iface_color."!important\'><span style='margin-right:2px!important;'>".$loc_type->picto . "</span> <span style='font-weight:normal; color:#AAA;margin-right:2px!important;'>" . $loc_type->post_title. "</span> ". $location->name."</a></h3>";
				
				$operative		= "<div class= 'my_location_contt'></div>";
				$loc_form		= get_location_($location, $loc_option, $loc_type);
				$tab_array		= array(
											apply_filters("smc_my_location_1", array("title" => '<i class="fa fa-cogs"></i>', 'hint'=>__("Settings", "smc"),	"slide" =>  $loc_form), $location)
										);
				$tab_array1		= apply_filters("add_location_tab", $tab_array, $location);
				if($tab_array1)	$tab_array = $tab_array1;
				$slide			.= count($tab_array)>1 ? Assistants::get_switcher($tab_array, 'locat') : $loc_form;
				
				$arr[]		= array(
					'title'	=> $title,
					'slide'	=> $slide
				);
				$i++;
			}
			$html			.= Assistants::get_lists($arr, '', '', 2);
			
		}
		$locs					= $UAM->get_uam_user_locations();
		if(count($locs))
			$html				.= "<h3>". __("I am member of premium Location", "smc")."</h3>";
			foreach($locs as $loc)
			{
				$location		= SMC_Location::get_instance($loc->id);
				$html			.= $location->name . "<BR>";
			}
		$html					.= "
		<!--script>
			jQuery(document).ready(function( $ )
			{	
				var customEvent = new CustomEvent('_my_locations_', {bubbles : true, cancelable : true})
				document.documentElement.dispatchEvent(customEvent);	
			});
		</script-->
		";
		$tabs = Assistants::get_switcher(
			array(
					array("title" => __("My Locations", "smc"),  "slide" => $html),
					array("title" => __("all Locations", "smc"), "slide" => all_locations_list())
				  ) ,'loc_page'
		);
		return $html;
	}
	function all_locations_list()
	{
		global $Soling_Metagame_Constructor;
		$locs		= $Soling_Metagame_Constructor->get_locations();
		$html		= "<h2>". __("All Locations", "smc")."</h2>";
		foreach($locs as $loc)
		{
			$html	.= $loc->name."<br>";
		}
		return $html;
	}
	function get_location_($location, $loc_option, $loc_type)
	{
		global $Soling_Metagame_Constructor;
		$html1			= "<div style='width:520px;'>
		<div class='width_450'>
			<h5>". __("Type of visibility", "smc"). "</h5>" ;
		
		$hiding_type		= (int)$loc_option['hiding_type'];
		$html1	.= "<div style=margin:15px 0;>";
		if($loc_type->use_by_player)
		{
			$html1			.= '
							<div style="margin:5px 0;">
								<div style="margin-top:2px;">
								'.$Soling_Metagame_Constructor->assistants->get_hint_helper(__("All user (Players and Spectrators) are behold all content. All Players can set posts and content.", "smc"), 120).'<input type="radio" name="hiding_type'. $location->term_id .'" id="is_locked1'. $location->term_id .'" value=0   class="css-checkbox"  '.checked( $hiding_type, 0, false ).' />
								<label for="is_locked1'. $location->term_id .'"  class="css-label2">'.__('Public Location', 'smc').'</label></div>
								<div style="margin-top:2px;">
								'.$Soling_Metagame_Constructor->assistants->get_hint_helper(__("All user (Players and Spectrators) are behold all content. Only privileged Insiders can create content.", "smc")).'<input type="radio" name="hiding_type'. $location->term_id .'" id="is_locked2'. $location->term_id .'" value=1   class="css-checkbox"  '.checked( $hiding_type, 1, false ).' />
								<label for="is_locked2'. $location->term_id .'"  class="css-label2">'.__('Lock this location', 'smc').'</label></div>
								<div style="margin-top:2px;">												
								'.$Soling_Metagame_Constructor->assistants->get_hint_helper(__("None but privileged Insiders can see and create content. ", "smc")).'<input type="radio" name="hiding_type'. $location->term_id .'" id="is_locked3'. $location->term_id .'" value=2   class="css-checkbox"  '.checked( $hiding_type, 2, false ).' />
								<label for="is_locked3'. $location->term_id .'"  class="css-label2">'.__('Hide this location','smc').'</label></div>
							</div>';
			
			$html1	.= "</div>";
		}
		else/**/
		{	
			switch($hiding_type)
			{
				case 0:
					$html1	.= '<div style="margin-top:2px;">' . $Soling_Metagame_Constructor->assistants->get_hint_helper(__("All user (Players and Spectrators) are behold all content. All Players can set posts and content.", "smc"), 120) . __('Public Location', 'smc'). '</div>';
					break;
				case 1:
					$html1	.= '<div style="margin-top:2px;">' . $Soling_Metagame_Constructor->assistants->get_hint_helper(__("All user (Players and Spectrators) are behold all content. Only privileged Insiders can create content.", "smc")).__('Lock this location', 'smc') ."</div>";
					break;
				case 2:
					$html1	.= '<div style="margin-top:2px;">' . $Soling_Metagame_Constructor->assistants->get_hint_helper(__("None but privileged Insiders can see and create content. ", "smc")). __('Hide this location', 'smc'). "</div>";
					break;
			}
			$html1	.= "</div>";
			$html1	.= "<div class='smp-comment'>" . __(" The type of visibility of this Location not be changed by  Player. Call Master ", "smc") . "</div>";
		}		
		
		$html1			.= "
			</div>
			<div class='width_450'>
				<h5>". __("Owners", "smc"). "</h5>
				<div style='display:inline-block;'>";
					
		for($i = 1; $i < 4; $i++)
		{
			
			$name		= "owner" . $i . '_' . $location->term_id;
			$data		= $loc_option ['owner'.$i];
			$html1		.= $data >= 1  ? 
			"<div style='margin-bottom:13px; padding-right:18px;display:inline-block;position:relative;'>".
				get_userdata($data)->display_name."
				<input type='hidden' name='$name' value='$data'>
			</div>" :
			"<label style='margin-bottom:13px;' class='smc_input'>".
			wp_dropdown_users(
				array(
						'show_option_none'  => "---",
						'echo'				=> false,
						'show'				=> 'display_name',
						'name'				=> $name,
						'class'				=> 'width_210',
					  )
			)."</label>";
			
		}		
		$html1			.= "
				</div>
			</div>
			<div class='width_450 '> ";
		
		if($hiding_type > 0)
		{
			$members	= explode(",", $loc_option['members']);
			$html1		.= "<div style='padding-top:7px;'>
				<h4>". __("Insiders", "smc"). "</h4>".
				$Soling_Metagame_Constructor->assistants->get_users_drop_list(
																				array(
																						"multiple"			=> true,
																						"selected"			=> $loc_option ['owner2'],
																						'name'				=> 'members_'.$location->term_id."[ ]",
																						'class'				=> 'chosen-select',
																						'selected'			=> $members,
																					  )
																			  ).
			"</div>";					
		}
		$html1			.= "
								</div>
								<div style='padding: 15px 0; position: relative; display: block; '>										
									<div class='smp-colorized smp-submit update_my_location' loc_id='".$location->term_id."'>".
										 __("Update").
									"</div>
								</div>
							</div>
							".
		'
		<script>
			show_my_locaion();
			jQuery(".chosen-container").css({width:"100%"});
			jQuery(".chosen-results .result-selected").css({color:"#000000!important"});
		</script>
		';
		return $html1;
	}
?>