<?php
	function my_log_page()
	{
		if(!current_user_can("manage_options"))	return "<div class='smc_comment'>".__("This content availabled only for Administrators.", "smc"). "</div>";
		$res		= Assistants::get_log();
		
		$html		.= "
		<style>
			body
			{
				overflow:auto!important;
			}
			td
			{
				color:#111;
			}
		</style>
		<div>
			<table id='smc-log'>
				<thead>
					<th>".__("Data", "smc"). "</th>
					<th>".__("Who", "smc"). "</th>
					<th>".__("time", "smc"). "</th>
				</thead>";
		foreach($res as $re)
		{
			$html	.= '<tr>'.
				"<td style='max-width:360px;'>" . $re->data . "</td>".
				"<td>" . $re->who . "</td>".
				"<td>" . $re->ts . "</td>";
			//Assistants::echo_me($re). 
			'</tr>';
		}
		$html		.= "</table>";
		if(current_user_can("administrator"))
		{
			$html	.= "
			<div style='width:100%;position:relative; display:inline-block;'>
				<div class='smp-colorized smp-submit lp-floater-simple' id='clear_log'>"  . __("Clear log", 'smc') . "</div>
				<div class='smp-colorized smp-submit lp-floater-simple' id='reload_log'>" . __("Reload log", 'smc') . "</div>
			</div>
		</div>";
		}
		echo $html;
	}
