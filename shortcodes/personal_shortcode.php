<?php
	function personal_page()
	{
		global $user_iface_color;			
		global $Soling_Metagame_Constructor;
		global $Direct_mesage_autor_types;
		global $Direct_message_avatars;
		
		
		$options	= get_option(SMC_ID);
		$farg		= array(
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'orderby'  		=> 'id',
									'order'     	=> 'DESC',
									'post_type' 	=> 'direct_message',
									'post_status' 	=> 'publish',	
									'meta_query' 	=> array(
																		array(
																				"key"		=> "enabled",
																				"value"		=> 1,
																				'compare'	=> '='		
																			),
																	),
								);
		$messages	= get_posts($farg);
		
		
		//================================
		//
		//	FROM SERVER
		//
		//================================
		$ar	 = '';
		if($_POST['change_mail_types'])
		{
			for($i=0; $i < count($Direct_message_avatars); $i++)
			{
				delete_user_meta(get_current_user_id(), "mail_level_".$i);
				//if(!update_user_meta(get_current_user_id(), "mail_level_".$i, $_POST['s_type_'.$i]=="on" ? 1 : 0 )) null;
					$ar		.= $i. ": ". $_POST['s_type_'.$i].' ';
			}
		}		
		
		//
		$autor_types		= '
		<div >
			<form name="avatar_types" method="post"  enctype="multipart/form-data" id="avatar_types">';
		for($i=0; $i < count($Direct_message_avatars); $i++)
		{
			$autor_types	.= '
			<div class="smc_author_type" style="min-width:19%;">
				<img src="'. $Direct_message_avatars[$i].'">
				<br>'.
				//(int)get_user_meta(get_current_user_id(), "mail_level_".$i).
				'<BR>
				<input type="checkbox" class="css-checkbox_black"  id="s_type_'.$i.'" name="s_type_'.$i.'" ' . checked(0, (int)get_user_meta(get_current_user_id(), "mail_level_".$i), false) . '/>
				<label for="s_type_'.$i.'"  class="css-label_black">'.$Direct_mesage_autor_types[$i].'</label> 
			</div>';
		}
		$autor_types		.= '
				<input type="submit" name="change_mail_types" value="'.__("Change", "smp").'"/>
			</form>
		</div>';
		//var_dump(get_term_link(3, "location"));
		//if(!$options['use_panel'])
		{
			$html				.= "			
			<table class='smc-login-table' border='0' callspan='0' callspacing='0' style=''> 
				<tr>
					<td class='smc-login-table'>
						<div class='smp-colorized smc-submit smc-alert' target_name='new_message_win'  id='new_mess_btn'>
							".__("add new Direct Message", "smc").
						"</div>
					</td>
					<td class='smc-login-table'>
						<div class='smp-colorized smc-submit smc-alert' target_name='master_dm_win'  id='to_master_btn'>
							".__("Call to Master", "smc"). 
						"</div>
					</td>
				</tr>
			</table>";
		}
		$html				.= "
			<div class='lp-hide' id='new_message_win'>".
				Direct_Message_Menager::add_new_dm_form().
			"</div>
			<div class='lp-hide' id='master_dm_win'>".
				Direct_Message_Menager::master_dm_form().
			"</div>";
						
		
		if(count($messages)==0)
		{
			$html			.= "<div class=smp-comment>".__("You haven't Direct Messages now", "smc")."</div>";
		}
		else
		{
			$html				.= "<div class='smp-production-container'>
				<div id='col1__' class='smp-cl1__' style='width: 100%;'>";
			$i=0;
			foreach($messages as $message)
			{
				$color			= get_post_meta($message->ID, "viewed", true) ? "#AAA" : "#".$user_iface_color;
				$html			.=  "<div class='smp-pr-title lp-personal-message-title lp_dm_title dm_list_btn' style='color: ".$color.";width:100%;' dm='".$message->ID."' id='production-btn-" . $message->ID . "' button_id=" . $i++ . ">" . $message->post_title . "</div>";
			}		
			$html				.= "
				</div>
				
			</div>
			";		
		}
		echo "<div id=smc_content>" . $html . "</div>";
	}
?>