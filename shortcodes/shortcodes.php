<?php
	
	require_once(SMC_REAL_PATH.'shortcodes/personal_shortcode.php');
	require_once(SMC_REAL_PATH.'shortcodes/my_locations_shortcode.php');
	require_once(SMC_REAL_PATH.'shortcodes/masters_shortcode.php');
	require_once(SMC_REAL_PATH.'shortcodes/my_logs.php');
	require_once(SMC_REAL_PATH.'shortcodes/location_display.php');
	function smc_add_shortcodes()
	{		
		add_shortcode('smc_personal_page', 	'personal_page'); 
		add_shortcode('smc_locations_page', 'locations_page'); 
		add_shortcode('smc_masters_page', 	'master_page'); 
		add_shortcode('my_log_page', 		'my_log_page'); 
		add_shortcode('location_display', 	'location_display'); 
	}
	add_action("init", "smc_add_shortcodes", 18);
?>