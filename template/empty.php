<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<?php wp_head(); ?>		
		<?php do_action("ermak_body_script"); ?>
	</head>
	<body id="<?php print get_stylesheet(); ?>" <?php body_class(); ?>>
		
		<?php echo apply_filters( 'ermak_body_before', "" ); ?>
	</body>
</html>