<?php
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	?>
	</pre>
	<div class="wrap sub-wrap">
		<h2><?php _e("Location types","smc");?></h2>
		<?php $project		= $this->options['project_name'];?>
		<?php
			$l_t			= get_posts(array('numberposts' => 500,'post_type'=>"location_type", 'orderby'=>"ID", 'order'=>'ASC'));
		?>
		
		<div>
			<div class='updated inpute-update' style='display:block;'>
				<h3><?php _e("Location settings","smc")?></h3>
				<div style="float:left;margin-right:2px;">
					<a href="/wp-admin/edit.php?post_type=location_type" class='button'><?php _e('Location types', "smc"); ?></a>
				</div>
				<div>
					<a href="/wp-admin/edit-tags.php?taxonomy=location" class='button'><?php _e('All Locations', "smc"); ?></a>
				</div>
			</div>
			
			<table class='wp-list-table widefat plugins' cellspacing='0'>
				<thead>
					<tr>
						<th scope='col' id='cb' class='manage-column column-cb check-column'  style="">
							<label class="screen-reader-text" for="cb-select-all-1">
								<?php _e("Select all"); ?>
							</label>
							<input id="cb-select-all-1" type="checkbox" />
						</th>
						<th scope='col' id='cb'  class="petit" style="width:70px">
							<?php _e("Use by player", "smc"); ?>
						</th>
						<th scope='col' id='name' class='manage-column column-name'  style="">
							<i class="fa fa-check-circle"></i> ��������� 
						</th>
						<th scope='col' id='description' class='manage-column column-description'  style="text-align:right; margin-right:0;">
							<!--p>
								<a href="/wp-admin/post-new.php?post_type=location_type" class="button" style="">����� ���</a>
							</p-->
						</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					global $post;
					foreach($l_t as  $post){?>
					<tr id='add-post-widget' class='active'>
						<th scope="row" class="check-column">
							<label class="screen-reader-text" for="cb-select-<?php the_ID(); ?>">�������</label>
							<input id="cb-select-<?php the_ID(); ?>" type="checkbox" name="post[]" value="<?php the_ID(); ?>" />
							<div class="locked-indicator">
							</div>
						</th>
						<th scope='row' class=''>
							<input id="demo_box_<?php the_ID(); ?>" class="css-checkbox" type="checkbox" <?php if(get_post_meta($post->ID, "use_by_player")[0]) {?>checked="checked"<?php };?>/>
							<label for="demo_box_<?php the_ID(); ?>" name="<?php the_ID(); ?>" class="css-label"> </label>
						</th>
						<td class='plugin-title'>
							<div>
								<div  style="display:none;">
									<span style="font-size:22px; margin-right:10px;">
										<?php print_r( get_post_meta($post->ID, "picto")[0] );?>
									</span> 
									<span>
										<b><?php the_title();?></b>
									</span>
									<span class="smc-comment">  | 
										<?php print_r( get_post_meta($post->ID, "slug")[0] );?>
									</span>
									<div class="row-actions visible">
										<span class='petit'>
											<a href="javascript:void(0)"; onclick="hidelt(<?php print_r ($post->ID); ?>);">
												<i class="fa fa-folder-open"></i> �������������
											</a> 
										</span>
									</div>
								</div>
								<div id="lt-show-<?php print_r ($post->ID); ?>" style="display:block; padding:0;margin:0;">
									<div style="float:left; width:62px;height:62px;border: 1px solid transparent!important;background:transparent;margin-right:3px;">
										<p style='font-size:38px; text-align:center; width:62px;'><?php print_r( get_post_meta($post->ID, "picto")[0] );?></p>
									</div>
									<div>
										<strong><input size="33" value='<?php the_title();?>' placeholder="������� ������� ��������"/></strong>
									</div>
									<div>
										<input class="smc-comment" size="37" value=' <?php print_r( get_post_meta($post->ID, "slug")[0] );?>' placeholder="�������� ������ ���������� �����������"/>
									</div>		
									<span class='petit'>
										<a href="#">
											<i class="fa fa-sun-o"></i> �����������
										</a> | 
									</span>
									<span class='petit'>
										<a href="#">
											<i class="fa fa-check-square"></i> <?php _e('update', "smc"); ?>
										</a> | 
									</span>
									<span class='petit'>
										<a href="javascript:void(0)" onclick="dellt('<?php print_r ($post->ID); ?>', '<?php _e("Are you really want to delete ", "smc").'<br>'.the_title().'"'; ?>');" class="edit">
											<i class="fa fa-times-circle"></i> <?php _e("Delete");?>
										</a>  
									</span>
									</span>
								
								</div>
								<div id="lt-edit-<?php print_r ($post->ID)?>" style="display:none;">
									<div style="float:left; width:62px;height:62px;border: 1px solid #aaa!important;background:#FFF;margin-right:3px;">
										<p style='font-size:38px; text-align:center; width:62px;'><?php print_r( get_post_meta($post->ID, "picto")[0] );?></p>
									</div>
									<div>
										<strong><input size="33" value='<?php the_title();?>' placeholder="������� ������� ��������"/></strong>
									</div>
									<div>
										<input class="smc-comment" size="37" value=' <?php print_r( get_post_meta($post->ID, "slug")[0] );?>' placeholder="�������� ������ ���������� �����������"/>
									</div>
									<span class='petit'>
										<a href="#">
											<i class="fa fa-check-square"></i> <?php _e('update', "smc"); ?>
										</a> | 
									</span>
									<span class='petit'>
										<a href="javascript:void(0)"; onclick="showlt(<?php print_r ($post->ID); ?>);">
											<i class="fa fa-folder-open"></i> <?php _e('cancel');?>
										</a> | 
									</span>
									<span class='petit'>
										<a href="#" class="edit">
											<i class="fa fa-times-circle"></i> <?php _e('delete');?>
										</a>  
									</span>
								</div>
							</div>
						</td>
						<td class='column-description desc'>
						</td>
					</tr>
					<tr>
						<td colspan='3' class='smc-red-marker clown' id="actions-<?php the_ID(); ?>" style="">
							<div>
								<p style="text-align:center;"><?php _e('delete');?>? <?php _e('close');?>?</p>
							</div>
						</td>
					</tr>
					<?php } ?>
					<tr id='add-post-widget-new' class='active update smc-red-marker'>
						<th scope="row" class="check-column">
							<label class="screen-reader-text" for="cb-select-<?php the_ID(); ?>">�������</label>
							<input id="cb-select-<?php the_ID(); ?>" type="checkbox" name="post[]" value="<?php the_ID(); ?>" />
							<div class="locked-indicator">
							</div>
						</th>
						<th scope='row' class=''>
							<input id="demo_box_new" class="css-checkbox" type="checkbox" />
							<label for="demo_box_new" name="<?php the_ID(); ?>" class="css-label"> </label>
						</th>
						<td class='plugin-title'>
							<div style="float:left; width:62px;height:62px;border: 1px solid #aaa!important;background:#FFF;margin-right:3px;">
								<p style=' font-family: "FontAwesome"; font-size:40px;padding-left:15px;'>?</p>
							</div>
							<div id="lt-show-new" style="display:block; backrgound:#000;">
								<div>
									<strong><input size="33" value='' placeholder="������� ������� ��������"/></strong>
								</div>
								<div>
									<input size="37" value='' placeholder="�������� ������ ���������� �����������"/>
								</div>
								<div class="row-actions visible">
									<span class=''>
										<a href="#" title="<?php _e("Add"); ?>">
											<i class="fa fa-plus-square"></i> <?php _e("Add"); ?>
										</a> 
									</span>
								</div>
							</div>
						</td>
						<td class='column-description desc'>
							
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th scope='col' id='cb' class='manage-column column-cb check-column'  style="">
							<label class="screen-reader-text" for="cb-select-all-2">
								<?php _e("Select all"); ?>
							</label>
							<input id="cb-select-all-2" type="checkbox" />
						</th>

						<th scope='col' id='cb'  class="petit" style="width:70px">
							<?php _e("Use by player", "smc"); ?>
						</th>
						<th scope='col' id='name' class='manage-column column-name'  style="">
							<?php _e("Title"); ?>
						</th>
						<th scope='col' id='description' class='manage-column column-description'  style="text-align:right; margin-right:0;">
							
						</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
	<pre>
	<?php

?>