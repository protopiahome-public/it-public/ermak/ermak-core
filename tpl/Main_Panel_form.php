<?php
	global $location_map_types, $SMC_Map;
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo "<script type='text/javascript' src='" . SMC_URLPATH . "js/simply_map_editor.js'></script>";
		/**/	
	if (isset($_POST['save'])) 
	{	
		$this->options['use_panel']					= isset($_POST['use_panel']) 				? 1 : 0;
		$this->options['panel_only_front']			= isset($_POST['panel_only_front']) 				? 1 : 0;
		$this->options['no_fixed_and_no_closed']	= 1;//isset($_POST['no_fixed_and_no_closed']) 	? 1 : 0;
		$this->options['use_location_menu']			= isset($_POST['use_location_menu']) 		? 1 : 0;
		$this->options['use_courusel']				= isset($_POST['use_courusel']) 			? 1 : 0;
		$this->options['account_panel']				= isset($_POST['account_panel']) 			? 1 : 0;
		$this->options['show_wheel_panel']			= isset($_POST['show_wheel_panel']) 		? 1 : 0;
		$this->options['main_map_name']				= stripslashes($_POST['main_map_name']);
		$this->options['mail_panel_cont']			= $_POST['mail_panel_cont'];
		$this->options['courusel_slides']			=(int) $_POST['courusel_slides'];
		$this->options['title_background']			= '#'.$_POST['title_background'];
		$this->options['main_map_color']			= '#'.$_POST['main_map_color'];
		$this->options['map_type']					= $_POST['map_type'];
		$this->options['contour_color']				= '#'.$_POST['contour_color'];
		$this->options['location_change_info']		= isset($_POST['location_change_info']) 	? 1 : 0;
		$this->options['design_show_children']		= isset($_POST['design_show_children']) 	? 1 : 0;
		$this->options['design_show_grandchildren']	= isset($_POST['design_show_grandchildren'])? 1 : 0;
		$this->options['enable_contour']			= isset($_POST['enable_contour']) 			? 1 : 0;
		$this->options['main_map_pic']				= stripslashes($_POST['main_map_picture_id']);
		$this->options['smc_height']				= $_POST['smc_height'];
		$this->options['exclude_cats']				= explode(',', $_POST['exclude_cats']);
		$this->options['nav_menu_height_klapan']	= $_POST['nav_menu_height_klapan']; // высота шапки тамы, которую можно спрятать под Панель
		$this->options['show_quick_menu']			= $_POST['quick_panel'] == 'on'; 
		$this->options['show_masters']				= $_POST['show_masters'] == "on"; 
		$this->options['show_news']					= $_POST['show_news'] == "on"; 
		$this->options['quick_menu_cat_id']			= $_POST['quick_menu_cat_id'];  
		$this->options['klapan_bckgrnd1']			= $_POST['klapan_bckgrnd1']; 
		if(!$this->options['public_content'])			$this->options['public_content'] = array();
		$this->options['public_content']['image']		= isset($_POST['pc_image']) 			? 1 : 0; 
		$this->options['public_content']['youtube']		= isset($_POST['pc_youtube']) 			? 1 : 0; 
		$this->options['public_content']['audio']		= isset($_POST['pc_audio']) 			? 1 : 0; 
		$this->options['public_content']['local_video']	= isset($_POST['pc_local_video'])		? 1 : 0; 
		$this->options['page_to_panel']				= isset($_POST['ptp']); 
		$this->options['display_update_period']		= $_POST['display_update_period']; 
		
		$this->options['design_lines']				= isset($_POST['design_lines']);  
		update_option(SMC_ID, $this->options);	
		echo '<div class="updated notice notice-success is-dismissible below-h2"><p><b>'.__('Settings saved.').'</b></p></div>';
		//echo $this->options['main_map_color'];
	}	
	// Внешний вид формы
	
	//echo '<div>'.$this->options['map_titles_coords'].'</div>';
	if($this->options['exclude_cats']!="")
		$exs		= implode(',', $this->options['exclude_cats']);

		
	?>
	
	
	
	<form method="post"  enctype="multipart/form-data" >
	<div class="wrap">
		<h2><?php _e("Main Metagame Panel","smc");?></h2>				
		<div class="sub-wrap">	
			<div class="submit">
					<input name="save" type="submit" class="button-primary" value="<?php _e('Save Draft'); ?>" />
			</div>
			<?php 
				$main		= '
				
				<div  class="smc-block smc-blue-marker" style="background:#f7fcfe;width: 95%; ">	
					<h3>'.__("Main Parameters",'smc').'</h3>
					<input name="use_panel" id="demo_box_1" class="css-checkbox" type="checkbox"  ' . checked($this->options["use_panel"], 1, 0) . ' />
					<label for="demo_box_1" name="demo_lbl_1" class="css-label">'.__("Use Panel","smc").'</label><BR>
					<input name="panel_only_front" id="panel_only_front" class="css-checkbox" type="checkbox"  ' . checked($this->options["panel_only_front"], 1, 0) . ' />
					<label for="panel_only_front" class="css-label">'.__("Use Panel only front page of site","smc").'</label><BR>'.						
					'<!--input name="no_fixed_and_no_closed" id="demo_box_101" class="css-checkbox" type="checkbox" '.checked($this->options["no_fixed_and_no_closed"], 1,0) .' />
					<label for="demo_box_101" name="demo_lbl_101" class="css-label">'.__("Is panel fixed by open and placed on top of content all posts","smc").'</label-->'.
					'<p style="margin-bottom:5px;"></p>
					<label for="mail_panel_cont"class="">'.__("SCC-selector of HTML-conteiner, that include Main Panel","smc").'</label><BR>
					<input name="mail_panel_cont" id="mail_panel_cont" type="text" value="'. $this->options['mail_panel_cont'] . '" style="width:100%; background:#EEE; font-weight:bold; font-size:20px;"/>						
					<div class="smc-description">' . __("If you want to make the panel is in header of the page, the field is blank. If the panel must be placed in any particular place template, select the desired CSS-selector of HTML container in the header which you want to see it. Attention! Make sure that the correct container is present in all the page templates or file header.php or file footer.php chosen WP-theme.", "smc") . '</div>
				
					<label for="slider0">'.__("Height of Main Panel", "smc").'</label>
					<input name="smc_height" id="slider0" value="'.$this->options["smc_height"].'" style="width:75%;"/>
					<p style="margin-bottom:5px;"></p>
					<label for="slider2">'.__("Overlay site header for ", "smc").'</label>
					<input name="nav_menu_height_klapan" id="slider2" value="'. $this->options["nav_menu_height_klapan"].'" style="width:75%;"/>
					<BR></div>';
				
				//Location menu
				$mm 		= isset($this->options['main_map_name']) ?  $this->options['main_map_name'] :  __("Main map", "smc");
				$lpto 		= '';
				for($i=0; $i < count($location_map_types); $i++)
					$lpto	.= '<option value=' . $i . ' ' . selected($this->options['map_type'], $i, 0).'>'.__($location_map_types[$i], "smc").'</option>';
				
				$loc_mrnu	= '						
					<div  class="smc-block smc-blue-marker" style="">						
						<h3>'.__("Location Menu","smc").'</h3>
						<input name="use_location_menu" id="demo_box_2" class="css-checkbox" type="checkbox"  '. checked($this->options['use_location_menu'], 1, 0) . ' />
						<label for="demo_box_2" class="css-label">' . __("Add Location Menu","smc") . '</label>
						<p style="margin-top:10px;"></p>
						<label for="demo_box_2" >'.__("Main map title","smc").'</label><BR>
						<input name="main_map_name" id="main_map_name" type="text" value="'.$mm.'" style="width:95%; background:#EEE; font-weight:bold; font-size:20px;"/>						
						<BR>
						<input name="title_background" id="title_background" value="' . $this->options['title_background'] . '" class="color rect_color"/> 
						<label for="title_background" style="vartical-align:middle;">'.__("Title background color", "smc").'</label>											
						<BR>
						<input class="color rect_color" name="main_map_color" id="main_map_color" value="' . $this->options['main_map_color'] . '"> 
						<label  style="vartical-align:middle;">' . __("Main map color","smc") . '</label>
						<BR>
						<div style="margin-top:10px;">
						<div style="margin:10px 0;">'.__("Klapan background", "smc") . '</div>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd11" type="radio" value="ametist_pattern1.jpg" '. checked("ametist_pattern1.jpg", $this->options['klapan_bckgrnd1'], 0) . ' 	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern1.jpg)!important"/>
						<!--input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd12" type="radio" value="ametist_pattern2.jpg" '. checked("ametist_pattern2.jpg", $this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern2.jpg)!important"/-->
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd13" type="radio" value="ametist_pattern3.jpg" '. checked("ametist_pattern3.jpg", $this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern3.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd14" type="radio" value="ametist_pattern4.jpg" '. checked("ametist_pattern4.jpg", $this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern4.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd15" type="radio" value="ametist_pattern5.jpg" '. checked("ametist_pattern5.jpg", $this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern5.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd16" type="radio" value="ametist_pattern6.jpg" '. checked("ametist_pattern6.jpg", $this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern6.jpg)!important"/>						
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd17" type="radio" value="pattern1.png" '. 		checked("pattern1.png", 		$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/pattern1.png)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd18" type="radio" value="pattern2.png" '. 		checked("pattern2.png", 		$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/pattern2.png)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd19" type="radio" value="pattern3.png" '. 		checked("pattern3.png", 		$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/pattern3.png)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd20" type="radio" value="pattern4.png" '. 		checked("pattern4.png", 		$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/pattern4.png)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd21" type="radio" value="pattern5.jpg" '. 		checked("pattern5.jpg", 		$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/pattern5.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd22" type="radio" value="pattern6.jpg" '. 		checked("pattern6.jpg", 		$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/pattern6.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd23" type="radio" value="ametist_pattern7.jpg" '.	checked("ametist_pattern7.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern7.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd24" type="radio" value="ametist_pattern8.jpg" '.	checked("ametist_pattern8.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern8.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd25" type="radio" value="ametist_pattern9.jpg" '.	checked("ametist_pattern9.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern9.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd26" type="radio" value="ametist_pattern10.jpg" '.checked("ametist_pattern10.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern10.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd27" type="radio" value="ametist_pattern11.jpg" '.checked("ametist_pattern11.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern11.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd28" type="radio" value="ametist_pattern12.jpg" '.checked("ametist_pattern12.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern12.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd29" type="radio" value="ametist_pattern13.jpg" '.checked("ametist_pattern13.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern13.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd30" type="radio" value="ametist_pattern14.jpg" '.checked("ametist_pattern14.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern14.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd31" type="radio" value="ametist_pattern15.jpg" '.checked("ametist_pattern15.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern15.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd32" type="radio" value="ametist_pattern16.jpg" '.checked("ametist_pattern16.jpg",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/ametist_pattern16.jpg)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd33" type="radio" value="tile1.png" '.checked("tile1.png",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/tile1.png)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd34" type="radio" value="tile2.png" '.checked("tile2.png",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/tile2.png)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd35" type="radio" value="tile3.png" '.checked("tile3.png",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/tile3.png)!important"/>
						<input class="bckgrnd_choose" name="klapan_bckgrnd1" id="klapan_bckgrnd36" type="radio" value="tile4.png" '.checked("tile4.png",	$this->options['klapan_bckgrnd1'], 0) . '	style="background-image:url(' . SMC_URLPATH . 'img/tile4.png)!important"/>
						</div>
						<p style="margin-top:10px;"></p>
						<label for="demo_box_3" name="demo_lbl_3" class="">'.__("Main map picture","smc").'</label><BR>
						<input name="main_map_picture_id" id="main_map_picture_id" type="text" value="'.(isset($this->options['main_map_pic']) ? $this->options['main_map_pic']:  "" ).'" style="width:95%; display:block;"/>						
						<a title="Задать миниатюру" href="/wp-admin/media-upload.php?tab=library&post_id=0&TB_iframe=true&width=640&height=408" id="set-post-thumbnail" class="thickbox">'.
							 wp_get_attachment_image( $this->options['main_map_pic'], array(160,160) ) .
						'</a>					
						<p></p>
						<label for="map_type" name="map_type_label" class="">'.__("Main map type","smc").'</label><BR>
						<div class="styled-select state rounded w400">
						<select  name="map_type" id="map_type" class="chosen-select1">'.
							$lpto .					
						'</select>
						</div>
						<hr>
						<div class="form-field" id="edit_map"  style="" src="' . SMC_URLPATH . 'svg-edit-2.6/svg-editor.html' . '">										
							<BR>' . 
							$this->get_map_editor() .							
						'</div>			
						<hr>						
						
					</div>';
					
					//carousel
					$caruosel_menu ='
					<div class="smc-block smc-blue-marker" style="">
						<h3>' . __("Video and comix carusel", "smc") . '</h3>	
						<input name="use_courusel" id="use_courusel" class="css-checkbox" type="checkbox" '. checked($this->options['use_courusel'], 1, 0) . ' />
						<label for="use_courusel" name="demo_lbl_3" class="css-label">'.__("Add Courusel","smc").'</label>
						
						<BR>	
						<div class="smc-description">'.
							__("Используйте конструкцию [smc-karusel height=300] для внедрения карусели в тело любого поста или страницы", "smc").
						'</div>
						<div>
							<label for="courusel_slides" value="">'.__("Count of slides", "smc").'</label><BR>
							<input name="courusel_slides" id="courusel_slides"  min="5" max="18" value="'.$this->options['courusel_slides'].'" style="width:75%;" />
							<p>
						</div>
						<div>
							<label for="exclude_cats" value="">'.__("Exclude Catigories", "smc").'</label>
							<p></p>
							<input type="text" name="exclude_cats" id="exclude_cats" value="'.$exs.'"/>
							<div class="smc-description">'.
								__("List ids of catigories. Delimiter - ',' (for example: 1,13,22).", "smc").
							'</div>
						</div>
						<hr>
						<div>
							<board_title>' . __("Types of media for user's messages", "smc") . '</board_title>
							<p>
							<input name="pc_image" id="pc_image" class="css-checkbox" type="checkbox" '. checked($this->options['public_content']['image'], 1, 0) . ' />
							<label for="pc_image" class="css-label">'.__("Images","smc").'</label>
							</p>
							<p>
							<input name="pc_youtube" id="pc_youtube" class="css-checkbox" type="checkbox" '. checked($this->options['public_content']['youtube'], 1, 0) . ' />
							<label for="pc_youtube" class="css-label">'.__("Youtube","smc").'</label>
							</p>
							<p>
							<input name="pc_audio" id="pc_audio" class="css-checkbox" type="checkbox" '. checked($this->options['public_content']['audio'], 1, 0) . ' />
							<label for="pc_audio" class="css-label">'.__("Audio","smc").'</label>
							</p>
							<!--p>
							<input name="pc_local_video" id="pc_local_video" class="css-checkbox" type="checkbox" '. checked($this->options['public_content']['local_video'], 1, 0) . ' />
							<label for="pc_local_video" class="css-label">'.__("Local video","smc").'</label>
							</p-->
						</div>
					</div>	
					';
					$account_panel	= '
						<div class="smc-block smc-blue-marker" style="">
						<h3>'.__("Account Panel", "smc").'</h3>	
						<input name="account_panel" id="demo_box_4" class="css-checkbox" type="checkbox" '.__($this->options['account_panel'], 1, 0).' />
						<label for="demo_box_4" name="demo_lbl_4" class="css-label">'.__("Enabled Account Panel","smc").'</label>
						<BR>	
						<div class="smc-description">'.
							__("Личный кабинет игрока на панели Локаций. Регистрация, Личные сообщение и тп.", "smc").
						'</div>
					</div>';
					$quick_panel	= '
						<div class="smc-block smc-blue-marker" style="">
							<h3>'.__("Quick Panel", "smc").'</h3>	
							<input name="quick_panel" id="demo_box_5" class="css-checkbox" type="checkbox" '. checked($this->options['show_quick_menu'], 1,0).'/>
							<label for="demo_box_5" class="css-label">'.__("Enabled Quick Panel","smc").'</label>
							<br>
							<div style="margin:10px 90px 10px 0; margin-left:40px; display:inline-block; position:relative;">
								<input name="show_masters" id="show_masters" class="css-checkbox" type="checkbox" '.checked(1, $this->options['show_masters'], 0).'/>
								<label for="show_masters" class="css-label">'. __("Enabled Masters Button","smc").'</label>
								<br>
								<input name="show_news" id="show_news" class="css-checkbox" type="checkbox" ' . checked(1, $this->options['show_news'], 0) . '/>
								<label for="show_news" class="css-label">' . __("Enabled News Button","smc") . '</label>
								<br>
								<label>' . __("Category as News","smc") . '</label><br>
								<div class="styled-select state rounded w400">'.
								wp_dropdown_categories('show_count=1&echo=0&hierarchical=1&name=quick_menu_cat_id&selected=' . $this->options['quick_menu_cat_id'] . '&class=chosen-select_').
							'	</div>
							</div>
							<BR>	
							<div class="smc-description">'.
								__("Fixes quick panel in left edge of page", "smc").
							'</div>					
						</div>'	;
					$weel_panel	='
						<div class="smc-block smc-blue-marker" style="">
							<h3>'.__("Wheel Panel", "smc").'</h3>	
							<input name="show_wheel_panel" id="show_wheel_panel" class="css-checkbox" type="checkbox" '. checked($this->options['show_wheel_panel'], 1, 0).'/>
							<label for="show_wheel_panel" class="css-label">'.__("Enabled Wheel Panel","smc").'</label>					
							<BR>	
							<div class="smc-description">'.
								__("Show button in the bottom of Main panel what opened the wheel panel. Wheel panel give a quick path to Specific statictics of the Game.", "smc").
							'</div>							
						</div>';
					/*
					
					
					
					
					<p></p>
					
				';
				*/
					$location_display = '<div class="smc-block smc-blue-marker" style="">
							<h3>'.__("Location Display", "smc").'</h3>	
								<input  type="number" step="1" id="display_update_period" name="display_update_period" value="'.$this->options['display_update_period'].'"/> mscs		
							<BR>	
							<div class="smc-description">'.
								__("Period updated for Location Display", "smc").
							'</div>							
						</div>';
				echo Assistants::get_switcher( 
												array(
														array("title" => __('Settings'), 						"slide" => $main ),
														array("title" => __("Location Menu","smc"), 			"slide" => $loc_mrnu,	'exec'=>'set_map',	'args'=>"ggg"),
														array("title" => __("Media carusel", "smc"), 			"slide" => $caruosel_menu ),														
														//array("title" => __("Account Panel", "smc"), 			"slide" => $account_panel ),														
														array("title" => __("Quick Panel", "smc"), 				"slide" => $quick_panel, "exec"=>"rewidth_select", "args"=>"" ),													
														//array("title" => __("Wheel Panel", "smc"), 				"slide" => $weel_panel ),														
														array("title" => __("Location Display", "smc"), 		"slide" => $location_display ),														
													 )
											 );
			?>
					
			<div class="submit">
				<input name="save" type="submit" class="button-primary" value="<?php _e('Save Draft'); ?>" />
			</div>
			<div>
				<?php //echo Assistants::echo_me( $this->options, true ); ?>
			</div>
			<div>
				<?php //echo Assistants::echo_me( get_option("smc_map_data"), true); ?>
			</div>
		</div>
		<div id="modal-fon" style="position:fixed; top:0; left:-80px; width:100%; height:100%; background:rgba(0,0,0,0.75); display:none;">
		</div>
		<div id="modal-window" style="position:fixed; 
					top:40px; 
					left:170px; 
					width:1200px; 
					height:600px; 
					background:#EEE; 
					border:1px solid #000;
					-webkit-box-shadow: 8px 8px 4px 4px rgba(0,0,0,0.5);
					box-shadow: 8px 8px 4px 4px rgba(0,0,0,0.5);
					display:none;">
			<div style="padding:10px;">
				<?php include SMC_REAL_PATH.'tpl/map-edit.php' ; ?>
			</div>
		</div>
		<div id="waiting-fon" style="position:fixed; z-index:10000; top:0; left:0; background:rgba(0,0,0,0.25); width:200%; height:100%;display:none;">
			<div style="position:absolute; top:45%; left:45%; text-align:center; ">
				<i class="fa fa-cog fa-spin fa-5x"></i><br>
				<?php _e("Please wait"); ?>
			</div>
		</div>
	</div>
	</form>
	<?php 
		$dd1				= '<?xml version="1.0" encoding="UTF-8"?><svg width="1000" height="400" xmlns="http://www.w3.org/2000/svg"><!-- Created with SVG-edit - http://svg-edit.googlecode.com/ --><g><title>Layer 1</title></g></svg>';
		$dd					= str_replace(array("\r", "\n"), array("", ""), $SMC_Map->get_map_data());
		if($dd == "" ) $dd = $dd1;
		$args				= array(
										'number' 		=> 0
										,'offset' 		=> 0
										,'orderby' 		=> 'id'
										,'order' 		=> 'ASC'
										,'hide_empty' 	=> false
										,'fields' 		=> 'all'
										,'slug' 		=> ''
										,'hierarchical' => true
										,'name__like' 	=> ''
										,'pad_counts' 	=> false
										,'get' 			=> ''
										,'child_of' 	=> 0
										,'parent' 		=> 0
									);
						
		$childLocs			= get_terms('location', $args);
		$chldren_id			= array();
		$chldren_name		= array();
		$chldren_id[]		= "L0";			
		$chldren_name[]		= "---";	
		foreach($childLocs	as $ch)
		{
			$chldren_id[]	= "L".$ch->term_id;			
			$chldren_name[]	= $ch->name;			
		}
		$ids				= json_encode($chldren_id);	
		$names				= json_encode($chldren_name);	
	?>
	<script>
		//http://ionden.com/a/plugins/ion.rangeSlider/index.html
		//console.log('map = <?php echo $dd ?>');
		set_range("#slider0", {min:250, max:800, postfix:"<?php _e(" px", "smc"); ?>"});
		set_range("#slider2", {min:0, max:500, postfix:"<?php _e(" px", "smc"); ?>"});
		set_range("#courusel_slides", {min:2, max:15, postfix:"<?php _e(" slides", "smc"); ?>"});
		set_chosen(".chosen-select", {max_selected_options: 5});
		jQuery(function($)
		{
			$("#waiting-fon").width(document.body.clientWidth);
			$("#waiting-fon").height(document.body.clientHeight);
			$( "#map_type" ).change(function() {					
				if($(this).val() ==1)
					$("#edit_map").show("fast");
				else
					$("#edit_map").hide("fast");
			});
			
		});
		
		var set_map		= function()
		{
			jQuery(function($)
			{
				$("svg").attr("id", "smc_location_map");
				$("#smc_location_map").append("<defs></defs>");
				var bbox = $("#smc_location_map")[0].getBBox();
				var bw	= bbox.width < 1100 && bbox.width > 500 ? bbox.width : 1100;
				var bh	= bbox.height <1100 && bbox.height> 300 ? bbox.height: 400;
				
				$("#smc_location_map").attr("width", bw);
				$("#smc_location_map").attr("height", bh);
				$(".geom-cont").width(bw);
				$(".geom-cont").height(bh);
					
			});			
		}
		window.saveSvg  = function (datas)
		{
			//console.log(datas);
			send(["save_main_map_data", datas]);
		}
		
		window.map_data 		= "map_data";
		var xml 				= '<?php echo $dd ?>';
		//var $map_data = StringtoXML(xml);
		window.map_data 		= xml;
		window.list_ids			= JSON.parse('<?php echo $ids?>');
		window.list_names		= JSON.parse('<?php echo $names?>');
		//alert(window.map_data);
		
		jQuery(document).ready(function($)
		{
			$("#modal-fon").appendTo("body");
			
			//$("modal-fon").changeZIndex('auto');
			$("#modal-window").appendTo("body");
			//$("modal-window").changeZIndex('auto');
			$("#waiting-fon").appendTo(document.body);
			$("#modal-window").css('z-index', '1000000000');
			
		});
	</script>
	
	<?php
?>