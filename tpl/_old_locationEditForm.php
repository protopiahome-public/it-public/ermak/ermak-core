<script>
		jQuery(document).ready(function($)
		{
			$("#uam-button").click(function()
			{				
				$("#location-uam").toggle();
			});
		});
	</script>
<?php
/**

 */
 
global $oUserAccessManager;
$aUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();

$iObjectId = null;
$sObjectType = 'location';

if (isset($_GET['tag_ID'])) {
    $iObjectId = $_GET['tag_ID'];
    
    $aUserGroupsForObject = $oUserAccessManager->getAccessHandler()->getUserGroupsForObject(
        $sObjectType,
        $iObjectId
    );
} else {
    $aUserGroupsForObject = array();
}
    
?>
<table class="form-table">
	<tbody>
		<tr>
			<th>
				<span id="uam-button" class="button"><?php _e("Set up user groups", 'smc'); ?></span>
			</th>
			<td id="location-uam" style="display:none;">
<?php

if (count($aUamUserGroups) > 0) {
	include UAM_REALPATH.'tpl/groupSelectionForm.php';
} elseif ($oUserAccessManager->getAccessHandler()->checkUserAccess()) {
    ?>
	<a href='admin.php?page=uam_usergroup'><?php _e("Set up user groups", 'smc');?></a>
	<?php
} else {
    echo TXT_UAM_NO_GROUP_AVAILABLE;
}
?>
			</td>
		</tr>
	</tbody>
</table>
<?php
if (isset($_GET['action'])) {
    $sAction = $_GET['action'];
} else {
    $sAction = null;
}

if ($sAction != 'edit') {
	?>
	<style type="text/css">
        .submit {
        	display: none;
        	position: relative;
        }
    </style>
	
    <p class="submit" style="display: block; position: relative;">
    	<input class="button" type="submit" value="Add New Category" name="submit" />
	</p>
	<?php
}