	
	<script type="text/javascript" src="<?php echo SMC_URLPATH ?>js/jssor/jssor.core.js"></script>
	<script type="text/javascript" src="<?php echo SMC_URLPATH ?>js/jssor/jssor.utils.js"></script> 
	<script type="text/javascript" src="<?php echo SMC_URLPATH ?>js/jssor/jssor.slider.js"></script>
	<script>
        jQuery(document).ready(function ($) {
			
            var options = {
                $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $DirectionNavigatorOptions: {
                    $Class: $JssorDirectionNavigator$,              //[Requried] Class to create direction navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
			
			$("li").live(
			{
				'click':function(event)
				{
					//alert('<i class="' + $(this).children().attr("class") + '"></i>');
					$("#picto1").html('<i class="' + $(this).children().attr("class") + '"></i>');
					$("#picto").val('<i class="' + $(this).children().attr("class") + '"></i>');
					$("#font-awesome-list").hide('fast');
				},
				'mouseover': function(event)
				{
					//alert($(this).position().left);
					var pos = $(this).position();
					var t	= pos.top  < 100 ? (pos.top  + 36) : pos.top  - 63;
					var l	= pos.left < 150 ? (pos.left + 38) : pos.left - 63;
					$(".layer_1").html('<div class="modal" style="left:' + l + 'px; top:' + t + 'px; "><i class="' + $(this).children().attr("class") + '"></i><div>');
					
				},
				'mouseout':function(event)
				{
					$(".layer_1").html('');
				}
			});
			$("#picto1").click(function(e){
				$("#font-awesome-list").animate({left: '100', top:100}, 50);
				$("#font-awesome-list").toggle('fast');
			});
			
			$('#font-awesome-list').bind('clickoutside', function (event) {
				$(this).hide();
			});
		})
    </script>
	<style>
		#font-awesome-list
		{
			display:none;
			position:absolute;
			top:0;
		}
		ul.font-group
		{
			display:inline-block;
			width:270px;
			margin-left:35px;
		}
		.font-group li
		{
			font-size:13px;
			list-style-type:none;
			padding:4px;			
			float:left;
			margin:1px;
			width:17px;
			height:16px;
			border:1px #DDD solid;
			background:#EEE;
			text-align:center;
			cursor:pointer
		}
		.font-group li:hover
		{
			background:red;
			color:white;
		}
		.font-group li:active
		{
			background:black;
		}
		.font-groupw
		{
			-moz-column-count: 25;
			-moz-column-gap: 10px;
			-webkit-column-count: 25;
			-webkit-column-gap: 10px;
			column-count: 25;
			column-gap: 10px;
		}
		.layer_1
		{
			position:absolute;
			top:0;
			left:0;
			
		}
		.modal
		{
			position:absolute;
			font-size:50px;
			text-align:center;
			background:#AAA;
			padding-top:10px;
			width:70px;
			height:60px;
		}
		.picto-slide
		{
			background:#666;
		}
		
		.jssord03l, .jssord03r, .jssord03ldn, .jssord03rdn
		{
			position: absolute;
			cursor: pointer;
			display: block;
			background: url(<?php echo SMC_URLPATH ?>img/d01.png) no-repeat;
			overflow:hidden;
			left:0!important;
		}
		.jssord03l { background-position: -3px -33px; }
		.jssord03r { background-position: -183px -33px; }
		.jssord03l:hover { background-position: -123px -33px; }
		.jssord03r:hover { background-position: -183px -33px; }
		.jssord03ldn { background-position: -243px -33px; }
		.jssord03rdn { background-position: -303px -33px; }
	</style>
	
	<div id="font-awesome-list">
		<div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 350px; height: 280px;">
			<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 350px; height: 280px; overflow: hidden;">
				<div class='picto-slide'>
					<ul class="font-group">
						<li><i class="fa fa-glass"></i> </li>
						<li><i class="fa fa-music"></i></li>
						<li><i class="fa fa-search"></i></li>
						<li><i class="fa fa-envelope-o"></i></li>
						<li><i class="fa fa-heart"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star-o"></i></li>
						<li><i class="fa fa-user"></i></li>
						<li><i class="fa fa-film"></i> </li>
						<li><i class="fa fa-th-large"></i></li>
						<li><i class="fa fa-th"></i></li>
						<li><i class="fa fa-th-list"></i></li>
						<li><i class="fa fa-check"></i></li>
						<li><i class="fa fa-times"></i></li>
						<li><i class="fa fa-search-plus"></i></li>
						<li><i class="fa fa-search-minus"></i></li>
						<li><i class="fa fa-power-off"></i></li>
						<li><i class="fa fa-signal"></i></li>
						<li><i class="fa fa-cog"></i></li>
						<li><i class="fa fa-trash-o"></i></li>
						<li><i class="fa fa-home"></i></li>
						<li><i class="fa fa-file-o"></i></li>
						<li><i class="fa fa-clock-o"></i></li>
						<li><i class="fa fa-road"></i></li>
						<li><i class="fa fa-download"></i></li>
						<li><i class="fa fa-arrow-circle-o-down"></i></li>
						<li><i class="fa fa-arrow-circle-o-up"></i></li>
						<li><i class="fa fa-inbox"></i></li>
						<li><i class="fa fa-play-circle-o"></i></li>
						<li><i class="fa fa-repeat"></i></li>
						<li><i class="fa fa-refresh"></i></li>
						<li><i class="fa fa-list-alt"></i></li>
						<li><i class="fa fa-lock"></i></li>
						<li><i class="fa fa-flag"></i></li>
						<li><i class="fa fa-headphones"></i></li>
						<li><i class="fa fa-volume-off"></i></li>
						<li><i class="fa fa-volume-down"></i></li>
						<li><i class="fa fa-volume-up"></i></li>
						<li><i class="fa fa-qrcode"></i></li>
						<li><i class="fa fa-barcode"></i></li>
						<li><i class="fa fa-tag"></i></li>
						<li><i class="fa fa-tags"></i></li>
						<li><i class="fa fa-book"></i></li>
						<li><i class="fa fa-bookmark"></i></li>
						<li><i class="fa fa-print"></i></li>
						<li><i class="fa fa-camera"></i></li>
						<li><i class="fa fa-font"></i></li>
						<li><i class="fa fa-bold"></i></li>
						<li><i class="fa fa-italic"></i></li>
						<li><i class="fa fa-text-height"></i></li>
						<li><i class="fa fa-text-width"></i></li>
						<li><i class="fa fa-align-left"></i></li>
						<li><i class="fa fa-align-center"></i></li>
						<li><i class="fa fa-align-right"></i></li>
						<li><i class="fa fa-align-justify"></i></li>
						<li><i class="fa fa-list"></i></li>
						<li><i class="fa fa-outdent"></i> 
						<li><i class="fa fa-indent"></i>
						<li><i class="fa fa-video-camera"></i> 
						<li><i class="fa fa-picture-o"></i> 
						<li><i class="fa fa-pencil"></i> 
						<li><i class="fa fa-map-marker"></i> 
						<li><i class="fa fa-adjust"></i> 
						<li><i class="fa fa-tint"></i>
						<li><i class="fa fa-pencil-square-o"></i> 
						<li><i class="fa fa-share-square-o"></i> 
						<li><i class="fa fa-check-square-o"></i> 
						<li><i class="fa fa-arrows"></i>
						<li><i class="fa fa-step-backward"></i> 
						<li><i class="fa fa-fast-backward"></i>
						<li><i class="fa fa-backward"></i> 
						<li><i class="fa fa-play"></i> 
						<li><i class="fa fa-pause"></i>
						<li><i class="fa fa-stop"></i> 
						<li><i class="fa fa-forward"></i> 
						<li><i class="fa fa-fast-forward"></i> 
						<li><i class="fa fa-step-forward"></i>
						<li><i class="fa fa-eject"></i>
						<li><i class="fa fa-chevron-left"></i>
						<li><i class="fa fa-chevron-right"></i></li>
						<li><i class="fa fa-plus-circle"></i>
					</ul>	
				</div>
				<div class='picto-slide'>
					<ul class="font-group">	
						<li><i class="fa fa-minus-circle"></i>
						<li><i class="fa fa-times-circle"></i>
						<li><i class="fa fa-check-circle"></i> 
						<li><i class="fa fa-question-circle"></i>
						<li><i class="fa fa-info-circle"></i>
						<li><i class="fa fa-crosshairs"></i>
						<li><i class="fa fa-times-circle-o"></i> 
						<li><i class="fa fa-check-circle-o"></i>
						<li><i class="fa fa-ban"></i>
						<li><i class="fa fa-arrow-left"></i> 
						<li><i class="fa fa-arrow-right"></i> 
						<li><i class="fa fa-arrow-up"></i>
						<li><i class="fa fa-arrow-down"></i>
						<li><i class="fa fa-share"></i>
						<li><i class="fa fa-expand"></i>
						<li><i class="fa fa-compress"></i>
						<li><i class="fa fa-plus"></i>
						<li><i class="fa fa-minus"></i>
						<li><i class="fa fa-asterisk"></i>
						<li><i class="fa fa-exclamation-circle"></i>
						<li><i class="fa fa-gift"></i> 
						<li><i class="fa fa-leaf"></i> 
						<li><i class="fa fa-fire"></i> 
						<li><i class="fa fa-eye"></i> 
						<li><i class="fa fa-eye-slash"></i>
						<li><i class="fa fa-exclamation-triangle"></i> 
						<li><i class="fa fa-plane"></i> 
						<li><i class="fa fa-calendar"></i> 
						<li><i class="fa fa-random"></i> 
						<li><i class="fa fa-comment"></i> 
						<li><i class="fa fa-magnet"></i> 
						<li><i class="fa fa-chevron-up"></i>
						<li><i class="fa fa-chevron-down"></i> 
						<li><i class="fa fa-retweet"></i>
						<li><i class="fa fa-shopping-cart"></i> 
						<li><i class="fa fa-folder"></i>
						<li><i class="fa fa-folder-open"></i> 
						<li><i class="fa fa-arrows-v"></i> 
						<li><i class="fa fa-arrows-h"></i> 
						<li><i class="fa fa-bar-chart-o"></i> 
						<li><i class="fa fa-twitter-square"></i>
						<li><i class="fa fa-facebook-square"></i>
						<li><i class="fa fa-camera-retro"></i> 
						<li><i class="fa fa-key"></i> 
						<li><i class="fa fa-cogs"></i> 
						<li><i class="fa fa-comments"></i> 
						<li><i class="fa fa-thumbs-o-up"></i> 
						<li><i class="fa fa-thumbs-o-down"></i> 
						<li><i class="fa fa-star-half"></i> 
						<li><i class="fa fa-heart-o"></i> 
						<li><i class="fa fa-sign-out"></i> 
						<li><i class="fa fa-linkedin-square"></i> 
						<li><i class="fa fa-thumb-tack"></i> 
						<li><i class="fa fa-external-link"></i> 
						<li><i class="fa fa-sign-in"></i> 
						<li><i class="fa fa-trophy"></i> 
						<li><i class="fa fa-github-square"></i> 
						<li><i class="fa fa-upload"></i> 
						<li><i class="fa fa-lemon-o"></i> 
						<li><i class="fa fa-phone"></i> 
						<li><i class="fa fa-square-o"></i> 
						<li><i class="fa fa-bookmark-o"></i> 
						<li><i class="fa fa-phone-square"></i>
						<li><i class="fa fa-twitter"></i> 
						<li><i class="fa fa-facebook"></i> 
						<li><i class="fa fa-github"></i> 
						<li><i class="fa fa-unlock"></i> 
						<li><i class="fa fa-credit-card"></i>
						<li><i class="fa fa-rss"></i> 
						<li><i class="fa fa-hdd-o"></i> 
						<li><i class="fa fa-bullhorn"></i>
						<li><i class="fa fa-bell"></i>
						<li><i class="fa fa-certificate"></i> 
						<li><i class="fa fa-hand-o-right"></i> 
						<li><i class="fa fa-hand-o-left"></i> 
						<li><i class="fa fa-hand-o-up"></i> 
						<li><i class="fa fa-hand-o-down"></i> 
						<li><i class="fa fa-arrow-circle-left"></i> 
						<li><i class="fa fa-arrow-circle-right"></i>
						<li><i class="fa fa-arrow-circle-up"></i> 
						<li><i class="fa fa-arrow-circle-down"></i>
					</ul>	
				</div>
				<div class='picto-slide'>
					<ul class="font-group"> 
						<li><i class="fa fa-globe"></i> 
						<li><i class="fa fa-wrench"></i>
						<li><i class="fa fa-tasks"></i> 
						<li><i class="fa fa-filter"></i> 
						<li><i class="fa fa-briefcase"></i> 
						<li><i class="fa fa-arrows-alt"></i> 
						<li><i class="fa fa-users"></i> 
						<li><i class="fa fa-link"></i>  
						<li><i class="fa fa-cloud"></i> 
						<li><i class="fa fa-flask"></i> 
						<li><i class="fa fa-scissors"></i>
						<li><i class="fa fa-files-o"></i> 
						<li><i class="fa fa-paperclip"></i>
						<li><i class="fa fa-floppy-o"></i>
						<li><i class="fa fa-square"></i> 
						<li><i class="fa fa-bars"></i> 
						<li><i class="fa fa-list-ul"></i> 
						<li><i class="fa fa-list-ol"></i> 
						<li><i class="fa fa-strikethrough"></i>
						<li><i class="fa fa-underline"></i> 
						<li><i class="fa fa-table"></i>
						<li><i class="fa fa-magic"></i> 
						<li><i class="fa fa-truck"></i>
						<li><i class="fa fa-pinterest-square"></i>
						<li><i class="fa fa-money"></i>
						<li><i class="fa fa-caret-down"></i>
						<li><i class="fa fa-caret-up"></i>
						<li><i class="fa fa-caret-left"></i>
						<li><i class="fa fa-caret-right"></i>
						<li><i class="fa fa-columns"></i>
						<li><i class="fa fa-sort"></i>
						<li><i class="fa fa-sort-asc"></i>
						<li><i class="fa fa-sort-desc"></i>
						<li><i class="fa fa-envelope"></i>
						<li><i class="fa fa-linkedin"></i> 
						<li><i class="fa fa-undo"></i>
						<li><i class="fa fa-gavel"></i>
						<li><i class="fa fa-tachometer"></i>
						<li><i class="fa fa-comment-o"></i> 
						<li><i class="fa fa-comments-o"></i> 
						<li><i class="fa fa-bolt"></i>
						<li><i class="fa fa-sitemap"></i>
						<li><i class="fa fa-umbrella"></i>
						<li><i class="fa fa-clipboard"></i>
						<li><i class="fa fa-lightbulb-o"></i>
						<li><i class="fa fa-exchange"></i>
						<li><i class="fa fa-cloud-download"></i>
						<li><i class="fa fa-cloud-upload"></i>
						<li><i class="fa fa-user-md"></i>
						<li><i class="fa fa-stethoscope"></i>
						<li><i class="fa fa-suitcase"></i>
						<li><i class="fa fa-bell-o"></i>
						<li><i class="fa fa-coffee"></i>
						<li><i class="fa fa-cutlery"></i>
						<li><i class="fa fa-file-text-o"></i>
						<li><i class="fa fa-building-o"></i>
						<li><i class="fa fa-hospital-o"></i>
						<li><i class="fa fa-ambulance"></i>
						<li><i class="fa fa-medkit"></i>
						<li><i class="fa fa-fighter-jet"></i>
						<li><i class="fa fa-beer"></i>
						<li><i class="fa fa-h-square"></i>
						<li><i class="fa fa-plus-square"></i>
						<li><i class="fa fa-angle-double-left"></i>
						<li><i class="fa fa-angle-double-right"></i>
						<li><i class="fa fa-angle-double-up"></i>
						<li><i class="fa fa-angle-double-down"></i>
						<li><i class="fa fa-angle-left"></i>
						<li><i class="fa fa-angle-right"></i>
						<li><i class="fa fa-angle-up"></i>
						<li><i class="fa fa-angle-down"></i>
						<li><i class="fa fa-desktop"></i>
						<li><i class="fa fa-laptop"></i>
						<li><i class="fa fa-tablet"></i>
						<li><i class="fa fa-mobile"></i>
						<li><i class="fa fa-circle-o"></i>
						<li><i class="fa fa-quote-left"></i>
						<li><i class="fa fa-quote-right"></i>
						<li><i class="fa fa-spinner"></i>
						<li><i class="fa fa-circle"></i>
						<li><i class="fa fa-reply"></i>
					</ul>	
				</div>
				<div class='picto-slide'>	
					<ul class="font-group">	
						<li><i class="fa fa-github-alt"></i>
						<li><i class="fa fa-folder-o"></i>
						<li><i class="fa fa-folder-open-o"></i>
						<li><i class="fa fa-smile-o"></i>
						<li><i class="fa fa-frown-o"></i>
						<li><i class="fa fa-meh-o"></i>
						<li><i class="fa fa-gamepad"></i>
						<li><i class="fa fa-keyboard-o"></i>
						<li><i class="fa fa-flag-o"></i>
						<li><i class="fa fa-flag-checkered"></i> 
						<li><i class="fa fa-terminal"></i> 
						<li><i class="fa fa-code"></i>
						<li><i class="fa fa-reply-all"></i>
						<li><i class="fa fa-mail-reply-all"></i>
						<li><i class="fa fa-star-half-o"></i>
						<li><i class="fa fa-location-arrow"></i>
						<li><i class="fa fa-crop"></i>
						<li><i class="fa fa-code-fork"></i>
						<li><i class="fa fa-chain-broken"></i>
						<li><i class="fa fa-question"></i>
						<li><i class="fa fa-info"></i>
						<li><i class="fa fa-exclamation"></i>
						<li><i class="fa fa-superscript"></i>
						<li><i class="fa fa-subscript"></i>
						<li><i class="fa fa-eraser"></i>
						<li><i class="fa fa-puzzle-piece"></i>
						<li><i class="fa fa-microphone"></i>
						<li><i class="fa fa-microphone-slash"></i>
						<li><i class="fa fa-shield"></i> 
						<li><i class="fa fa-calendar-o"></i> 
						<li><i class="fa fa-fire-extinguisher"></i>
						<li><i class="fa fa-rocket"></i>
						<li><i class="fa fa-maxcdn"></i>
						<li><i class="fa fa-chevron-circle-left"></i>
						<li><i class="fa fa-chevron-circle-right"></i>
						<li><i class="fa fa-chevron-circle-up"></i>
						<li><i class="fa fa-chevron-circle-down"></i>
						<li><i class="fa fa-html5"></i>
						<li><i class="fa fa-css3"></i> 
						<li><i class="fa fa-anchor"></i> 
						<li><i class="fa fa-unlock-alt"></i>
						<li><i class="fa fa-bullseye"></i>
						<li><i class="fa fa-ellipsis-h"></i>
						<li><i class="fa fa-ellipsis-v"></i>
						<li><i class="fa fa-rss-square"></i>
						<li><i class="fa fa-play-circle"></i>
						<li><i class="fa fa-ticket"></i>
						<li><i class="fa fa-minus-square"></i>
						<li><i class="fa fa-minus-square-o"></i>
						<li><i class="fa fa-level-up"></i>
						<li><i class="fa fa-level-down"></i>
						<li><i class="fa fa-check-square"></i>
						<li><i class="fa fa-pencil-square"></i>
						<li><i class="fa fa-external-link-square"></i>
						<li><i class="fa fa-share-square"></i>
						<li><i class="fa fa-compass"></i> 
						<li><i class="fa fa-caret-square-o-down"></i>
						<li><i class="fa fa-caret-square-o-up"></i>
						<li><i class="fa fa-caret-square-o-right"></i> 
						<li><i class="fa fa-eur"></i>
						<li><i class="fa fa-gbp"></i>
						<li><i class="fa fa-usd"></i>
						<li><i class="fa fa-inr"></i>
						<li><i class="fa fa-jpy"></i>
						<li><i class="fa fa-rub"></i>
						<li><i class="fa fa-krw"></i>
						<li><i class="fa fa-btc"></i>
						<li><i class="fa fa-file"></i>
						<li><i class="fa fa-file-text"></i>
						<li><i class="fa fa-sort-alpha-asc"></i>
						<li><i class="fa fa-sort-alpha-desc"></i>
						<li><i class="fa fa-sort-amount-asc"></i>
						<li><i class="fa fa-sort-amount-desc"></i>
						<li><i class="fa fa-sort-numeric-asc"></i>
						<li><i class="fa fa-sort-numeric-desc"></i>
						<li><i class="fa fa-thumbs-up"></i>
						<li><i class="fa fa-thumbs-down"></i>
						<li><i class="fa fa-youtube-square"></i>
						<li><i class="fa fa-youtube"></i>
						<li><i class="fa fa-xing"></i> 
						<li><i class="fa fa-xing-square"></i>
					</ul>		
				</div>
				<div class='picto-slide'>
					<ul class="font-group">	
						<li><i class="fa fa-youtube-play"></i>
						<li><i class="fa fa-dropbox"></i>
						<li><i class="fa fa-stack-overflow"></i>
						<li><i class="fa fa-instagram"></i>
						<li><i class="fa fa-flickr"></i>
						<li><i class="fa fa-adn"></i>
						<li><i class="fa fa-bitbucket"></i> 
						<li><i class="fa fa-bitbucket-square"></i></li>
						<li><i class="fa fa-tumblr"></i> </li>
						<li><i class="fa fa-tumblr-square"></i> </li>
						<li><i class="fa fa-long-arrow-down"></i> </li>
						<li><i class="fa fa-long-arrow-up"></i> </li>
						<li><i class="fa fa-long-arrow-left"></i> </li>
						<li><i class="fa fa-long-arrow-right"></i> </li>
						<li><i class="fa fa-apple"></i> </li>
						<li><i class="fa fa-windows"></i></li>
						<li><i class="fa fa-android"></i></li>
						<li><i class="fa fa-linux"></i> </li>
						<li><i class="fa fa-dribbble"></i></li>
						<li><i class="fa fa-skype"></i></li>
						<li><i class="fa fa-foursquare"></i></li>
						<li><i class="fa fa-trello"></i></li>
						<li><i class="fa fa-female"></i></li>
						<li><i class="fa fa-male"></i></li>
						<li><i class="fa fa-gittip"></i></li>
						<li><i class="fa fa-sun-o"></i></li>
						<li><i class="fa fa-moon-o"></i></li>
						<li><i class="fa fa-archive"></i></li>
						<li><i class="fa fa-bug"></i></li>
						<li><i class="fa fa-vk"></i></li>
						<li><i class="fa fa-weibo"></i></li>
						<li><i class="fa fa-renren"></i></li>
						<li><i class="fa fa-pagelines"></i></li>
						<li><i class="fa fa-stack-exchange"></i></li>
						<li><i class="fa fa-arrow-circle-o-right"></i></li>
						<li><i class="fa fa-arrow-circle-o-left"></i></li>
						<li><i class="fa fa-dot-circle-o"></i></li>
						<li><i class="fa fa-wheelchair"></i></li>
						<li><i class="fa fa-vimeo-square"></i></li>
						<li><i class="fa fa-try"></i></li>
						<li><i class="fa fa-plus-square-o"></i></li>		
						<!--li><i class="fa fa-caret-square-o-left"></li-->
					</ul>
				</div>
			</div>
			
			<!-- Direction Navigator Skin Begin -->
			<div style="position:absolute; top:0; left:0; width:60px; height:100%; background:#999;">
			</div>
			<!-- Arrow Left -->
			<span u="arrowleft" class="jssord03l" style="width: 55px; height: 55px; top: 10px; left:0px!important;">
			</span>
			<!-- Arrow Right -->
			<span u="arrowright" class="jssord03r" style="width: 55px; height: 55px; top: 65px; left:0px!important;">
			</span>
			<!-- Direction Navigator Skin End -->
		</div>
		<div class="layer_1" >
		</div>
	</div>