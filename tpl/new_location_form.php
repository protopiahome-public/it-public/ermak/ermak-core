<?php

		global $location_hide_types;
		?>
			
			<div class="form-field">
				<label for="term_meta[location_type]"><?php _e("location_type", "smc"); ?></label>
				<select  name="term_meta[location_type]" id="term_meta[location_type]" class="chosen-select">
					<?php
						$posts		= get_posts(array('numberposts' => 500,'post_type'=>"location_type", 'orderby'=>"ID", 'order'=>'ASC'));
						foreach($posts as $post)
						{
							$selected		= "";
							echo "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
						}
					
					?>
				</select>
			</div>
			<div class="form-field">
				<label for="term_meta[creator]"><?php _e("Location creator", "smc"); ?></label>					
				<?php
					wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[creator]',
																				'class'				=> 'chosen-select',
																				'multi'             => true,
																				'selected'			=> $term_meta ['creator']
																			  )
																	  );
				?>
			</div>
			<div class="form-field"  style="height:100px;">
				<label for="term_meta[owners]"><?php _e("Location owners", "smc"); ?></label>
				<p></p>
				<?php
				wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[owner1]',
																				'class'				=> 'chosen-select',
																				'multi'             => true,
																			  )
																	  );
				echo "<p></p>";
				wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[owner2]',
																				'class'				=> 'chosen-select',
																				'multi'             => true,
																			  )
																	  );
				echo "<p></p>";
				wp_dropdown_users(
																		array(
																				'show_option_none'  => "---",
																				'echo'				=> true,
																				'show'				=> 'display_name',
																				'name'				=> 'term_meta[owner3]',
																				'class'				=> 'chosen-select',
																				'multi'             => true,
																			  )
																	  );
				echo "<p></p>";
				?>
			</div>
			<div class="form-field" style="height:150px;">
				<label for="term_meta[hiding_type]"><?php _e("Hiding Type", "smc"); ?></label>
				<p></p>
				<select  name="term_meta[hiding_type]" id="term_meta[hiding_type]" class="chosen-select">
					<option value=0><?php echo __($location_hide_types[0], "smc")?></option>
					<option value=1><?php echo __($location_hide_types[1], "smc")?></option>
					<option value=2><?php echo __($location_hide_types[2], "smc")?></option>
				</select>
			</div>
				
			<script>
				//set_chosen(".chosen-select", {max_selected_options: 5});
			</script>
		<?php
		
		?>