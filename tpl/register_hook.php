<?php
	//init_textdomain();
	function pre_init_location_types()
	{
		$get_p				= get_posts(array("post_type"=>"location_type"));
		if(count($get_p)	!=0)return;
		require_once(SMC_REAL_PATH.'tpl/post_trumbnail.php');
		$types				= loctype_presets();
		foreach($types as $type)
		{
			$new_loctype_ID 				= wp_insert_post(array(
																	  "comment_status"=>'closed'
																	, 'post_name'=> $type['post_name']
																	, 'post_status' => 'publish'
																	, "post_title"=> __($type['post_name'], "smc")
																	, "post_type"=>"location_type"
																	)
															);
				
			//insert trumbail
			
			$src_dir		= SMC_URLPATH."img/";
			$wp_upload_dir 	= wp_upload_dir();				
			$filename		= download_url($src_dir . $type['thumbnail']);
			//$filename		= 'lp_location_default.jpg';		
			cyfu_publish_post($new_loctype_ID, $filename);
			
			add_post_meta($new_loctype_ID, "slug", 			$type['post_name']);
			add_post_meta($new_loctype_ID, "use_by_player", $type['use_by_player']);
			add_post_meta($new_loctype_ID, "picto", 		$type['picto']);
			add_post_meta($new_loctype_ID, "color", 		$type['color']);
			add_post_meta($new_loctype_ID, "map_behavior", 	$type['map_behavior']);
		}
	}	
	function get_loctype_dialog()
	{
		global $Soling_Metagame_Constructor;
		$types			= loctype_presets();
		$ens			= $Soling_Metagame_Constructor->get_location_type_names();
		$html			= "<div>";
		foreach($types as $type)
		{
			$post_name	= $type['post_name'];
			$checked	= in_array( __($type['post_name'], "smc"), $ens) ? "" : "checked";
			$html		.= "
			<div>
				<input type='checkbox' class='css-checkbox' id='$post_name' value='$post_name' $checked locy='location_type'>
				<label for='$post_name' class='css-label'>".__($post_name, "smc")."</label>
			</div>";
		}
		$html			.= "</div>";
		return $html;
	}
	function install_loctypes($arr)
	{
		require_once(SMC_REAL_PATH.'tpl/post_trumbnail.php');
		$types				= loctype_presets();
		$types_names		= array();
		$html				= array();
		foreach($types as $type)
		{
			if(!in_array( $type['post_name'], $arr ) )	continue;
			
			$new_loctype_ID 				= wp_insert_post(array(
																	  "comment_status"=>'closed'
																	, 'post_name'=> $type['post_name']
																	, 'post_status' => 'publish'
																	, "post_title"=> __($type['post_name'], "smc")
																	, "post_type"=>"location_type"
																	)
															);
				
			//insert trumbail
			/*
			$src_dir		= SMC_URLPATH."img/";
			$wp_upload_dir 	= wp_upload_dir();				
			$filename		= $src_dir . $type['thumbnail'];
			//$filename		= download_url($src_dir . $type['thumbnail']);
			cyfu_publish_post($new_loctype_ID, $filename);
			*/
			add_post_meta($new_loctype_ID, "slug", 			$type['post_name']);
			add_post_meta($new_loctype_ID, "use_by_player", $type['use_by_player']);
			add_post_meta($new_loctype_ID, "picto", 		$type['picto']);
			add_post_meta($new_loctype_ID, "color", 		$type['color']);
			add_post_meta($new_loctype_ID, "map_behavior", 	$type['map_behavior']);
			
			$html[]			= $type['post_name'];
		}
		return implode(",", $html);
	}	
	function loctype_presets()
	{
		return array(
			array(
					'post_name'=> "Planet system"
					,"use_by_player" => 0
					,"color" => '#838B8B'
					,"picto"=>'<i class="fa fa-sun-o"></i>'
					,"thumbnail"=>"lp_planet_system.jpg"
					,"map_behavior"=>3
					),
			array(
					'post_name'=> "Planet"
					,"use_by_player" => 0
					,"color" => '#1874CD'
					,"picto"=>'<i class="fa fa-globe"></i>'
					,"thumbnail"=>"lp_planet.jpg"
					,"map_behavior"=>3
					),
			array(
					'post_name'=> "Land"
					,"use_by_player" => 0
					,"color" => '#66CD00'
					,"picto"=>'<i class="fa fa-map-marker"></i>'
					,"thumbnail"=>"lp_land.jpg"
					,"map_behavior"=>3
					),
			array(
					'post_name'=> "Region"
					,"use_by_player" => 0
					,"color" => '#66CD00'
					,"picto"=>'<i class="fa fa-map-marker"></i>'
					,"thumbnail"=>"lp_region.jpg"
					,"map_behavior"=>3
					),
			array(
					'post_name'=> "City"
					,"use_by_player" => 0
					,"color" => '#CD2A1F'
					,"picto"=>'<i class="fa fa-building-o"></i>'
					,"thumbnail"=>"lp_city.jpg"
					,"map_behavior"=>2
					),
			array(
					'post_name'=> "Company"
					,"use_by_player" => 1
					,"color" => '#CD6090'
					,"picto"=>'<i class="fa fa-briefcase"></i>'
					,"thumbnail"=>"lp_company.jpg"
					,"map_behavior"=>1
					),
			array(
					'post_name'=> "Logistic Company"
					,"use_by_player" => 1
					,"color" => '#EEAD0E'
					,"picto"=>'<i class="fa fa-truck"></i>'
					,"thumbnail"=>"lp_logistic_company.jpg"
					,"map_behavior"=>1
					),
			array(
					'post_name'=> "Laboratory"
					,"use_by_player" => 1
					,"color" => '#9BCD9B'
					,"picto"=>'<i class="fa fa-flask"></i>'
					,"thumbnail"=>"lp_laboratory.jpg"
					,"map_behavior"=>1
					),
			array(
					'post_name'=> "Military"
					,"use_by_player" => 1
					,"color" => '#CDBE70'
					,"picto"=>'<i class="fa fa-bolt"></i>'
					,"thumbnail"=>"lp_military.jpg"
					,"map_behavior"=>1
					)
		);
	}
?>