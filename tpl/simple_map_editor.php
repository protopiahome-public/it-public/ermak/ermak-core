<?php
	global $UAM;
	global $smc_height;
	$args				= array(
									'number' 		=> 120
									,'offset' 		=> 0
									,'orderby' 		=> 'count'
									,'order' 		=> 'DESC'
									,'hide_empty' 	=> false
									,'fields' 		=> 'all'
									,'slug' 		=> ''
									,'hierarchical' => true
									,'name__like' 	=> ''
									,'pad_counts' 	=> false
									,'get' 			=> ''
									,'child_of' 	=> 0
									,'parent' 		=> $location_id
								);
								
	$locs				= get_terms('location', $args);

	foreach($locs as $loc)
	{
		$nbb .='<option value="' . $loc->term_id . '">' . $loc->name . '</option>';		
	}
	$me_help	= "
	<ol>
	<li>Draw map in a vector graphics editor that supports svg-format, like Adobe Illustrator or online editor <a href='https://svg-edit.github.io/svgedit/releases/svg-edit-2.8.1/svg-editor editor.html'>SVGEdit</a>
	<li>You need to consider the following:
	<ul>
		<li>Grouped elements are ignored editor Ermak.
		<li>The contours of the elements are ignored editor Ermak.
	</ul>
	<li>Save result to the svg file to the hard disk of your computer.
	<li>Open the file in the current editor.
	<li>Specify the elements to which it is necessary to assign Locations with no parents. Each selected element, assign a value from the drop-down list.
	<li>Save the map and check it in the Metagame panel on the front of the site
	</ol>";
	
	$hlp_mm = '
	<div style="max-width:400px;">'.
		"<h2>".__("How create a map", "smc"). "</h2>".
		$me_help.
	'</div>';
		
	$this->simple_map_editor = '
	<div style="position:relative;">
		<div class="geom-cont" style="width:1000px; height:'. ($smc_height + 20) .'px;">
			<div id="geom-svg" style="width:1000px;"> 
				<h2>'. __("Map", "smc") . '</h2>'.
				$this->get_main_map_svg_data().
			'</div>
			<div class="geom-menu" id="geom-menu"  style="width:700px;">
				<!--div id="menu_drag_btn" class="svg_menu_element">
				</div-->
				<div class="svg_menu_btn unselected" id="draw_path_btn" 	type="draw" item="map_draw">
					<span><i class="fa fa-arrow-up" style="-moz-transform: rotate(-45deg);
						-webkit-transform: rotate(-45deg);
						-o-transform: rotate(-45deg);
						-ms-transform: rotate(-45deg);
						transform: rotate(-45deg);"></i></span>
				</div>
				<div class="svg_menu_btn svg_upload hint hint--top" data-hint="'.__("Load svg source file", "smc").'" id="open_path_btn" style="margin: 0; margin-right: 4px; padding: 5px 4px; overflow: visible;">			
					<i class="fa fa-folder-open"></i>
					<input type="file" accept="image/svg" style="
						position: absolute;
						left: 0;
						top: 0;
						width: 100%;
						height: 100%;
						transform: scale(1);
						letter-spacing: 10em;     /* IE 9 fix */
						-ms-transform: scale(1); /* IE 9 fix */
						opacity: 0;
						cursor: pointer;
						padding:5px;
						">				
				</div>			
				<!--div class="svg_menu_btn unselected" id="paint_btn" 	type="paint" item="map_paint">
					<i class="fa fa-paint-brush"></i>
				</div-->
				<div id="u_img_button" class="svg_menu_btn unselected hint hint--top" data-hint="'.__("Send Map element texture", "smc").'">
					<i class="fa fa-picture-o"></i>
				</div>			
				<div class="svg_menu_btn2 unselected" id="edit-map-button" >
					<span>'  . __("Edit map", "smc"). '</span>
				</div>		
				<div class="svg_menu_btn2 unselected" id="id_location_cont" style="padding:0; display:none;">
					<select id="id_locations">
						<option value="-1"> --- </option>'.
						$nbb .				
					'</select>
				</div>
				<div class="svg_menu_btn2 unselected" item="save_map_button" >' .
					__("Save map", "smc") .
				'</div>
				<div class="svg_menu_btn2 unselected" id="edit_map_instruction" >
					<span>'  . __("Instruction", "smc"). '</span>
				</div>'.
				Assistants::get_instruction_sign($hlp_mm, "m_instruction") .
				'<div id="map_instruction" class="lp-hide">'.
					$hlp_mm.
				'</div>
			</div>
		</div>
		<div style="margin-top:30px;">
			<h3>' . __("Design", "smc") . '</h3>
			<input type="checkbox" class="css-checkbox" id="enable_contour" name="enable_contour" '. checked(1, $this->options['enable_contour'], 0) . '>
			<label class="css-label" for="enable_contour">' . __("Enable map contour", "smc"). '</label> 	
			<input class="color rect_color" id="contour_color" name="contour_color" value="' . $this->options['contour_color'] . '"> 
			<label class="smc-label">' . __("Contour color", "smc") . '</label><Br>
			<input type="checkbox" class="css-checkbox" id="design_lines" name="design_lines" '. checked(1, $this->options['design_lines'], 0) . '>
			<label class="css-label" for="design_lines">' . __("Enable target Lines to Lands", "smc"). '</label><Br>
			<input type="checkbox" class="css-checkbox" id="design_show_children" name="design_show_children" '. checked(1, $this->options['design_show_children'], 0) . '>
			<label class="css-label" for="design_show_children">' . __("Show children child-Locations", "smc"). '</label><Br>
			<input type="checkbox" class="css-checkbox" id="design_show_grandchildren" name="design_show_grandchildren" '. checked(1, $this->options['design_show_grandchildren'], 0) . '>
			<label class="css-label" for="design_show_grandchildren">' . __("Show children of grandchild-Locations", "smc"). '</label> 	
			
		</div>
		<div class="form-field" id="panel_parameters"  style="margin-top:30px;">
			<h3>' . __('Behavior', "smc") . '</h3>
			<input type="checkbox" class="css-checkbox" id="location_change_info" name="location_change_info" ' . checked(1, $this->options['location_change_info'], 0) . '>
			<label class="css-label" for="location_change_info">' . __("href of map elements to post.", "smc") . '</label> <br>	
			<input name="ptp" id="ptp" class="css-checkbox" type="checkbox"  '. checked($this->options['page_to_panel'], 1, 0) . ' />
			<label for="ptp" class="css-label">'.__("Set content of special Pages to Metagame Panel","smc").'</label><br>
		</div>
	</div>';

?>