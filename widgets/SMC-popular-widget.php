<?php 
	/*
	
	*/

	// Stop direct access of the file
	if( !defined( 'ABSPATH' ) ) 
		die( );
	
	if( ! class_exists( 'SMC_PopularWidget' )
	&& ! class_exists( 'SMC_PopularWidgetFunctions' ) 
	&& file_exists( dirname( __FILE__ ) . "/_inc/functions.php" ) ){
		
	include_once( dirname( __FILE__ ) . "/_inc/functions.php" );
	
	class SMC_PopularWidget extends SMC_PopularWidgetFunctions {
		
		public $tabs = array();
		public $defaults = array();
		public $version = "1.6.6";
		public $current_tab = false;
		
		/**
		 * Constructor
		 *
		 * @return void
		 * @since 0.5.0
		 */
		function SMC_PopularWidget( ){
			
			$this->load_text_domain( );
			$this->SMC_PopularWidgetFunctions( ); 
			
			$this->WP_Widget( 'popular-widget', __( 'Popular Widget', 'pop-wid' ), 
				array( 'classname' => 'popular-widget', 'description' => __( "Display most popular posts and tags", 'pop-wid' ) ) 
			);
			
			define( 'SMC_POPWIDGET_FOLDER', plugin_basename( dirname( __FILE__ ) ) );
			define( 'SMC_POPWIDGET_ABSPATH', str_replace("\\","/", dirname( __FILE__ ) ) );
			define( 'SMC_POPWIDGET_URL', WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)) . "/" );
			
			$this->defaults = apply_filters( 'pop_defaults_settings', array(
				'nocomments' => false, 'nocommented' => false, 'noviewed' => false, 'norecent' => false, 
				'userids' => false, 'imgsize' => 'thumbnail', 'counter' => false, 'excerptlength' => 15, 'tlength' => 20,
				'meta_key' => '_popular_views', 'calculate' => 'visits', 'title' => '', 'limit'=> 5, 'cats'=>'', 'lastdays' => 90,
				'taxonomy' => 'post_tag', 'exclude_users' => false, 'posttypes' => array( 'post' => 'on' ), 'thumb' => false,
				'excerpt' => false, 'notags'=> false, 'exclude_cats' => false
			) );
			
			$this->tabs = apply_filters( 'pop_defaults_tabs', array(
				 'recent' =>  __( 'Recent Posts', 'pop-wid' ) , 
				 'comments' => __( 'Recent Comments', 'pop-wid' ) , 
				 'commented' => __( 'Most Commented', 'pop-wid' ), 
				 'viewed' => __( 'Most Viewed', 'pop-wid' ), 
				 'tags' => __( 'Tags', 'pop-wid' ) 
			 ) );
			 
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $args
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function widget( $args, $instance ) {
			if( file_exists( SMC_POPWIDGET_ABSPATH . '/_inc/widget.php' ) )
				include( SMC_POPWIDGET_ABSPATH . '/_inc/widget.php'  );
		}
		
		/**
		 * Configuration form.
		 *
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function form( $instance ) {
			if( file_exists( SMC_POPWIDGET_ABSPATH . '/_inc/form.php' ) )
				include( SMC_POPWIDGET_ABSPATH . '/_inc/form.php' );
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $instance
		 * @return array
		 * @since 1.5.6
		 */
		function update( $new_instance, $old_instance ){
			foreach( $new_instance as $key => $val ){
				if( is_array( $val ) )
					$new_instance[$key] = $val;
					
				elseif( in_array( $key, array( 'lastdays', 'limit', 'tlength', 'excerptlength' ) ) )			
					$new_instance[$key] = intval( $val );
					
				elseif( in_array( $key,array( 'calculate', 'imgsize', 'cats', 'userids', 'title', 'meta_key' ) ) )	
					$new_instance[$key] = trim( $val,',' );	
			}
			
			if( empty($new_instance['meta_key'] ) )
				$new_instance['meta_key'] = $this->defaults['meta_key'];
				
			return $new_instance;
		}
	}
	
	// do that thing you do!
	add_action( 'widgets_init' , create_function( '', 'return register_widget("SMC_PopularWidget");' ) );
	
	}//end if
