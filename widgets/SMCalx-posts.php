<?php
/*
	License: GNU General Public License v2.0
	License URI: http://www.gnu.org/licenses/gpl-2.0.html
	
	Copyright: (c) 2013 Alexander "Alx" Agnarson - http://alxmedia.se
	
		@package SMCAlxPosts
		@version 1.0
*/

class SMCAlxPosts extends WP_Widget {

/*  Constructor
/* ------------------------------------ */
	function SMCAlxPosts() {
		parent::__construct( false, __('Ermak Popular Posts', 'smc'), array('description' => 'Показует записи из категории', 'classname' => 'smc_widget_alx_posts') );;	
		add_action( 'init',				array($this, 'redirect_login_page'));
	}
	function redirect_login_page() 
	{  
		$login_page  	= home_url( '/' );  
		$page_viewed 	= basename($_SERVER['REQUEST_URI']);  
		$this->name 	= __('Ermak Popular Posts', 'smc');
		$this->widget_options['description'] 	= __('Показует записи из категории', 'smc');
	}
/*  Widget
/* ------------------------------------ */
	public function widget($args, $instance) {
		global $post;
		global $UAM;
		extract( $args );
		$instance['title'] ? NULL : $instance['title'] = '';
		$title = apply_filters('widget_title',$instance['title']);
		$output = $before_widget."\n";
		if($title)
			$output .= $before_title.$title.$after_title;
		ob_start();
		//print_r($instance['posts_num'] );
?>

	<?php 
		$args				= array(
			'post_type'				=> array( 'post' ),
			'showposts'				=> $instance['posts_num'] + 10,
			'cat'					=> $instance['posts_cat_id'],
			'ignore_sticky_posts'	=> true,
			'orderby'				=> $instance['posts_orderby'],
			'order'					=> 'dsc',
			'date_query' => array(
				array(
					'after' => $instance['posts_time'],
				),
			),
		);
		if($instance['posts_cat_id']=='All')
		{
			unset($args['cat']);//$instance['posts_cat_id'];
		}
		$posts = new WP_Query($args	);
		$i = 0;
	?>
	
	<ul class="alx-posts group <?php if($instance['posts_thumb']) { echo 'thumbs-enabled'; } ?>">
		<?php 
		if(!$posts->have_posts())
		{
			echo "сообщений пока нет";
		}
		else
		{ 
		?>
			<?php while ($posts->have_posts()): $posts->the_post(); ?>
			
			<?php 
				$acc			= 1;
				foreach(get_the_category(get_the_ID() ) as $category)
				{
					$acc 		*= $UAM->isAccessLocation($category->cat_ID);
				}
				if(!$acc)		continue;			
			?>
			<?php if(++$i >= $instance['posts_num'] + 1) break;?>
			<li>
				<div class="widget-item ">
				<?php if($instance['posts_category']) { ?><p class="widget-item-category">
				<?php 
					$cats				= '';
					foreach((get_the_category()) as $category) { 
						$cats			.= '<a href="/?cat='.$category->cat_ID.'" title="'.$category->cat_name.' (количество постов: '.$category->category_count.')">'.$category->cat_name.' ['.$category->category_count.']</a> <BR> ';
					}
					$locs				= get_the_terms(get_the_ID(), SMC_LOCATION_NAME);
					if($locs)
					{
						foreach($locs as $loc)
						{
							$lc				= SMC_Location::get_term_meta( $loc->term_id );//get_option("taxonomy_".$loc->term_id);
							$loc_type_id	= $lc['location_type'];
							$cats			.= '<a href="/?true='.$loc->slug.'" title="'.$loc->name.' (количество постов: '.$loc->count.')">'.get_post_meta($loc_type_id, "picto", true)." ".$loc->name.' ['.$loc->count.']</a> <BR> ';
						}
					}
					echo mb_substr($cats, 0, mb_strlen($cats)-3);
					//echo $cats;
				?></p><?php } ?>
				<?php if($instance['posts_thumb']) { // Thumbnails enabled? ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<div class="widget-item-thumbail">
							<?php if ( has_post_thumbnail() ){
								//the_post_thumbnail(array($instance['post_thumb_size'],$instance['post_thumb_size'])); 
								the_post_thumbnail("thumbnail"); 
							}?>
						</div>
					
				<?php } // Thumbnails enabled? ?> 
				
					</a>
						<div class="widget-item-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
						<?php 
							$content = get_the_content(); 					
							$content = apply_filters('the_content', $content);
							//$content = mb_substr($content,0, 100);
							$content = wp_trim_words($content, $instance['content_length']);
						?>
						<div class='widget-item-text'>
						</div>
						<div class='widget-item-text'><?php echo $content; ?> 	</div>		
						<p> </p>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<span class='widget-item-date'>
								<i class="fa fa-angle-double-right  fa-1x"></i>	
							</span>
							<span class="widget-item-date"> <i class="fa fa-coffee">
								</i>  <?php the_time('j M, Y'); ?> &nbsp; &nbsp; (<?php echo $post->comment_count."";?>)
							</span>
						</a>					
						<?php //echo "ddd - ";print_r($post->comment_count);?>
				</div>
				
			</li>
			<?php endwhile; ?>
			<?php //wp_reset_postdata() ;?>
			</ul><!--/.alx-posts-->
			<?php if($instance['button_text_enabled']):?>
			<?php
				$list		=	get_category_link( $instance['posts_cat_id'])
			?>
				<div>
					<a href="<?php echo  $list; ?>" class='botton'>
						<?php echo $instance['button_text']; ?>
					</a>
				</div>
				<?php 
					//echo  wp_get_archives('type=alpha');  
					//echo get_post_type_archive_link( 'post' );
				?>
			<?php endif; ?>
	<?php } ?>

<?php
		$output .= ob_get_clean();
		$output .= $after_widget."\n";
		echo $output;
	}
	
/*  Widget update
/* ------------------------------------ */
	public function update($new,$old) {
		$instance = $old;
		$instance['title'] = strip_tags($new['title']);
	// Posts
		$instance['posts_thumb'] = $new['posts_thumb']?1:0;
		$instance['posts_category'] = $new['posts_category']?1:0;
		$instance['posts_num'] = strip_tags($new['posts_num']);
		$instance['posts_cat_id'] = strip_tags($new['posts_cat_id']);
		$instance['posts_orderby'] = strip_tags($new['posts_orderby']);
		$instance['posts_time'] = strip_tags($new['posts_time']);
		$instance['button_text_enabled'] = strip_tags($new['button_text_enabled']);
		$instance['button_text'] = strip_tags($new['button_text']);
		$instance['post_thumb_size'] = $new['post_thumb_size'];
		$instance['content_length'] = $new['content_length'];
		return $instance;
	}

/*  Widget form
/* ------------------------------------ */
	public function form($instance) {
		// Default widget settings
		$defaults = array(
			'title' 			=> '',
		// Posts
			'posts_thumb' 		=> 1,
			'posts_category'	=> 1,
			'posts_num' 		=> '4',
			'posts_cat_id' 		=> '0',
			'posts_orderby' 	=> 'date',
			'posts_time' 		=> '0',
			'button_text_enabled'=> '0',
			'button_text'		=> "see all",
			'post_thumb_size'	=> 32,
			'content_length'	=> 8,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
?>

	<style>
	.widget .widget-inside .alx-options-posts .postform { width: 100%; }
	.widget .widget-inside .alx-options-posts p { margin-bottom: 0.3em; }
	.widget .widget-inside .alx-options-posts hr { border: none; border-bottom: 1px dotted #ddd; margin: 1em 0; }
	.widget .widget-inside .alx-options-posts h4 { margin-bottom:0.665em; }
	</style>
	
	<div class="alx-options-posts">
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Заголовок:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
		</p>
		
		<hr>
		<h4>List Posts</h4>
		
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('posts_thumb'); ?>" name="<?php echo $this->get_field_name('posts_thumb'); ?>" <?php checked( (bool) $instance["posts_thumb"], true ); ?>>
			<label for="<?php echo $this->get_field_id('posts_thumb'); ?>">Показывать миниатюры</label>
		</p>	
		<p>
			<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id('post_thumb_size'); ?>">Размер миниатюры</label>
			<input style="width:20%;" id="<?php echo $this->get_field_id('post_thumb_size'); ?>" name="<?php echo $this->get_field_name('post_thumb_size'); ?>" type="text" value="<?php echo absint($instance['post_thumb_size']); ?>" size='3' />
		</p>
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('posts_category'); ?>" name="<?php echo $this->get_field_name('posts_category'); ?>" <?php checked( (bool) $instance["posts_category"], true ); ?>>
			<label for="<?php echo $this->get_field_id('posts_category'); ?>">Показывать названия рубрик</label>
		</p>		
		<p>
			<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("posts_num"); ?>">Количество постов</label>
			<input style="width:20%;" id="<?php echo $this->get_field_id("posts_num"); ?>" name="<?php echo $this->get_field_name("posts_num"); ?>" type="text" value="<?php echo absint($instance["posts_num"]); ?>" size='3' />
		</p>		
		<p>
			<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("content_length"); ?>">Количество слов в посте</label>
			<input style="width:20%;" id="<?php echo $this->get_field_id("content_length"); ?>" name="<?php echo $this->get_field_name("content_length"); ?>" type="text" value="<?php echo absint($instance["content_length"]); ?>" size='3' />
		</p>
		<p>
			<label style="width: 100%; display: inline-block;" for="<?php echo $this->get_field_id("posts_cat_id"); ?>">Рубрика:</label>
			<?php wp_dropdown_categories( array( 'name' => $this->get_field_name("posts_cat_id"), 'selected' => $instance["posts_cat_id"], 'show_option_all' => 'All', 'show_count' => true ) ); ?>		
		</p>
		<p style="padding-top: 0.3em;">
			<label style="width: 100%; display: inline-block;" for="<?php echo $this->get_field_id("posts_orderby"); ?>">Тип сортировки:</label>
			<select style="width: 100%;" id="<?php echo $this->get_field_id("posts_orderby"); ?>" name="<?php echo $this->get_field_name("posts_orderby"); ?>">
			  <option value="date"<?php selected( $instance["posts_orderby"], "date" ); ?>>по полулярности</option>
			  <option value="comment_count"<?php selected( $instance["posts_orderby"], "comment_count" ); ?>>по комментариям</option>
			  <option value="rand"<?php selected( $instance["posts_orderby"], "rand" ); ?>>случайная</option>
			</select>	
		</p>
		<p style="padding-top: 0.3em;">
			<label style="width: 100%; display: inline-block;" for="<?php echo $this->get_field_id("posts_time"); ?>">За время:</label>
			<select style="width: 100%;" id="<?php echo $this->get_field_id("posts_time"); ?>" name="<?php echo $this->get_field_name("posts_time"); ?>">
			  <option value="0"<?php selected( $instance["posts_time"], "0" ); ?>>от начала</option>
			  <option value="1 year ago"<?php selected( $instance["posts_time"], "1 year ago" ); ?>>последний год</option>
			  <option value="1 month ago"<?php selected( $instance["posts_time"], "1 month ago" ); ?>>последний месяц</option>
			  <option value="1 week ago"<?php selected( $instance["posts_time"], "1 week ago" ); ?>>последняя неделя</option>
			  <option value="1 day ago"<?php selected( $instance["posts_time"], "1 day ago" ); ?>>посление сутки</option>
			</select>	
		</p>
		<hr>
		<p>
			<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('button_text_enabled'); ?>" name="<?php echo $this->get_field_name('button_text_enabled'); ?>" <?php checked( (bool) $instance["button_text_enabled"], true ); ?>>
			<label for="<?php echo $this->get_field_id('button_text_enabled'); ?>">Поставить кнопку "Перейти к рубрике</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('button_text'); ?>">текст на кнопке "Перейти к рурике:</label>
			<input class="widefat" id="<?php echo $this->get_field_id('button_text'); ?>" name="<?php echo $this->get_field_name('button_text'); ?>" type="text" value="<?php echo esc_attr($instance["button_text"]); ?>" />
		</p>
		<br>
	</div>
<?php

}

}

/*  Register widget
/* ------------------------------------ */
	function alx_register_widget_posts() { 
		register_widget( 'SMCAlxPosts' );
	}
	add_action( 'widgets_init', 'alx_register_widget_posts' );
?>
<?php
<<<EOF
<style type="text/css">	
	.botton
	{
		display: block;
		background: #fcfcfc; /* Old browsers */
		background: -moz-linear-gradient(top,  #fcfcfc 0%, #e2e2e2 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#e2e2e2)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  #fcfcfc 0%,#e2e2e2 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  #fcfcfc 0%,#e2e2e2 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  #fcfcfc 0%,#e2e2e2 100%); /* IE10+ */
		background: linear-gradient(to bottom,  #fcfcfc 0%,#e2e2e2 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#e2e2e2',GradientType=0 ); /* IE6-9 */
		-webkit-border-radius: 2px 2px 2px 2px;
		border-radius: 2px 2px 2px 2px;
		border: 1px solid #CCC;
		padding: 10px 45px;
		color: #7B7B7B;
		font-size: 0.875em;
		font-family: inherit;
		text-decoration: none;
		text-shadow: 0 1px 0 #FFFFFF;
		text-align: center;
		box-shadow: inset 0 1px 0 #FFFFFF,0 1px 2px rgba(0,0,0,0.1);
	}
	.botton:hover
	{
		background: #fcfcfc; /* Old browsers */
		background: -moz-linear-gradient(top,  #fcfcfc 0%, #ececec 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#ececec)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  #fcfcfc 0%,#ececec 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  #fcfcfc 0%,#ececec 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  #fcfcfc 0%,#ececec 100%); /* IE10+ */
		background: linear-gradient(to bottom,  #fcfcfc 0%,#ececec 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#ececec',GradientType=0 ); /* IE6-9 */

	}
	.botton:active {	
		background: #fcfcfc; /* Old browsers */
		background: -moz-linear-gradient(bottom,  #fcfcfc 0%, #ececec 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#ececec)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(bottom,  #fcfcfc 0%,#ececec 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(bottom,  #fcfcfc 0%,#ececec 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(bottom,  #fcfcfc 0%,#ececec 100%); /* IE10+ */
		background: linear-gradient(to top,  #fcfcfc 0%,#ececec 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient(endColorstr='#ececec', startColorstr='#fcfcfc', GradientType=0 ); /* IE6-9 */
		.box-shadow(none);
		padding-top: 11px;
		padding-bottom: 9px;
	}

</style>
@xxx why is it on auth output?
EOF;
?>