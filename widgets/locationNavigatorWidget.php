<?php
	/*
	Plugin Name: Location Navigator Widget
	
	License: GNU General Public License v2.0
	License URI: http://www.gnu.org/licenses/gpl-2.0.html
	
	Copyright: (c) 2014 genagl@gmail.com
	
		@package genagl
		@version 1.0
	*/
	class LocationNavigatorWidget extends WP_Widget 
	{
		/*  Constructor
		/* ------------------------------------ */
		function LocationNavigatorWidget() 
		{
			parent::__construct( false, __("Ermak Locations", "smc"), array('description' => 'Locations accordeon', 'classname' => 'widget_location_navigator') );;	
			add_action( 'init',				array($this, 'redirect_login_page'));
		}
		function redirect_login_page() 
		{  
			$login_page  	= home_url( '/' );  
			$page_viewed 	= basename($_SERVER['REQUEST_URI']);  
			$this->name 	= __('Ermak Locations', 'smc');
			$this->widget_options['description'] 	= __('Player Cabinet', 'smc');
		}
		/*  Widget
		/* ------------------------------------ */
		public function widget($args, $instance) 
		{
			global $UAM;
			global $Soling_Metagame_Constructor;	
			global $user_iface_color;			
			extract( $args );
			$output = $before_widget."\n";
			
			
			
			if(is_tax('location'))
			{
				$cur_term		= get_term_by('name', single_term_title("",0), 'location');
				$location_id 	= $cur_term->term_id;
			}
			else
				$location_id	= 0;
			
			$parent1			= get_term_by("id", $cur_term->parent, "location");
			$parent_url			= $location_id == 0 ? "#" : '/?true='.$parent1->slug;
			
			$args				= array(
									'number' 		=> 0
									,'offset' 		=> 0
									,'orderby' 		=> 'count'
									,'order' 		=> 'DESC'
									,'hide_empty' 	=> false
									,'fields' 		=> 'all'
									,'slug' 		=> ''
									,'hierarchical' => true
									,'name__like' 	=> ''
									,'pad_counts' 	=> false
									,'get' 			=> ''
									,'child_of' 	=> 0
									,'parent' 		=> $location_id
								);
									
			$locs				= get_terms('location', $args);			
			$locations			= array();
			foreach($locs as $loc)
			{
				if($UAM->isAccessLocation($loc->term_id))
					$locations[] = $loc;
			}			
			$cur_location		= get_term_by("term_id", $location_id, 'location');
			$cur_lt				= $Soling_Metagame_Constructor->get_location_type($cur_location->term_id);
			$cur_location_options = SMC_Location::get_term_meta(  $location_id );  //get_option( "taxonomy_$location_id");
			
			
			?>
			<style>
				#wrapper-250 {
					width:100%;
					margin:0;
					display:block;
				}
				.accordion,
				.accordion ul,
				.accordion li,
				.accordion a,
				.accordion span {
					margin: 0;
					padding: 0;
					border: none;
					outline: none;
					
				}
				.accordion li {
					list-style: none;
					margin: 0!important;
				}
				/* Макет и Стиль */
				.accordion li > a {
					display: block;
					position: relative;
					min-width: 110px;
					padding: 0 10px 0 30px;
					height: 32px;
					font: bold 12px/32px Arial, sans-serif;
					text-decoration: none;
					text-shadow: 0px 1px 0px rgba(0,0,0, .35);
					-webkit-box-shadow: inset 0px 1px 0px 0px rgba(255,255,255, .1), 0px 1px 0px 0px rgba(0,0,0, .1);
					-moz-box-shadow: inset 0px 1px 0px 0px rgba(255,255,255, .1), 0px 1px 0px 0px rgba(0,0,0, .1);
					box-shadow: inset 0px 1px 0px 0px rgba(255,255,255, .1), 0px 1px 0px 0px rgba(0,0,0, .1);
					margin: 0!important;
				}
				.accordion > li:hover > a,
				.accordion > li:target > a,
				.accordion > li > a.active {
					color: #EEE;
					text-shadow: 1px 1px 1px rgba(255,255,255, .2);
					background:  <?php echo $user_iface_color ?>; 
					margin: 0!important;
				}
				.accordion li > a span {
					display: block;
					position: absolute;
					top: 7px;
					right: 0;
					padding: 0 10px;
					margin: 0;
					margin-right: 10px;
					font: normal bold 12px/18px Arial, sans-serif;
					background: #404247;
					-webkit-border-radius: 15px;
					-moz-border-radius: 15px;
					border-radius: 15px;
					-webkit-box-shadow: inset 1px 1px 1px rgba(0,0,0, .2), 1px 1px 1px rgba(255,255,255, .1);
					-moz-box-shadow: inset 1px 1px 1px rgba(0,0,0, .2), 1px 1px 1px rgba(255,255,255, .1);
					box-shadow: inset 1px 1px 1px rgba(0,0,0, .2), 1px 1px 1px rgba(255,255,255, .1);'
				}
				.accordion > li:hover > a span,
				.accordion > li:target > a span,
				.accordion > li > a.active span {
					color: #fdfdfd;
					text-shadow: 0px 1px 0px rgba(0,0,0, .35);
					background: <?php echo $user_iface_color ?>; 
				}
				/* Подменю */
				.sub-menu li a {
					color: #797979!important;
					text-shadow: 1px 1px 0px rgba(255,255,255, .2);
					background: #e5e5e5;
					border-bottom: 1px solid #c9c9c9;
					-webkit-box-shadow: inset 0px 1px 0px 0px rgba(255,255,255, .1), 0px 1px 0px 0px rgba(0,0,0, .1);
					-moz-box-shadow: inset 0px 1px 0px 0px rgba(255,255,255, .1), 0px 1px 0px 0px rgba(0,0,0, .1);
					box-shadow: inset 0px 1px 0px 0px rgba(255,255,255, .1), 0px 1px 0px 0px rgba(0,0,0, .1);
				}
				.sub-menu li:hover a { background: #efefef; }
				.sub-menu li:last-child a { border: none; }
				.sub-menu li > a span {
					color: #797979;
					text-shadow: 1px 1px 0px rgba(255,255,255, .2);
					background: transparent;
					border: 1px solid #c9c9c9;
					-webkit-box-shadow: none;
					-moz-box-shadow: none;
					box-shadow: none;
				}
				.sub-menu em {
					position: absolute;
					top: 0;
					left: 0;
					margin-left: 14px;
					color: #a6a6a6;
					font: normal 10px/32px Arial, sans-serif;
				}
				/* функциональность */
				 
				.accordion li > .sub-menu {
					display: none;
				}
				.accordion li:target > .sub-menu {
					display: block;
				}
			</style>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
			<script type="text/javascript">
 
	$(document).ready(function() {
 
		// Хранение переменных 
		var accordion_head = $('.accordion > li > a'),
			accordion_body = $('.accordion li > .sub-menu');
 
		// Открытие первой вкладки при загрузке 
		accordion_head.first().addClass('active').slideDown('normal');
 
		// Выбор функции 
		accordion_head.on('click', function(event) {
 
			// Отключить заголовок ссылки 
			event.preventDefault();
 
			// Отображение и скрытие вкладок при клике 
			if ($(this).attr('class') != 'active'){
				accordion_body.slideUp('slow');//
				$(this).next().stop(true,true).slideToggle('normal');
				accordion_head.removeClass('active');
				$(this).addClass('active');
			}
 
		});
 
	});
 
</script>
			<!-- http://dbmast.ru/menyu-navigacii-v-stile-akkordeon-css3-jquery -->
			<div id="wrapper-250">
				<?php 
					$par	= $parent_url =="#" ? __("Locations Menu", "smc") : '<a href="'.$parent_url.'" ><i class="fa fa-arrow-up"></i></a><span style="margin-left:10px;"> '.$cur_location->name.'</span>';
					$output .= $before_title.$par.$after_title;
					ob_start();
				?>
				<ul class="accordion">
					<?php foreach($locations as $location):?>
						<?php 
							$lt 	= $Soling_Metagame_Constructor->get_location_type($location->term_id);							
							$tax_children = get_term_children( $location->term_id, 'location' ); 
							$url 	= '/?true='.$location->slug;
						?>
						<li id="location_<?php print_r($location->term_id); ?>">
							<a run="<?php print_r($tax_children>0);?>" url="<?php echo $url; ?>" href="<?php echo '#location_'.($location->term_id) ?>" title="<?php echo $lt->post_title . " " . $location->name; ?>"><?php echo $lt->picto . " " . $location->name; ?><span><?php print_r($cnt); ?></span></a>
							<ul class="sub-menu">
								<?php
									
									$ii=0;
									foreach ($tax_children as $child_id)
									{
										$child 		= get_term_by( 'id', $child_id, 'location' );
										echo '<li><a href="/?true='.$child->slug.'"><em>'.++$ii.'</em>' . $child->name . '</a></li>';
									}
								?>
								
							</ul>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php
			$output .= ob_get_clean();
			$output .= $after_widget."\n";
			echo $output;
		}
		
		/*  Widget update
		/* ------------------------------------ */
		public function update($new,$old) 
		{
			$instance = $old;
			$instance['title'] = strip_tags($new['title']);
			return $instance;
		}
		
		/*  Widget form
		/* ------------------------------------ */
		public function form($instance) 
		{
			// Default widget settings
			$defaults = array(
				'title' 			=> '',
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			?>
			
			<style>
				.widget .widget-inside .alx-options-posts .postform { width: 100%; }
				.widget .widget-inside .alx-options-posts p { margin-bottom: 0.3em; }
				.widget .widget-inside .alx-options-posts hr { border: none; border-bottom: 1px dotted #EEE; margin: 1em 0; }
				.widget .widget-inside .alx-options-posts h4 { margin-bottom:0.665em; }
			</style>
			<div class="alx-options-posts">
				<p>
					Widget haven't setings
				</p>
				
				<hr>
			</div>
			<?php
		}
			
	}
	/*  Register widget
	/* ------------------------------------ */
	function register_widget_loc_navi() { 
		register_widget( 'LocationNavigatorWidget' );
	}
	add_action( 'widgets_init', 'register_widget_loc_navi' );
?>